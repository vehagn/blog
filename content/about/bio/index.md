---
title: "Bio"
date: 2023-10-01T12:00:00+02:00

layout: profile

showDate: false
showEdit: false
showWordCount: false
showReadingTime: false

showHero: true
heroStyle: background

showComments: false
showRelatedContent: false
sharingLinks: false
---

Hi, and welcome! 👋

I've always had a curious min and enjoy exploring new ideas and concepts,
this led me to get picked up by [Edgeworks](https://edgeworks.no/) (🇳🇴) where I currently work as a DevOps consultant
engaged at [Statens pensjonskasse](https://spk.no/en).

Coming from a background in physics I kind of stumbled into the world of IT,
though I feel right at home with the problem-solving opportunities and open-minded culture I'm now part of.

The [elastic wave equation](https://en.wikipedia.org/wiki/Wave_equation) has been replaced
by [YAML](https://en.wikipedia.org/wiki/YAML)

{{< katex >}}
$$\rho(\vec{x})\ddot{u}(\vec{x},t) -\nabla\sigma(\vec{x},t) = \vec{f}(\vec{x},t)$$

and the [Poincaré Sphere](https://en.wikipedia.org/wiki/Unpolarized_light#Poincar%C3%A9_sphere) now an almost forgotten
tool for [Mueller Matrix Ellipsometry](https://en.wikipedia.org/wiki/Ellipsometry) optimisation.

{{< dynamic-image title="The Poincaré Sphere describing polarisation" id="poincaré" dark="images/01-poincare-sphere-dark.svg" light="images/01-poincare-sphere-light.svg" >}}

This blog is an outlet for my current obsessions,
be it what I'm currently working on or other interests like old motorcycles or brewing.

{{< katex >}}
$$\sum_{n+1}^{\infin}\frac{1}{n^2} = \frac{\pi^2}{6}$$

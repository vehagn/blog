---
title: "Gateway API with Cilium and Cert-manager"
date: 2023-12-30T17:45:00+01:00
lastmod: 2024-08-13T23:00:00+02:00

categories:
  - posts
  - kubernetes
  - network

tags:
  - kubernetes
  - gateway api
  - cilium
  - cert-manager
  - https
  - tls
  - networking
---

In the Gateway API SIG's own words,
> If you're familiar with the older [Ingress API](https://kubernetes.io/docs/concepts/services-networking/ingress/),
> you can think of the Gateway API as analogous to a more-expressive next-generation version of that API.

In this article we'll quickly review the role-oriented architecture of the Gateway API before we implement it using
Cilium and Cert-manager.
Other [Gateway API implementations](https://gateway-api.sigs.k8s.io/implementations/) are listed on the Gateway SIG
site.

We'll mainly take a look at replacing `Ingress` resources for traffic from clients outside the cluster to services
inside the cluster ([north/south traffic](https://gateway-api.sigs.k8s.io/concepts/glossary/#northsouth-traffic)).
Although the Gateway API also supports
so-called [east/west traffic](https://gateway-api.sigs.k8s.io/concepts/glossary/#eastwest-traffic) between workloads
within a cluster (through the [GAMMA-initiative](https://gateway-api.sigs.k8s.io/concepts/gamma/)),
this is outside the scope of this article.

Before reading this article you might want to try
a [hands-on lab on Cilium Gateway API](https://isovalent.com/labs/gateway-api/) by Isovalent, the company behind Cilium.

{{< alert "circle-info" >}}
**Edit 2024.08.10**: This article is updated to work with [Cilium v1.6.0](https://github.com/cilium/cilium/releases)
and [Gateway API v1.1.0](https://github.com/kubernetes-sigs/gateway-api/releases/tag/v1.1.0).
{{< /alert >}}

## Overview

In the role-oriented design of the Gateway API,
the infrastructure provider provisions a `GatewayClass` which the cluster operators can use to create different
`Gateway` resources.
Application developers can then connect to this `Gateway` using `HTTPRoute`s connecting to plain old `Service`s.

{{< mermaid-init >}}

{{< mermaid-chart >}}
{{< readfile-rel file="resources/diagrams/gateway-api.mmd" >}}
{{< /mermaid-chart >}}

Comparing this with the Ingress API we see that the `Ingress` resource has been split into the `Gateway` and `HTTPRoute`
objects with different responsibilities.

## Gateway API

Kubernetes 1.30 doesn't ship with the Gateway API CRDs (Custom Resource Definitions),
we therefore need to add them ourselves.
The Gateway API is split up into several components, 
and the SIG maintains [releases](https://github.com/kubernetes-sigs/gateway-api/releases) with both standard and 
experimental installs that combines several of these components that should work with all compliant Gateway 
implementations.

You are however free choose which components you want,
and Cilium [recommends](https://docs.cilium.io/en/stable/network/servicemesh/gateway-api/gateway-api/#prerequisites)
applying the following Gateway CRDs

```shell
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.1.0/config/crd/standard/gateway.networking.k8s.io_gatewayclasses.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.1.0/config/crd/experimental/gateway.networking.k8s.io_gateways.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.1.0/config/crd/standard/gateway.networking.k8s.io_httproutes.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.1.0/config/crd/standard/gateway.networking.k8s.io_referencegrants.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.1.0/config/crd/standard/gateway.networking.k8s.io_grpcroutes.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/v1.1.0/config/crd/experimental/gateway.networking.k8s.io_tlsroutes.yaml
```

Since we want to use
the [GatewayInfrastructure](https://gateway-api.sigs.k8s.io/reference/spec/#gateway.networking.k8s.io/v1.GatewayInfrastructure)
field to set the `Gateway` LoadBalancer `Service` IP using an annotation we've changed to the experimental `Gateway` CRD
since the standard CRD doesn't support that field, and Cilium has support for this feature.

Alternatively, you can use the full experimental Gateway CRD install which should also work

```shell
kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v1.1.0/experimental-install.yaml
```

## Cilium

[Cilium v1.16](https://github.com/cilium/cilium/releases/tag/v1.16.0) supports Gateway API v1.1.
If you're stuck with [Cilium v1.15](https://github.com/cilium/cilium/releases/tag/v1.15.0) you should use Gateway API
v1.0.

To get started, Cilium has a page
on [Migrating from Ingress to Gateway](https://docs.cilium.io/en/stable/network/servicemesh/ingress-to-gateway/ingress-to-gateway/#gs-ingress-to-gateway).

Following the [documentation of Cilium](https://docs.cilium.io/en/stable/network/servicemesh/gateway-api/gateway-api/)
we can enable Gateway support in one of two ways,
either with the Cilium-CLI ([&geq; v0.15](https://github.com/cilium/cilium-cli/releases))

```shell
cilium install --version 1.16.0 \
    --set kubeProxyReplacement=true \
    --set gatewayAPI.enabled=true \
    --set envoy.securityContext.capabilities.keepCapNetBindService=true
```

or using the Helm Chart as described in the [summary](#summary) section.

Note that with the dedicated L7 Envoy Proxy `DaemonSet` enabled by default in Cilium v1.16.0,
you also have to set `envoy.securityContext.capabilities.keepCapNetBindService` to true.

If you're in an environment where you can't use `LoadBalancer` type `Services` it's now also possible to run in
[host network mode](https://docs.cilium.io/en/stable/network/servicemesh/gateway-api/gateway-api/#host-network-mode) by
adding either `--set gatewayAPI.hostNetwork.enabled=true` to the `cilium install` command above,
or

```yaml
gatewayAPI:
  enabled: true
  hostNetwork:
    enabled: true
```

in the Helm values.

If you plan to use port numbers less than 1024
— e.g. 443 for HTTPS-traffic,
in host network mode,
you also need to add the
`NET_BIND_SERVICE` [Linux capability](https://man7.org/linux/man-pages/man7/capabilities.7.html) in the Envoy
_securityContext_.
We've done in the Helm Chart described in the [summary](#summary).

Note that the Envoy proxy should also have the `NET_ADMIN` and `SYS_ADMIN` capabilities enabled.
If you're running with Linux Kernel &geq; 5.8, and [CRI-O](https://cri-o.io/) &geq; 1.22.0 
or [containerd](https://containerd.io/) &geq; 1.5.0,
you can replace `SYS_ADMIN` with the `BPF` and `PERFORM` capabilities as noted in
the [Helm Chart value.yaml file comments](https://github.com/cilium/cilium/blob/4e012f493ed7eda2a745be9dd22db5503ed06765/install/kubernetes/cilium/values.yaml#L2319).

See
the [Cilium Gateway API documentation](https://docs.cilium.io/en/stable/network/servicemesh/gateway-api/gateway-api/)
for more information.

## Cert-manager

Gateway API support is a beta feature in the latest stable release of [Cert-manager](https://cert-manager.io/) at the
time of writing (v1.15.2).
To enable this support we add the `--enable-gateway-api` flag on startup of Cert-manager.
This is done by setting it as an extra argument
when [installing Cert-manager using its Helm Chart](https://cert-manager.io/docs/installation/helm/)

```shell
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager --version v1.15.2 \
    --namespace cert-manager --set crds.enabled=true --create-namespace \
    --set "extraArgs={--enable-gateway-api}"
```

If you don't want to enable Gateway API support in Cert-manager you can instead manually create a `Certificate` resource
and reference the TLS-secrets generated by that in the `Gateway` resource.

## Configuration

Once we have the Gateway API CRDs available and enabled support for it in Cilium and Cert-manager we can finally start
to create the resources we need to utilise it.

### Infrastructure Provider

If a cluster wide `GatewayClass` resource referencing Cilium is not already present (`kubectl get gatewayclasses`) we
need to create one ourselves[^1]

[^1]: In the [Isovalent Cilium Gateway API lab](https://isovalent.com/labs/gateway-api/) this `GatewayClass` is already
created for you.

{{< highlight yaml "linenos=table,hl_lines=5 7" >}}
{{< readfile-rel file="resources/gateway/gateway-class.yaml" >}}
{{< /highlight >}}

Take note of the `GatewayClass` *name* (line 5) and make sure of the *controllerName* on line 7.

If the `GatewayClass` is created successfully you should be able to view the supported features by running

```shell
kubectl describe gatewayclass cilium
```

### Cluster Operator

For convenience, we'll group the  _cluster operator_ related resources in the `gateway` namespace.
This allows us an easy overview of our gateways and connected resources as cluster operators.

```shell
kubectl create ns gateway
```

#### TLS certificates (Cloudflare)

To automatically provision TLS certificates attached to our `Gateway` we can create a Cert-manager `Issuer` resource.
This section is optional if you don't want certificates,
though it's highly recommended!

For details on how to automatically provision wildcard certificates using Cert-manager and Let's Encrypt I've summarised
the process in a previous article
on [Traefik Wildcard Certificates]({{< ref "/articles/2023/12/traefik-wildcard-certificates" >}}),
so I'll allow myself to be brief here.

Obtain a Cloudflare API token (or from you supported DNS provider of choice) as mentioned in the above article and
create a `Secret` for it

{{< highlight yaml "linenos=table,hl_lines=5 9" >}}
{{< readfile-rel file="resources/gateway/cloudflare-api-token.yaml" >}}
{{< /highlight >}}

We can then reference this secret in an `Issuer` resource (line 17) which enables us to complete a DNS-01 challenge that
allows us to issue wildcard certificates for the proven domain.
Remember to provide the domain owner e-mail on line 10.

{{< highlight yaml "linenos=table,hl_lines=5 10 17" >}}
{{< readfile-rel file="resources/gateway/cloudflare-issuer.yaml" >}}
{{< /highlight >}}

#### Gateway

Next we create a `Gateway` resources that references the _cilium_ `GatewayClass` (line 10).

{{< highlight yaml "linenos=table,hl_lines=5 8 10 15 19" >}}
{{< readfile-rel file="resources/gateway/gateway.yaml" >}}
{{< /highlight >}}

The annotation on line 8 is picked up by Cert-manager to automatically create a `Certificate` resource similar to the
one below

{{< highlight yaml "linenos=table,hl_lines=9 11-13 17" >}}
{{< readfile-rel file="resources/gateway/certificate.yaml" >}}
{{< /highlight >}}

If you didn't enable Gateway API support in Cert-manager,
you can instead create this resource manually.

The `Certificate` uses the _cloudflare-issuer_ `Issuer` (lines 11–13) and creates a TLS-`Secret` with a name (line 17)
corresponding to the one requested by the `Gateway` resource (line 19).

We need to create at least one listener per `Gateway` that listens for e.g. `HTTPRoute` resources that matches.
In our case we've created an HTTPS-listener on port 443 that matches all subdomains.
The _tls_-field of the Gateway is picked up by Cert-manager which will create a TLS-secret with the given name when an
`HTTPRoute` is attaches.
We allow eligible `HTTPRoutes` from all namespaces to connect through this `Gateway`,
though we can also create
a [selector](https://gateway-api.sigs.k8s.io/reference/spec/#gateway.networking.k8s.io/v1.RouteNamespaces) for more
fine-grained control.

#### Gateway Service

When the Gateway API is not run on the host network,
a `LoadBalancer` type `Service` is created when the `Gateway` is picked up by the Cilium controller.
Out-of-the box, this `Service` is assigned the next available IP from
e.g. [Cilium LB-IPAM]({{< ref "/articles/2023/12/migrating-from-metallb-to-cilium" >}}).

For a deterministic and idempotent configuration we need to set a pre-determined specific IP,
something we can do by annotating the spawned `Service` with `io.cilium/lb-ipam-ips: "<--IP-->"`.

As of Cilium 1.16.0, the only way I've found to manipulate the spawned `Service` from the `Gateway` is to add

```yaml
spec:
  infrastructure:
    annotations:
      io.cilium/lb-ipam-ips: "<--IP-->"
```

to the `Gateway` which requires the _experimental_ `Gateway` CRD we applied earlier.

This approach was introduced to me by [u/h_hover](https://www.reddit.com/user/h_hoover/) in
a [Reddit comment](https://www.reddit.com/r/kubernetes/comments/18pone8/comment/keqh9wu/).

In another thread on Reddit, [u/nuskovg](https://www.reddit.com/r/kubernetes/comments/18pone8/comment/keqhvlj/)
points to
the [GatewayAddress field](https://gateway-api.sigs.k8s.io/reference/spec/#gateway.networking.k8s.io/v1.GatewayAddress)
used by the [GatewaySpec](https://gateway-api.sigs.k8s.io/reference/spec/#gateway.networking.k8s.io/v1.GatewaySpec) as a
possible solution.
The support for this field is extended,
and [u/TheGarbInC](https://www.reddit.com/r/kubernetes/comments/18pone8/comment/kersfrp/) mentions an
open [GitHub issue](https://github.com/cilium/cilium/issues/21926) for Cilium support which is yet undecided.

#### DNS

Now that you've got your `Gateway` and attached `LoadBalancer` `Service` set up you want to point you DNS to the
`Service` IP.
This IP address should be the same as what you set in the `Gateway` infrastructure annotation field
(`io.cilium/lb-ipam-ips`), but to make sure you can run

```shell
kubectl get svc -A | grep LoadBalancer
```

and find the `External IP` of the `Service` named `<GatewayClass.name>-gateway-<Gateway.name>`,
in our case `cilium-gateway-cilium-gateway`.[^2]

[^2]: Knowing about this scheme, I hope you pick better names for your `Gateway` and `GatewayClass` resources.

Open up port `443` in your router and/or firewall to the `Service` IP and point you DNS to your public IP.
If you don't have your public IP at hand you can find it by running

```shell
dig +short myip.opendns.com @resolver1.opendns.com
```

In case you don't have the possibility to open ports,
— e.g. behind a [CGNAT](https://en.wikipedia.org/wiki/Carrier-grade_NAT),
you can try using a tunnel like [cloudflared](https://github.com/cloudflare/cloudflared).

If everything is set up correctly,
an external web request should roughly take the following path:

{{< mermaid-chart >}}
{{< readfile-rel file="resources/diagrams/external-request.mmd" >}}
{{< /mermaid-chart >}}

First the hostname is looked up in a DNS.
The DNS should respond with the IP you set up,
and the request is relayed to your router.
Next the router port forwards the request to the `Service` IP connected to the `Gateway`.
The `Gateway` then presents the attached certificate and the journey continues.

{{< mermaid-chart >}}
{{< readfile-rel file="resources/diagrams/gateway-routing.mmd" >}}
{{< /mermaid-chart >}}

From the `Gateway` the request is channeled to the correct `HTTPRoute`
– route ɑ in this case,
based on the rules you've set up, e.g. hostname or header-matching.
Next, the `HTTPRoute` directs the request to its attached `Service`,
which then finally delivers the request to its destination.
Hopefully the application responds with [something nice](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/418).

If you don't want to expose your public IP,
you can instead use a service
like [`cloudflared`](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/) to tunnel traffic
directly to your cluster.
If you want to go this route you can find an example
configuration [here](https://gitlab.com/vehagn/mini-homelab/-/blob/f1330cf1a19402297e77cdb7b273897c166716ee/infra/cloudflared/cloudflared-config.yaml#L16).

### Application developer

Now that both the infrastructure provider and cluster operator have done their job (*kudos to you!*),
we can let the application developers (also you) take the centre stage.

Given a `Service` named `my-service` we can create a simple`HTTPRoute` referencing our `Gateway` (line 9) and the 
`Service` (line 19) to expose the `Service`

{{< highlight yaml "linenos=table,hl_lines=9 12 19" >}}
{{< readfile-rel file="resources/gateway/http-route.yaml" >}}
{{< /highlight >}}

`HTTPRoutes` also allows developers to easily
do [header based routing](https://gateway-api.sigs.k8s.io/guides/http-routing/) for canary deployments,
or [traffic splitting](https://gateway-api.sigs.k8s.io/guides/traffic-splitting/) for blue-green testing.

I strongly encourage you to take a look all the capabilities on
the [Gateway API user guide](https://gateway-api.sigs.k8s.io/guides/) for more ideas.

## Summary

I'm running [Argo CD with Kustomize + Helm]({{< ref "/articles/2023/09/argocd-kustomize-with-helm" >}}) in an attempt to
follow GitOps best-practices.
This summary assumes a similar setup together with [Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets).
My full homelab configuration as of the writing of this article can be found
on [GitHub as a reference](https://github.com/vehagn/homelab/tree/d1ae9a189f4470613dfc14606a8eda53509d9cf9).
All the resources below can also be in the GitLab repository backing this 
site [here](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2023/012/cilium-gateway-api/resources).

### Gateway API

We gather all resources related to the `Gateway` in one namespace.
This includes the Cert-manager `Issuer`.

{{< highlight yaml >}}
{{< readfile-rel file="resources/gateway/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/gateway/gateway-class.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/gateway/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/gateway/sealed-cloudflare-api-token.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/gateway/cloudflare-issuer.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/gateway/gateway-with-infrastructure.yaml" >}}
{{< /highlight >}}

### Cilium

Cilium is configured to use v1.15.0 to support `Gateway` `Service` annotation which works in conjunction with
Cilium LB-IPAM and L2 announcements.

{{< highlight yaml >}}
{{< readfile-rel file="resources/cilium/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cilium/values.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cilium/announce.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cilium/ip-pool.yaml" >}}
{{< /highlight >}}

### Cert-manager

{{< highlight yaml >}}
{{< readfile-rel file="resources/cert-manager/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cert-manager/ns.yaml" >}}
{{< /highlight >}}

### Cloudflared

For completeness’s sake this is the
relevant [cloudflared](https://developers.cloudflare.com/cloudflare-one/connections/connect-networks/) config I'm
currently running.

{{< highlight yaml >}}
{{< readfile-rel file="resources/cloudflared/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cloudflared/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cloudflared/config.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cloudflared/credentials.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cloudflared/daemon-set.yaml" >}}
{{< /highlight >}}

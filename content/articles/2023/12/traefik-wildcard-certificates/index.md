---
title: "Wildcard Certificates with Traefik"
date: 2023-12-20T12:00:00+02:00

categories:
  - posts
  - kubernetes
  - network

tags:
  - kubernetes
  - traefik
  - cloudflare
  - cert-manager
  - let's encrypt
  - https
  - tls
  - acme
  - networking
---

In this article we'll explore how to use [Traefik](https://traefik.io/) in [Kubernetes](https://kubernetes.io/) combined
with [Cert-manager](https://cert-manager.io/) as 
an [ACME](https://en.wikipedia.org/wiki/Automatic_Certificate_Management_Environment)
(Automatic Certificate Management Environment) client to issue certificates 
through [Let's Encrypt](https://letsencrypt.org/).

If instead of Kubernetes you're running [docker-compose](https://docs.docker.com/compose/),
Major Hayden has an excellent tutorial on how to
configure [Wildcard LetsEncrypt certificates with Traefik and Cloudflare](https://major.io/p/wildcard-letsencrypt-certificates-traefik-cloudflare/).

In order to issue wildcard certificates we need to prove to
a [Certificate Authority](https://en.wikipedia.org/wiki/Certificate_authority) (CA) that we own the domain.
One way to prove ownership is with a [DNS-01 challenge](https://letsencrypt.org/docs/challenge-types/#dns-01-challenge).

We'll be using [Cloudflare](https://www.cloudflare.com/) as a DNS provider,
but the examples should be easily adapted for other providers.

## Overview

There's a lot of moving parts when dealing 
with [TLS/SSL certificates](https://www.cloudflare.com/en-gb/learning/ssl/what-happens-in-a-tls-handshake/) and
[public key infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure) which we'll not cover here.

Below is a simplified overview of the certificate issuance flow we'll implement.
If you feel the explanation is lacking or just plain wrong I'd be happy to hear from you!

{{< mermaid-init >}}

{{< mermaid-chart >}}
flowchart RL
  
subgraph LE[Let's Encrypt]
  CA
end
subgraph Cloudflare
  TXT
end
subgraph Issuer
  Token[API Token]
end
subgraph Certificate
  TLS[TLS Secret]
end
subgraph Traefik
  Ingress
end

Issuer --> LE
Token --> TXT
LE --> |DNS-01|Cloudflare
CA --> Issuer
Certificate --> Issuer
Traefik --> TLS
{{< /mermaid-chart >}}


1. The `Issuer` asks Let's Encrypt for a DNS-01 challenge.
2. Using the Cloudflare API Token the `Issuer` creates a TXT DNS record as an answer to the DNS-01 challenge.
3. Let's Encrypt verifies the TXT record to establish trust.
4. After verification Let's Encrypt will now sign certificate requests from the `Issuer`.
5. The `Certificate` resource asks the `Issuer` for a valid TLS certificate and stores it as a `Secret`.
6. Traefik attaches the TLS `Secret` to eligible `Ingress` and `IngressRoute` resources.

## Traefik Proxy

In their own words, 
> _[Traefik](https://doc.traefik.io/traefik/) is an open-source Edge Router that makes publishing your
> services a fun and easy experience_.

On Kubernetes Traefik can be installed using their [Helm Chart](https://github.com/traefik/traefik-helm-chart)

```shell
helm repo add traefik https://traefik.github.io/charts
helm repo update
helm install traefik traefik/traefik
```

I've described a different approach suitable for Argo CD and Kustomize in the [Summary](#summary) section.

## Cert-manager

While Traefik has native Let's Encrypt integration,
running multiple instances can raise some issues.
To solve those issues we can either pay for [Traefik Enterprise](https://traefik.io/traefik-enterprise/),
or delegate the certificate lifecycle management to a centralised service,
e.g. [Cert-manager](https://cert-manager.io/).

Cert-manager keeps a list of supported DNS-01 providers
on [their webpage](https://cert-manager.io/docs/configuration/acme/dns01/#supported-dns01-providers).[^1]
Luckily for us Cloudflare is one of those providers,
though any of
the [providers](https://community.letsencrypt.org/t/dns-providers-who-easily-integrate-with-lets-encrypt-dns-validation/86438)
listed by Let's Encrypt should be possible to integrate with.

[^1]: Curiously Traefik's list of [supported DNS-01 providers](https://doc.traefik.io/traefik/https/acme/#providers) 
that can automate DNS verification is longer than Cert-manager's.

We can install Cert-manager by using their Helm Chart

```shell
helm repo add jetstack https://charts.jetstack.io
helm repo update
helm install cert-manager jetstack/cert-manager \
  --namespace cert-manager --create-namespace \
  --version v1.13.3 --set installCRDs=true
```

For other options check the [Summary](#summary) section
or [Cert-manager's docs](https://cert-manager.io/docs/installation/).

## DNS Provider (Cloudflare)

I went with Cloudflare as a DNS provider since they're well known and offer a free tier.
Creating an account and setting up your site to use Cloudflare is outside the scope of this article,
but it should be pretty straightforward by going to [their site](https://www.cloudflare.com/).

Cert-manager has great documentation on how
to [configure DNS-01 providers](https://cert-manager.io/docs/configuration/acme/dns01/).
For [Cloudflare](https://cert-manager.io/docs/configuration/acme/dns01/cloudflare/) we only need an API-token with the
appropriate permissions.

After logging in to Cloudflare navigate to **{{< icon "account" >}}My Profile** in the upper right corner,
find the **{{< icon "code-braces" >}}API Tokens** pane in the left sidebar,
and locate the **Create Token** button under User API Tokens.

Create a custom token and give it the following permissions

* Zone – Zone – Read
* Zone – DNS – Edit

For the Zones Resources it's recommended to include all zones.

Take note of the token as we need it in the next section.

## Configuration

With a Cloudflare API token in hand,
and having set up both Traefik and Cert-manager,
we're finally ready to configure
wildcard [certificates for use by Traefik](https://github.com/traefik/traefik-helm-chart/blob/master/EXAMPLES.md#provide-default-certificate-with-cert-manager-and-cloudflare-dns)!

First we create a secret[^2] with the API token we got from Cloudflare.
Using `stringData` instead of `data` allows us to use clear text content instead of having to `base64` encode the
token.[^3]

[^2]: Check out [Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets) if you plan on doing it the GitOps way.
[^3]: [Kubernetes Secret resource documentation](https://kubernetes.io/docs/concepts/configuration/secret/)

{{< highlight yaml "linenos=table,hl_lines=4 8" >}}
apiVersion: v1
kind: Secret
metadata:
  name: cloudflare-api-token
  namespace: traefik
type: Opaque
stringData:
  api-token: "<--CLOUDFLARE API TOKEN-->"
{{< / highlight >}}

Then we create an `Issuer` resource providing the domain owner e-mail on line 9 and reference the API token 
secret for `apiTokenSecretRef` (line 16).

For testing purposes it might be a good idea to
use [Let's Encrypt staging server](https://letsencrypt.org/docs/staging-environment/) instead of the production server
to  avoid being rate limited.
To do so change the `server` on line 8 to `https://acme-staging-v02.api.letsencrypt.org/directory`.

{{< highlight yaml "linenos=table,hl_lines=4 8-9 16" >}}
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: cloudflare-issuer
  namespace: traefik
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: "<--YOUR EMAIL-->"
    privateKeySecretRef:
      name: cloudflare-key
    solvers:
      - dns01:
          cloudflare:
            apiTokenSecretRef:
              name: cloudflare-api-token
              key: api-token
{{< / highlight >}}

Next we create a `Certificate` which references the above `Issuer` by name on line 12,
inserting your own domain name on line 4, 7, 9, and 10 where appropriate.

{{< highlight yaml "linenos=table,hl_lines=4 7 9-10 12" >}}
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: wildcard-<--YOUR DOMAIN-->
  namespace: traefik
spec:
  secretName: wildcard-<--YOUR DOMAIN-->-tls
  dnsNames:
    - "<--YOUR DOMAIN-->"
    - "*.<--YOUR DOMAIN-->"
  issuerRef:
    name: cloudflare-issuer
    kind: Issuer
{{< / highlight >}}

Verify that everything is ready by running

```shell
$ kubectl get certificate -n traefik

NAME             READY   SECRET               AGE
wildcard-<...>   True    wildcard-<...>-tls   5m
```

If the certificate is not ready, the logs of the Cert-manager pod can give you more information.

### Default certificate

Assuming everything is running fine you can start using the certificate by adding

{{< highlight yaml "linenos=table,hl_lines=4 7 9-10 12" >}}
tlsStore:
  default:
    defaultCertificate:
      secretName: wildcard-<--YOUR DOMAIN-->-tls
{{< / highlight >}}

to the Traefik Helm Chart `values.yaml`.

This instructs Traefik to present the TLS `Secret` generated by the `Certificate` resource by default for all `Ingress`
and `IngressRoute` resources.

Update Traefik to apply the new settings

```shell
helm upgrade traefik traefik/traefik --values values.yaml
```

and you should be all set!

### Per Ingress

It's also possible to issue certificates per `Ingress` or `IngressRoute` resource.
For instructions on how to do this you can 
read [this blog post by Traefik](https://traefik.io/blog/secure-web-applications-with-traefik-proxy-cert-manager-and-lets-encrypt/)
where they use [Cert-manager annotations](https://cert-manager.io/docs/usage/ingress/) to request certificates.

## Summary

I'm running [Argo CD with Kustomize + Helm]({{< ref "/articles/2023/09/argocd-kustomize-with-helm" >}}) in an attempt to 
follow GitOps best-practices.

I'm employing 
an [App of Apps Pattern](https://argo-cd.readthedocs.io/en/stable/operator-manual/cluster-bootstrapping/#app-of-apps-pattern)
where I'm using Argo CD's `ApplicationSet` to generate `Application` resources for each folder.
You can find my current homelab configuration 
on [GitHub](https://github.com/vehagn/homelab/blob/main/infra/application-set.yaml) as a demonstration.

### Cert-manager

```yaml
#cert-manager/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - ns.yaml

helmCharts:
  - name: cert-manager
    repo: https://charts.jetstack.io
    version: 1.13.3
    releaseName: cert-manager
    namespace: cert-manager
    valuesInline:
      crds.enabled: true
```

```yaml
#cert-manager/ns.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: cert-manager
```

### Traefik

```yaml
#traefik/kustomization.yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization

resources:
  - ns.yaml
  - cloudflare-token-cert-manager.yaml
  - cloudflare-issuer.yaml
  - cloudflare-cert.yaml

helmCharts:
  - name: traefik
    repo: https://traefik.github.io/charts
    version: 26.0.0
    releaseName: traefik
    namespace: traefik
    includeCRDs: true
    valuesFile: values.yaml
```

```yaml
#traefik/values.yaml
tlsStore:
  default:
    defaultCertificate:
      secretName: wildcard-<--YOUR DOMAIN-->-tls
```

```yaml
#traefik/ns.yaml
apiVersion: v1
kind: Namespace
metadata:
  name: traefik
```

This should preferably be a [`SealedSecret`](https://github.com/bitnami-labs/sealed-secrets)[^2] instead of an 
unencrypted `Secret`

```yaml
#traefik/cloudflare-token-cert-manager.yaml
apiVersion: v1
kind: Secret
metadata:
  name: cloudflare-api-token
  namespace: traefik
type: Opaque
stringData:
  api-token: "<--CLOUDFLARE API TOKEN-->"
```

```yaml
#traefik/cloudflare-issuer.yaml
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: cloudflare-issuer
  namespace: traefik
spec:
  acme:
    server: https://acme-v02.api.letsencrypt.org/directory
    email: "<--YOUR EMAIL-->"
    privateKeySecretRef:
      name: cloudflare-key
    solvers:
      - dns01:
          cloudflare:
            apiTokenSecretRef:
              name: cloudflare-api-token
              key: api-token
```

```yaml
#traefik/cloudflare-cert.yaml
apiVersion: cert-manager.io/v1
kind: Certificate
metadata:
  name: wildcard-<--YOUR DOMAIN-->
  namespace: traefik
spec:
  secretName: wildcard-<--YOUR DOMAIN-->-tls
  dnsNames:
    - "<--YOUR DOMAIN-->"
    - "*.<--YOUR DOMAIN-->"
  issuerRef:
    name: cloudflare-issuer
    kind: Issuer
```
---
title: "Theme Dynamic Images"
date: 2023-11-19T12:00:00+02:00

categories:
  - posts
  - hugo
  - blowfish
tags:
  - images
  - hugo
  - theme
  - blowfish
  - dark mode
  - light mode
  - responsive
  - dynamic theme
---

In a [recent article]({{< ref "/garage/2023/10/mo-unit-wiring" >}}) I wanted one of the images to change
dynamically with the theme.

I didn't find any support for this in the [Blowfish Hugo theme](https://blowfish.page/docs/) I'm using,
so I had to get creative.
I also wanted to reuse an earlier shortcode I experimented with for automatically resizing images.

The end result is a markup render-image hook and a custom dynamic-image shortcode that both use the same image partial.

## Motivation

We've generated two similar images using [ImageMagick](https://imagemagick.org/).

A dark version
![Hello](images/01-hello-dark.gif "Dark grey 'HELLO'")
```shell
 magick -background none -fill "#333" -size 1000x250 -gravity center \
        -stroke  white -strokewidth 2 label:HELLO 01-hello-dark.gif
```

... and a light version
![Hello](images/01-hello-light.gif "Light grey 'HELLO'")

```shell
 magick -background none -fill "#CCC" -size 1000x250 -gravity center \
        -stroke  black -strokewidth 2 label:HELLO 01-hello-light.gif
```

The markdown for rendering these images are

```markdown
![Hello](images/01-hello-dark.gif "Dark grey 'HELLO'")
```

```markdown
![Hello](images/01-hello-light.gif "Light grey 'HELLO'")
```

We want the dark image to show when we're using a dark theme,
and the light image to show when we're using a light theme.
We also want to be able to toggle between both versions independent of which theme we're using.

By using the shortcode defined below this boils down to

```go-html-template
{{</* dynamic-image title="title" alt="alt" dark="images/01-hello-dark.gif" light="images/01-hello-light.gif" */>}}
```

which should render "HELLO" in either dark grey for the dark theme,
or light grey if you like to burn your retinas.

{{< dynamic-image title="Theme dynamic 'HELLO'. Click the ☾/☼ symbol to change." alt="HELLO" id="hello" dark="images/01-hello-dark.gif" light="images/01-hello-light.gif" >}}

We can also statically render both images using regular markdown image syntax.

## Implementation

So how is this all tied together?

By creating a partial which we invoke in both our markup render image hook and our dynamic image shortcode we can avoid
duplicating code.
This should make it easier to maintain and refine by only having to change code one place.

### Partial

We've created the following partial under `layouts/partials` as `image.html`

{{< highlight go-html-template >}}
{{< readfile file="/layouts/partials/image.html" >}}
{{< /highlight >}}

Since we don't have access to the `.Page` attribute in a partial we have to supply it manually together with the `url`
to the image, a `title`, an `alt`-attribute, and an (optional) `id`.
We then use the `.Page` element to do to a lookup for the image in both page- and global space.

The optional `dynamic` flag is used to render a button-element which is used to change images individually.

Bitmap images are resized to conserve bandwidth for smaller screens while simultaneously offering higher quality images
for bigger screens using 
the [`srcset`-attribute](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images).

### Markup render image hook

Next we override the markup image render hook by creating `render-image.html` under `layouts/_default/_markup` and 
invoke our image partial.

{{< highlight go-html-template >}}
{{< readfile file="/layouts/_default/_markup/render-image.html" >}}
{{< /highlight >}}

We supply the partial with the `.Page` attribute and parse the `url` from the `.Destination` attribute supplied from
the markup code.
The `title` and `alt` variables are taken directly from the `.Title` and `.Text` markup properties respectively.

### Dynamic image shortcode

The first part of the dynamic image shortcode is fairly similar to the markup render image hook.
We provide `title` and `alt`-attributes to be shared by both the dark and light version and supply separate URLs for 
the `dark` and `light` images.
We're basically calling the `image` partial twice with slightly different input.

{{< highlight go-html-template >}}
{{< readfile file="/layouts/shortcodes/dynamic-image.html" >}}
{{< /highlight >}}

The responsiveness happens in the `<script>` part where we hide certain elements based on their ID and the current theme
or selection.

To stop two different dynamic images interfering with each other we define unique function names for each of them.
This is done by taking the `md5`-hash of the ID to get a legal function name and pass it to `safeJS` to remove the 
quotation marks around the result.

In the script we're using 
the [Blowfish Theme function `getTargetAppearance()`](https://github.com/search?q=repo%3Anunocoracao/blowfish%20getTargetAppearance&type=code)
to get the current theme.
The `appearance-switcher` and `appearance-switcher-mobile` elements are also Blowfish specific.

Other themes might have similar functions and elements that can be used if you want to port this functionality.

The `{{ $id }}-dark-button` and `{{ $id }}-light-button` elements are the optional "dynamic" button from the `image`
partial.
By creating an event listeners on these elements we can change the `hidden` attribute for the corresponding image.

At the end of the script we're invoking the `change{{ $id | md5 | safeJS }}()`-function once in order to hide the
unwanted image.
This means that if the reader has disabled Javascript both the dark and light image will show.

## Other uses

The images could also be completely different and could give a different experience based on which theme you've chosen,
though in the current iteration they would share the same title and alt-text.

Having a versatile partial should make further image-centric shortcodes easier to write.

{{< dynamic-image title="Danbo either drawing a heart or enjoying the outdoors" alt="Danbo" id="danbo" dark="images/02-danbo-heart.jpg" light="images/02-danbo-rock.jpg" >}}

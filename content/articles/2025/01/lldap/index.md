---
title: "LLDAP — Declarative Selfhosted Lightweight Authentication"
date: 2025-01-26T14:00:00+02:00
#lastmod: 2024-07-10T12:45:00+02:00

categories:
  - posts
  - kubernetes

tags:
  - k8s
  - kubernetes
  - homelab
  - auth
  - authentication
  - ldap
  - ldaps
  - lldap
  - kustomize
---

Selfhosting multiple applications often means having to deal with disparate user accounts unless you can integrate them
with a common third party.
A tried and tested framework for this is the ubiquitous LDAP,
or [Lightweight Directory Access Protocol](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol), server.

The LDAP server functions as a
centralised [identity and access management (IAM)](https://en.wikipedia.org/wiki/Identity_and_access_management),
meaning each application sees it as a single source of truth.

## Motivation

[LDAP](https://ldap.com/) is a complex protocol with its own [terminology](https://ldap.com/glossary-of-ldap-terms/)
and [filter syntax](https://www.ldapexplorer.com/en/manual/109010000-ldap-filter-syntax.htm).
A common LDAP implementation is Microsoft's [Active Directory](https://en.wikipedia.org/wiki/Active_Directory),
or their cloud based [Entra ID](https://www.microsoft.com/en-gb/security/business/identity-access/microsoft-entra-id/)
offering.

For homelab use you probably only need the bare minimum of the LDAP features.
I've found the features offered by [LLDAP](https://github.com/lldap/lldap) to more than meet my needs.
Other alternatives that LLDAP suggests themselves are [OpenLDAP](https://www.openldap.org/),
[FreeIPA](https://www.freeipa.org/) and [Kanidm](https://kanidm.com/).

LLDAP should integrate with most applications that support LDAP.
The LLDAP repository supplies ample [example integrations](https://github.com/lldap/lldap/tree/main/example_configs)
— including [Dex](https://github.com/dexidp/dex), [Gitea](https://about.gitea.com/),
[Proxmox](https://www.proxmox.com/), [Home Assistant](https://www.home-assistant.io/),
and [Authelia](https://www.authelia.com/).

Using a common [identity provider (IdP)](https://en.wikipedia.org/wiki/Identity_provider) alleviates the need to
individually maintain users in each application,
making it easier to add new users and change passwords everywhere at once.

A more modern approach would be to use
an [OAuth](https://oauth.net/2/)/[OIDC](https://openid.net/developers/how-connect-works/) client
— like [Keycloak](https://www.keycloak.org/), [Zitadel](https://zitadel.com/), [Authelia](https://www.authelia.com/),
[Authentik](https://goauthentik.io/), or [Kanidm](https://kanidm.com/), but not all applications natively support
OAuth/OIDC.
For applications that doesn't support OAuth/OIDC there's the option of using a middleware
like [OAuth2 Proxy](https://oauth2-proxy.github.io/oauth2-proxy/)
or [Traefik Forward Auth](https://github.com/thomseddon/traefik-forward-auth),
but that's an article for a later time.

Most of the aforementioned OAuth clients can act as a simple LDAP server themselves,
and all except Kanidm can fetch users from an external LDAP-server,
but in this article we're keeping it simple with only LDAP with LLDAP.

My motivation for choosing LLDAP is that it's simple and easy to get started,
as well as the possibility to declaratively configure groups and users using
a [bootstrap script](https://github.com/lldap/lldap/blob/main/scripts/bootstrap.sh).
As a nice bonus, LLDAP is written
in [Safe](https://doc.rust-lang.org/nomicon/meet-safe-and-unsafe.html) [Rust](https://www.rust-lang.org/),
which in theory should make it resilient
to [buffer overflow](https://owasp.org/www-community/attacks/Buffer_overflow_attack)
and [use-after-free](https://owasp.org/www-community/vulnerabilities/Using_freed_memory) attacks.

## Overview

[Evantage-WS](https://github.com/Evantage-WS/lldap-kubernetes) has already done a great job creating both a simple
example [Kubernetes](https://kubernetes.io/) deployment, and a [Helm](https://helm.sh/) chart for LLDAP.

In this article we will use [Kustomize](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/) to
configure LLDAP and explore most configuration and integration options available for LLDAP.

After configuring and verifying a basic deployment of LLDAP,
we will take a look at how we can bootstrap users and groups declaratively,
before configuring [SMTP](https://www.cloudflare.com/en-gb/learning/email-security/what-is-smtp/) for password
resetting.
Next we'll enable [Secure LDAP (LDAPS)](https://jumpcloud.com/blog/ldap-vs-ldaps) with the help
of [Cert manager](https://cert-manager.io/),
and lastly we'll integrate with an external database to
enable [high availability](https://en.wikipedia.org/wiki/High_availability).

This article assumes you have your own domain at `<DOMAIN>.<TLD>` which will be used as an example throughout the
article.

If you're not one for details you can skip to the [Summary](#summary)-section for the final manifests or visit the
[repository](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2025/01/lldap/resources) for this article.

## Basic Configuration

We'll leverage Kustomize's ability to reference the different manifests required to configure LLDAP in order to organise
everything

{{< highlight yaml "linenos=table, hl_lines=12" >}}
{{< readfile-rel file="resources/basic/kustomization.yaml" >}}
{{< /highlight >}}

LLDAP can be configured using either
a [configuration file](https://github.com/lldap/lldap/blob/main/lldap_config.docker_template.toml) or environment
variables.
To save some lines of YAML we'll take advantage of
[
`envFrom`](https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/#configure-all-key-value-pairs-in-a-configmap-as-container-environment-variables)
to map _[ConfigMap](https://kubernetes.io/docs/concepts/configuration/configmap/)_ data to container environment
variables.

We can utilise Kustomize's built-in
_[configMapGenerator](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/#configmapgenerator)_ to
automatically generate a _ConfigMap_ that holds some of
the LLDAP configuration environment variables.
Most important is the `LLDAP_LDAP_BASE_DN`-variable (line 12) which should be configured to your own domain.

Next, we declaratively define
the _[Namespace](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)_ for completeness' sake

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/basic/ns.yaml" >}}
{{< /highlight >}}

and prepare a _[Service](https://kubernetes.io/docs/concepts/services-networking/service/)_ referencing named ports
(line 14, 17) that we'll define shortly

{{< highlight yaml "linenos=table, hl_lines=14 17" >}}
{{< readfile-rel file="resources/basic/svc.yaml" >}}
{{< /highlight >}}

We also need to define cryptographic values preferably stored in a
_[Secret](https://kubernetes.io/docs/concepts/configuration/secret/)_-resource to hold a secure random
value designated `LLDAP_JWT_SECRET`
— used to sign the [JWTs](https://jwt.io/) issued by LLDAP,
and a seed called `LLDAP_KEY_SEED`
— used to hash passwords stored in the database.
The LLDAP configuration suggests to run

```shell
LC_ALL=C tr -dc 'A-Za-z0-9!#%&'\''()*+,-./:;<=>?@[\]^_{|}~' </dev/urandom | head -c 32; echo ''
```

in order to generate a random enough value for the JWT secret and at least 12 random characters for the hash seed.
Assuming you don't encrypt your secret using e.g. [Sealed Secrets](https://github.com/bitnami-labs/sealed-secrets),
or run an implementation of [Secrets Store CSI Driver](https://secrets-store-csi-driver.sigs.k8s.io/introduction),
the _Secret_ should look something like

{{< highlight yaml "linenos=table, hl_lines=14 17" >}}
{{< readfile-rel file="resources/basic/crypto.yaml" >}}
{{< /highlight >}}

Note that it's possible
— though computationally prohibitive,
to brute force user passwords if you know the key seed and have access to a database dump.

Lastly, we also need to define an admin user for our LLDAP instance

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/basic/admin-credentials.yaml" >}}
{{< /highlight >}}

With the auxiliary config created,
we're ready to design a _[Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)_ of LLDAP
that uses all the _ConfigMaps_ and _Secrets_ we just defined (line 22, 24, 26)

Note that we've picked the rootless version of LLDAP (line 19) for some extra security.
An even more hardened deployment can be found in the [Summary](#summary)-section.

{{< highlight yaml "linenos=table, hl_lines=19 22 24 26 28 30 33" >}}
{{< readfile-rel file="resources/basic/deployment.yaml" >}}
{{< /highlight >}}

We've also named port 3890 and 17170 which are, respectively, the default LDAP and web ports
(line 28, 30).
These can be changed with the `LLDAP_LDAP_PORT` and `LLDAP_HTTP_PORT` environment variables.

By defining everything declaratively
— see the [Bootstrapping](#bootstrapping-optional)-section,
we don't need to configure additional persistence,
and can thus use an emptyDir-volume for storing the LLDAP-data (line 33).

If you on the other hand want to store the state you have to define a
_[PersistentVolume](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)_ with an associated
_PersistentVolumeClaim_.
For easier _PersistentVolume_ provisioning you can take a look
at [Container Storage Interface (CSI)](https://kubernetes-csi.github.io/docs/) implementations like
the [Proxmox CSI Plugin]({{< ref "/articles/2024/06/k8s-proxmox-csi" >}}).

Another option for persistence is using an external database,
something we'll explore in the aptly named [External Database](#external-database-optional)-section later.

Applying the manifests by running

```shell
kubectl apply -k .
```

in the folder containing the _kustomization.yaml_-file should then deploy LLDAP.

## Testing

Assuming the deployment went OK,
we can now test our LDAP server.

In order to do this we first need to install some tools.
Depending on you OS/package manager,
these tools are bundled in either the openldap (brew),
ldap-utils (apt), or openldap-clients (dnf, yum, apk).

With the tools installed we need to create a route to our LDAP-server which can be done
by [port-forwarding](https://kubernetes.io/docs/tasks/access-application-cluster/port-forward-access-application-cluster/)
the service we just created by running[^1]

```shell
kubectl port-forward service/lldap 3890:389 -n lldap
```

[^1]: Note that the service is routing port 3890 form the pod to port 389,
and then we're port-forwarding port 389 from the service back to port 3890 on the client.

If everything is working as expected you should now be able to invoke

```shell
ldapsearch -H ldap://localhost:3890 \
  -D "uid=<USER>,ou=people,dc=<DOMAIN>,dc=<TLD>" \
  -w "<PASSWORD>" \
  -b "dc=<DOMAIN>,dc=<TLD>" "(objectClass=*)"
```

to list all objects stored in LLDAP.

Some LDAP-operations are not supported by LLDAP,
e.g.

```shell
ldapwhoami -H ldap://localhost:3890 \
  -D "uid=<USER>,ou=people,dc=<DOMAIN>,dc=<TLD>" \
  -w "<PASSWORD>" 
```

which will result in the following message

```shell
ldap_parse_result: Server is unwilling to perform (53)
        additional info: Unsupported extended operation: 1.3.6.1.4.1.4203.1.11.3
Result: Server is unwilling to perform (53)
Additional info: Unsupported extended operation: 1.3.6.1.4.1.4203.1.11.3
```

I assume this means that the functionality isn't implemented in LLDAP.

## Bootstrapping (Optional)

The default configuration for LLDAP only allows declaration of an admin user,
but they graciously supply
a [bootstrap](https://github.com/lldap/lldap/blob/main/example_configs/bootstrap/bootstrap.md)-option that lets us
configure as many users, groups, and custom attributes (schemas) we want[^2]

[^2]: At the time of writing it seems that it's not supported to bootstrap a value for a custom attribute,
only the attribute itself.

LDAP objects to be bootstrapped are defined as a [JSON](https://developer.mozilla.org/en-US/docs/Glossary/JSON)-object,
e.g. a user can be defined as

{{< highlight json "linenos=table" >}}
{{< readfile-rel file="resources/bootstrap/users/user.json" >}}
{{< /highlight >}}

where only the `id` and `email` fields are mandatory.

It's possible to define multiple users in a single file,
though not as a JSON-list, but as consecutive JSON-objects, e.g.

{{< highlight json "linenos=table" >}}
{{< readfile-rel file="resources/bootstrap/users/more-users.json" >}}
{{< /highlight >}}

Note that it's possible to omit the password,
in which case you need to configure password reset
— which we cover in the [next section](#smtp--password-reset-optional),
in order to set an initial password to be able to log in.

Groups are defined in a similar fashion as users

{{< highlight json "linenos=table" >}}
{{< readfile-rel file="resources/bootstrap/groups/groups.json" >}}
{{< /highlight >}}

Check
the [official documentation](https://github.com/lldap/lldap/blob/main/example_configs/bootstrap/bootstrap.md#user-and-group-schema-config-file-example)
for how to configure custom schemas.

We can easily use Kustomize's _configMapGenerator_ to create _ConfigMaps_ of the configuration files (line 9-11) to
mount them into a _Pod_

{{< highlight yaml "linenos=table, hl_lines=9-11 15 16" >}}
{{< readfile-rel file="resources/bootstrap/kustomization.yaml" >}}
{{< /highlight >}}

The bootstrap script also needs the http-url of LLDAP (line 15) which will be the service name assuming you run the
bootstrapping [_Job_](https://kubernetes.io/docs/concepts/workloads/controllers/job/) in the same _Namespace_.

Setting the `DO_CLEANUP` variable to true (line 16) will
_“delete groups and users not specified in config files, also remove users from groups that they do not belong to”_.

If you're using [Argo CD](https://argo-cd.readthedocs.io/) you can add the annotations on line 8 and 9 in order to
relaunch the job on every sync.
Read the Argo CD documentation for more information
about [resource hooks](https://argo-cd.readthedocs.io/en/stable/user-guide/resource_hooks/).

We can reuse the same image as the main deployment (line 15) which ship with the `bootstrap.sh`-script,
we just have to change the start-command as on line 16.
The bootstrap environment variables are injected from the `bootstrap-env`-_ConfigMap_ (line 19),
as well as mapping the admin credentials (line 21 and 26) from the `lldap-credentials`-_Secret_ (line 24 and 29).

{{< highlight yaml "linenos=table, hl_lines=8-9 15 16 19 21 24 26 29 34 37 43 47 53 57" >}}
{{< readfile-rel file="resources/bootstrap/bootstrap.yaml" >}}
{{< /highlight >}}

Next we create [projected volumes](https://kubernetes.io/docs/concepts/storage/projected-volumes/) (line 43 and 53) from
the `bootstrap-config`-_ConfigMap_ (line 47 and 57) containing the JSON-files we previously wrote,
and mount them at the correct locations (line 34 and 37).

## SMTP — Password Reset (Optional)

If you plan on hosting multiple personal accounts in your LLDAP instance it's a good idea to let them reset their
passwords themselves.
In order to enable this we need some kind of SMTP-integration for LLDAP to be able to send password reset e-mails.

It's possible to self-host a simple solution like [docker-postfix](https://github.com/bokysan/docker-postfix),
or something more complex like [mailcow](https://mailcow.email/), [Mox](https://www.xmox.nl/),
or [Dovecot](https://www.dovecot.org/), but setting up one of those are all articles in themselves.

I haphazardly picked [SendGrid](https://sendgrid.com/) which I found easy to integrate with LLDAP.
Take a look at this Hacker News [thread](https://news.ycombinator.com/item?id=39174199) for more options.

It should be fairly straight forward to create a free account with SendGrid and verify it with your domain under
**Settings** > **Sender Authentication** using DNS entries.

You can create a new sender e-mail, e.g. `no-reply@<DOMAIN>.<TLD>` under **Marketing** > **Senders** and find a guide on
how to integrate it with LLDAP under **Email API** > **Integration Guide** shown in the picture below

![SendGrid SMTP integration](images/sendgrid-integration.png "SendGrid integration wizard. Choose SMTP Relay.")

Choose the **SMTP Relay** option and take note of the API key you create.

Also make sure to disable tracking under **Settings** > **Tracking** to avoid overwriting links and adding images to
track opened e-mails.

![SendGrid tracking settings](images/sendgrid-tracking.png "SendGrid tracking settings. Disable link overwriting and opened e-mail tracking")

With your API-key (line 13) and e-mail address (line 14) in hand,
create the following _Secret_

{{< highlight yaml "linenos=table, hl_lines=13 14" >}}
{{< readfile-rel file="resources/lldap/smtp-settings.yaml" >}}
{{< /highlight >}}

and inject the environment variables in the LLDAP _Deployment_ by adding

```yaml
envFrom:
  - secretRef:
      name: smtp-settings
```

to the LLDAP-container.

We also need to configure the LLDAP base URL to be used in the password reset e-mail which is done with the
`LLDAP_HTTP_URL` environment variable which can be done using Kustomize's _configMapGenerator_

```yaml
configMapGenerator:
  - name: lldap-config
    namespace: lldap
    literals:
      - LLDAP_HTTP_URL="https://lldap.<DOMAIN>.<TLD>"
```

## LDAPS (Optional)

LDAPS is LDAP over TLS/SSL, and is highly recommended if you plan to expose your LDAP-server to the internet
— though exposing LDAP to the internet is not advisable,
or want an extra layer of security for inter-container communication.

Similar to [HTTPS](https://no.wikipedia.org/wiki/HTTPS) we need an [X.509](https://en.wikipedia.org/wiki/X.509)
certificate to enable the secure part.
The easiest way to obtain such a certificate is by using [Cert manager](https://cert-manager.io/).
I've already explained how to get started with Cert manager in
my [Traefik wildcard certificates]({{< ref "/articles/2023/12/traefik-wildcard-certificates" >}}) article,
so I'll allow myself to be brief here.

Assuming you've configured a _[ClusterIssuer](https://cert-manager.io/docs/concepts/issuer/)_ imaginatively called
`cluster-issuer` (line 13),
you can create the following _[Certificate](https://cert-manager.io/docs/usage/certificate/)_-resource

{{< highlight yaml "linenos=table, hl_lines=9 13 14" >}}
{{< readfile-rel file="resources/lldap/cert.yaml" >}}
{{< /highlight >}}

to issue a certificate for you chosen domain main (line 9) as
a _[TLS Secret](https://kubernetes.io/docs/concepts/configuration/secret/#tls-secrets)_ called `cert` (line 14).

We can then create a volume in our LLDAP _Deployment_ with this secret

```yaml
volumes:
  - name: cert
    secret:
      secretName: cert
      items:
        - key: tls.key
          path: tls.key
        - key: tls.crt
          path: tls.crt
```

and mount the volume in the LLDAP-container

```yaml
volumeMounts:
  - name: cert
    mountPath: /cert
```

Next we can configure LLDAP to enable LDAPS and use the certificate

```yaml
configMapGenerator:
  - name: lldap-config
    namespace: lldap
    literals:
      - LLDAP_LDAPS_OPTIONS__ENABLED="true"
      - LLDAP_LDAPS_OPTIONS__CERT_FILE="/cert/tls.crt"
      - LLDAP_LDAPS_OPTIONS__KEY_FILE="/cert/tls.key"
```

For TLS-communication to work inside the cluster with public certificate,
i.e. not self-signed certificates for the `cluster.local` domain
— which we in that case have to distribute,
we can create DNS-entries for our LLDAP-service.

We start by assigning a static `clusterIP` (line 9) to the LLDAP-service.
To [minimise the possibility of a Service Cluster IP conflict](https://kubernetes.io/docs/concepts/services-networking/cluster-ip-allocation/)
we can pick an IP in the lower band of the Service IP range, e.g. `10.96.0.15`

{{< highlight yaml "linenos=table, hl_lines=9 19-21" >}}
{{< readfile-rel file="resources/lldap/svc.yaml" >}}
{{< /highlight >}}

We also need to route to the LDAPS-port (line 19-21) by adding

```yaml
ports:
  - name: ldaps
    containerPort: 6360
```

to the LLDAP container.

You can now test that everything works by port-forwarding the ldaps-port

```shell
kubectl port-forward service/lldap 6360:636 -n lldap
```

and try to get the certificate information by running

```shell
openssl s_client -connect localhost:6360
```

If this works you can add an entry in your `/etc/hosts`-file

```shell
# /etc/hosts
127.0.0.1    lldap.<DOMAIN>.<TLD>
```

to route `lldap.<DOMAIN>.<TLD>` to our port-forward in order for the certificate to be valid.
If the following search returns OK everything should be configured properly

```shell
ldapsearch -H ldaps://lldap.<DOMAIN>.<TLD>:6360 \
  -D "uid=<USER>,ou=people,dc=<DOMAIN>,dc=<TLD>" \
  -w "<PASSWORD>" \
  -b "dc=stonegarden,dc=dev" "(objectClass=*)"
```

Clean up by stopping the port-forward and remove the `/etc/hosts` entry.

We can now create a rule in [CoreDNS](https://kubernetes.io/docs/tasks/administer-cluster/coredns/) to forward all
internal requests to `lldap.<DOMAIN>.<TLD>` to `10.96.0.15` by editing the Corefile-config by running

{{< alert >}}
CoreDNS is an integral part of Kubernetes, an invalid config can potentially break your cluster until resolved.
{{< /alert >}}

```shell
kubectl edit configmap coredns -n kube-system
```

and adding the following `hosts` block in the Corefile-config

```yaml
data:
  Corefile: |-
    .:53 {
        ...
        hosts { 
            10.96.0.15 lldap.<DOMAIN>.<TLD>
            fallthrough 
        }
        ...
    }
```

Restart the CoreDNS pods to apply the configuration changes

```shell
kubectl rollout restart deployment coredns -n kube-system
```

If you can't
— or don't want to,
edit the CoreDNS config you can instead add a host alias to the _Pods_ that should talk ldaps with LLDAP, e.g.

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: <DEPLOYMENT_NAME>
spec:
  template:
    spec:
      hostAliases:
        - ip: 10.96.0.15
          hostnames:
            - lldap.<DOMAIN>.<TLD>
```

## External Database (Optional)

LLDAP ships with a built-in [SQLite](https://www.sqlite.org/) database to store its state.
For most homelab uses this should be more than enough,
but if you want [high availability](https://en.wikipedia.org/wiki/High_availability) you need to use an external
database in order to be able to spin up multiple instances of LLDAP.

LLDAP supports both [MySQL](https://www.mysql.com/) and [PostgreSQL](https://www.postgresql.org/).
I've previously written about [PostgreSQL on Kubernetes]({{< ref "/articles/2024/10/k8s-postgres" >}}) where I decided
to go with [CloudNative PG (CNPG)](https://cloudnative-pg.io/) to spin up PostgreSQL clusters inside Kubernetes
clusters.
Other alternatives
are [Zalando Postgres Operator](https://opensource.zalando.com/postgres-operator/docs/quickstart.html),
[Crunchy Data](https://www.crunchydata.com/),
[KubebBocks](https://github.com/apecloud/kubeblocks),
[Percona Everest](https://docs.percona.com/),
[Stolon](https://github.com/sorintlab/stolon),
and [StackGres](https://stackgres.io/).

Assuming you've configured
a _[default StorageClass](https://kubernetes.io/docs/tasks/administer-cluster/change-default-storage-class/)_ and CNPG,
you can create a database cluster for LLDAP with the following resource

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/lldap/cnpg-db.yaml" >}}
{{< /highlight >}}

With the above manifest, CNPG creates a secret called `lldap-postgres-app` with a connection uri on the form
`postgresql://<USER>:<PASSWORD>@<URI>:<PORT>/<DATABASE>` which we can map to the `LLDAP_DATABASE_URL` environment
variable in the LLDAP container

```yaml
env:
  - name: LLDAP_DATABASE_URL
    valueFrom:
      secretKeyRef:
        name: lldap-postgres-app
        key: uri
```

If you already have a database you want to connect with you can instead create a _Secret_ with the correct database uri.

LLDAP provides a [migration guide](https://github.com/lldap/lldap/blob/main/docs/database_migration.md) if you start
with SQLite and decide to change to an external database at a later stage.

## Summary

All resources can be found
in [this GitLab repo](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2025/01/lldap/resources),
and are written for use
with [Argo CD with Kustomize + Helm]({{< ref "/articles/2023/09/argocd-kustomize-with-helm" >}}),
but should be easily adaptable for other approaches using e.g. [Flux CD](https://fluxcd.io/).

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/svc.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/crypto.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/admin-credentials.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/smtp-settings.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/cert.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/cnpg-db.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/deployment.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/lldap/bootstrap.yaml" >}}
{{< /highlight >}}

{{< highlight json >}}
{{< readfile-rel file="resources/lldap/group-schemas/group-application.json" >}}
{{< /highlight >}}

{{< highlight json >}}
{{< readfile-rel file="resources/lldap/user-schemas/user-details.json" >}}
{{< /highlight >}}

{{< highlight json >}}
{{< readfile-rel file="resources/lldap/groups/groups.json" >}}
{{< /highlight >}}

{{< highlight json >}}
{{< readfile-rel file="resources/lldap/users/user.json" >}}
{{< /highlight >}}

{{< highlight json >}}
{{< readfile-rel file="resources/lldap/users/more-users.json" >}}
{{< /highlight >}}

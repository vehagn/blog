---
title: "Authelia"
date: 2024-09-07T22:00:00+02:00
#lastmod: 2024-07-10T12:45:00+02:00

draft: true

categories:
  - posts
  - kubernetes

tags:
  - k8s
  - kubernetes
  - homelab
  - auth
  - authentication
  - ldap
  - lldap
---

https://www.authelia.com/reference/guides/ldap/#defaults

https://www.authelia.com/integration/ldap/introduction/#lldap

```yaml
authentication_backend:
  ldap:
    implementation: lldap
```

doesn't work? use custom?

## Still in beta!

https://www.authelia.com/roadmap/active/openid-connect/

https://github.com/lldap/lldap/blob/main/example_configs/authelia_config.yml

### Crypto

```shell
docker run authelia/authelia:latest authelia crypto rand --length 128 --charset alphanumeric
```

## JWKS

https://www.authelia.com/configuration/identity-providers/openid-connect/provider/#key

```shell
docker run -u "$(id -u):$(id -g)" -v "$(pwd)":/keys authelia/authelia:latest authelia crypto pair rsa generate --directory /keys
```

```shell
openssl genrsa -out secret-authelia-jwk-private.pem 2048
openssl rsa -in secret-authelia-jwk-private.pem -outform PEM -pubout -out authelia-jwk-public.pem
```

Figure out certificate chain!
https://www.authelia.com/reference/guides/generating-secure-values/#generating-an-rsa-self-signed-certificate

https://security.stackexchange.com/questions/278872/why-is-jwt-claim-x5t-thumbprint-useful
https://stackoverflow.com/questions/69179822/jwk-key-creation-with-x5c-and-x5t-parameters
https://mojoauth.com/glossary/jwt-x.509-certificate-chain/
https://redthunder.blog/2017/06/08/jwts-jwks-kids-x5ts-oh-my/
https://stytch.com/blog/understanding-jwks/
x5t and x5c is for possibility of revoking signing key?

```shell
docker run -u "$(id -u):$(id -g)" -v "$(pwd)":/keys authelia/authelia:latest authelia crypto certificate rsa generate --common-name authelia.stonegarden.dev --directory /keys
```

```shell
openssl req -x509 -nodes -newkey rsa:2048 -keyout private.pem -out public.crt -sha256 -days 365 -subj '/CN=authelia.stonegarden.dev'
```

ECDSA:

```shell
docker run -u "$(id -u):$(id -g)" -v "$(pwd)":/keys authelia/authelia:latest authelia crypto pair ecdsa generate --curve P256 --directory /keys
```

### JWK

https://cert-manager.io/v1.1-docs/usage/certificate/
https://cert-manager.io/docs/reference/api-docs/#cert-manager.io/v1.CertificatePrivateKey

## Argo CD

https://www.authelia.com/integration/openid-connect/argocd/

https://www.authelia.com/integration/openid-connect/frequently-asked-questions/#how-do-i-generate-a-client-identifier-or-client-secret

client_id

```shell
docker run authelia/authelia:latest authelia crypto rand --length 72 --charset rfc3986
```

client_secret

```shell
docker run authelia/authelia:latest authelia crypto hash generate pbkdf2 --variant sha512 --random --random.length 72 --random.charset rfc3986
```

https://argo-cd.readthedocs.io/en/stable/operator-manual/user-management/#existing-oidc-provider

https://argo-cd.readthedocs.io/en/stable/operator-manual/user-management/#sensitive-data-and-sso-client-secrets
https://github.com/argoproj/argo-cd/issues/13928#issuecomment-2146020186

No ES256 support? // https://github.com/argoproj/argo-cd/issues/9433

RBAC: https://github.com/argoproj/argo-cd/blob/master/docs/operator-manual/rbac.md


NETBIRD: https://github.com/authelia/authelia/discussions/7185
---
title: "Talos Kubernetes on Proxmox using OpenTofu"
date: 2024-08-07T22:00:00+02:00
lastmod: 2024-09-08T14:45:00+02:00

categories:
  - posts
  - kubernetes

tags:
  - k8s
  - kubernetes
  - homelab
  - talos
  - cilium
  - proxmox
  - opentofu
  - tofu
  - terraform
  - csi
---

[Talos](https://talos.dev) is an [immutable](https://dictionary.cambridge.org/dictionary/english/immutable) operating
system designed to only run [Kubernetes](https://kubernetes.io/).
The advantage of Talos is an out-of-the-box Kubernetes install,
as well as a smaller attack surface,
and easier maintenance.

In this article we'll take a look at how to bootstrap and upgrade a multi-node Talos cluster running in VMs on
a [Proxmox Virtual Environment](https://www.proxmox.com/en/proxmox-virtual-environment/overview) 8.2 cluster.
We'll be using [OpenTofu](https://opentofu.org/)/[Terraform](https://www.terraform.io/) to do this in a declaratively
following [IaC](https://en.wikipedia.org/wiki/Infrastructure_as_code) principles.

This article can be treated as a continuation of my previous scribbles
on [Bootstrapping k3s with Cilium]({{< ref "/articles/2024/02/bootstrapping-k3s-with-cilium" >}})
and running [Kubernetes on Proxmox]({{< ref "/articles/2024/03/proxmox-k8s-with-cilium" >}})
using [Debian 12](https://debian.org).
The previous articles will hopefully form a basis with more nerdy details if my explanations fall short here.

The initial idea was inspired by [this post](https://olav.ninja/talos-cluster-on-proxmox-with-terraform) written
by [Olav](https://github.com/olav-st) which is more straight to the point.
I've added my own twists, tweaks and thoughts on his initial configuration which I hope warrants this article.

## Overview

The Kubernetes-Tofu recipe we'll be bootstrapping today is sweetened
using [Cilium's](https://cilium.io/) [eBPF](https://ebpf.io/) enhanced honey,
subtly seasoned with the [Sealed Secrets](https://sealed-secrets.netlify.app/) sauce,
and baked at 256° GiB with volumes provisioned
by [Proxmox CSI Plugin](https://github.com/sergelogvinov/proxmox-csi-plugin).
As an optional garnish we'll top it with Intel iGPU drivers
for [Quick Sync Video](https://en.wikipedia.org/wiki/Intel_Quick_Sync_Video) support.

Key ingredients for this Tofu dish will be the [bpg/proxmox](https://github.com/bpg/terraform-provider-proxmox)
and [siderolabs/talos](https://github.com/siderolabs/terraform-provider-talos) providers.
To complete the optional bootstrapping we'll also utilise
the [Mastercard/rastapi](https://github.com/Mastercard/terraform-provider-restapi)
and [hashicorp/kubernetes](https://github.com/hashicorp/terraform-provider-kubernetes) providers.

At the end we'll have three servings of Talos-Kubernetes control-plane node and one worker node all clustered together.
Adjust the recipe according to your needs.

We start by exploring the [Talos Linux Image Factory](https://factory.talos.dev/) to generate an image with the system
components we need.
Next we'll create a [virtual machine](https://en.wikipedia.org/wiki/Virtual_machine) in Proxmox and use Talos Machine
Configuration to bootstrap our Kubernetes cluster.

After kickstarting Kubernetes we take a look at how we can use the provided configuration to upgrade our cluster
in-place, before discussing potential improvements to this approach.

### Folder structure

Seeing as this is a fairly long article it might be beneficial to have an overview of the folder structure that will be
used for all the resources

```shell
🗃️
├── 📂 talos                    # Talos configuration
│   ├── 📁 image                # Image schematic
│   ├── 📁 inline-manifests     # Bootstrapping manifests (Cilium)
│   └── 📁 machine-config       # Machine config templates
└── 📂 bootstrap                # Optional bootstrap
   ├── 📂 sealed-secrets        # Secrets management
   │   └── 📁 certificate       # Encryption keys
   ├── 📂 proxmox-csi-plugin    # CSI Driver
   └── 📂 volumes               # Volume provisioning
       ├── 📁 persistent-volume # Kubernetes PVs
       └── 📁 proxmox-volume    # Proxmox disk images
```

For a full overview of all the files check out the [Summary](#summary) or the repository for this
article [here](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/08/talos-proxmox-tofu/resources).

## Hardware

The hardware used in this article are
two [Intel N100](https://ark.intel.com/content/www/us/en/ark/products/231803/intel-processor-n100-6m-cache-up-to-3-40-ghz.html)
based mini-PCs affectionately named [euclid](https://en.wikipedia.org/wiki/Euclid)
and [cantor](https://en.wikipedia.org/wiki/Georg_Cantor),
and a
third [Intel i3-N305](https://www.intel.com/content/www/us/en/products/sku/231805/intel-core-i3n305-processor-6m-cache-up-to-3-80-ghz/specifications.html)
based machine called [abel](https://en.wikipedia.org/wiki/Niels_Henrik_Abel),
all with 32 GB RAM running clustered Proxmox VE 8.2.

{{< mermaid-init >}}

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/cluster.mmd" >}}
{{< /mermaid-chart >}}

Though the cluster would benefit from [Ceph](https://ceph.io/en/),
we won't touch upon that here.

## Talos Module

Once Talos is up and running it must be configured to run and cluster properly.
It's possible to use `talosctl` to do this manually,
but we'll leverage the Talos-provider to do this for us based on their
own [example](https://github.com/siderolabs/terraform-provider-talos/blob/main/docs/resources/machine_bootstrap.md).

To keep all the configuration in one place and avoid repeating ourselves,
we can create a _cluster_ variable to hold the values shared by the whole cluster

```terraform
variable "cluster" {
  description = "Cluster configuration"
  type = object({
    name            = string
    endpoint        = string
    gateway         = string
    talos_version   = string
    proxmox_cluster = string
  })
}
```

The cluster name is set via the _name_-variable,
while the _endpoint_-variable signifies the main Kubernetes API endpoint.
In a highly available setup this should be configured to use all control plane nodes using e.g. a load balancer.
For more information on configuring a HA endpoint read the Talos documentation
on "[Decide the Kubernetes Endpoint](https://www.talos.dev/latest/introduction/prodnotes/#decide-the-kubernetes-endpoint)".

Sidero
labs [recommends](https://github.com/siderolabs/terraform-provider-talos/blob/80c5534fbd876506201f21b50c950ce053402a55/docs/data-sources/machine_configuration.md#talos_machine_configuration-data-source)
setting the optional _talos_version_-variable to avoid unexpected behaviour when upgrading,
and we will follow that advice here.

We'll use the _gateway_-variable to set the default network gateway for all nodes.
Jumping ahead a bit,
the _proxmox_cluster_-variable will be used to configure the `topology.kubernetes.io/region` label to be used by our
chosen [CSI](https://kubernetes-csi.github.io/docs/)-controller.

The values we will use in this article is given below.
Note that we've skipped HA configuration and set the Kubernetes API endpoint to simply be the IP of the first
control-plane node.

```terraform
cluster = {
  name            = "talos"
  endpoint        = "192.168.1.100"
  gateway         = "192.168.1.1"
  talos_version   = "v1.7"
  proxmox_cluster = "homelab"
}
```

To allow for easy customisation of the nodes we'll use a map that can be looped through to create and configure the
required VMs

```terraform
variable "nodes" {
  description = "Configuration for cluster nodes"
  type = map(object({
    host_node     = string
    machine_type  = string
    datastore_id = optional(string, "local-zfs")
    ip            = string
    mac_address   = string
    vm_id         = number
    cpu           = number
    ram_dedicated = number
    update = optional(bool, false)
    igpu = optional(bool, false)
  }))
}
```

In this map we're using the `hostname` as a key and the node configuration as values.
The _host_node_-variable indicates which Proxmox VE hypervisor node the VM should run on,
whereas the _machine_type_-variable decides the node-type
— either `controlplane` or `worker`.
The remaining variables are for VM-configuration,
the non-obvious ones are perhaps _datastore-id_-variable
— which is used to control where the VM disk should be stored,
as well as the _update_-flag
— which selects which image should be used,
and the _igpu_-flag that can be used to enable passthrough of the host iGPU.

The configuration for a four node cluster with three control-plane nodes and one worker looks like

```terraform
nodes = {
  "ctrl-00" = {
    machine_type  = "controlplane"
    ip            = "192.168.1.100"
    mac_address   = "BC:24:11:2E:C8:00"
    host_node     = "abel"
    vm_id         = 800
    cpu           = 8
    ram_dedicated = 4096
  }
  "ctrl-01" = {
    host_node     = "euclid"
    machine_type  = "controlplane"
    ip            = "192.168.1.101"
    mac_address   = "BC:24:11:2E:C8:01"
    vm_id         = 801
    cpu           = 4
    ram_dedicated = 4096
    igpu          = true
  }
  "ctrl-02" = {
    host_node     = "cantor"
    machine_type  = "controlplane"
    ip            = "192.168.1.102"
    mac_address   = "BC:24:11:2E:C8:02"
    vm_id         = 802
    cpu           = 4
    ram_dedicated = 4096
  }
  "work-00" = {
    host_node     = "abel"
    machine_type  = "worker"
    ip            = "192.168.1.110"
    mac_address   = "BC:24:11:2E:08:00"
    vm_id         = 810
    cpu           = 8
    ram_dedicated = 4096
    igpu          = true
  }
}
```

Note that we've arbitrarily enabled iPGU passthrough on the `ctrl-01` and `work-00` nodes hosted on `euclid` and `abel`
respectively.

### Image Factory

By definition, an immutable OS disallows changing components after it's been installed.
To not include everything you might need in one image, [Sidero Labs](https://www.siderolabs.com/)
— the people behind Talos,
have created [Talos Linux Image Factory](https://factory.talos.dev/) to let you customise which packages are included.

Talos Linux Image Factory enables us to create a Talos image with the configuration we want either through point and
click,
or by POSTing a YAML/JSON schematic to [https://factory.talos.dev/schematics](https://factory.talos.dev/schematics) to
get back a unique schematic ID.

In our example we want to install _QEMU guest agent_ to report VM status to the Proxmox hypervisor,
including [Intel microcode](https://en.wikipedia.org/wiki/Intel_microcode) and iGPU drivers to be able to take full
advantage of [Quick Sync Video on Kubernetes]({{< ref "/articles/2024/05/intel-quick-sync-k8s" >}}).

The schematic for this configuration is

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/image/schematic.yaml" >}}
{{< /highlight >}}

which yields the schematic ID

```json
{
  "id": "dcac6b92c17d1d8947a0cee5e0e6b6904089aa878c70d66196bb1138dbd05d1a"
}
```

when POSTed to [https://factory.talos.dev/schematics](https://factory.talos.dev/schematics).

Combining our wanted `schematic_id`, `version`, `platform`, and `architecture` we can use

```
https://factory.talos.dev/image/<schematid_id>/<version>/<platform>-<architecture>.raw.gz
```

as a template to craft a URL to download the requested image.

A simplified Tofu recipe to automate the process of downloading the Talos image to a Proxmox host looks like

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/simplified/image.tf" >}}
{{< /highlight >}}

Iterating on the above configuration we can craft the following recipe to allow for changing the image incrementally
across our cluster

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/image.tf" >}}
{{< /highlight >}}

with the following variable definition

```terraform
variable "image" {
  description = "Talos image configuration"
  type = object({
    factory_url = optional(string, "https://factory.talos.dev")
    schematic = string
    version   = string
    update_schematic = optional(string)
    update_version = optional(string)
    arch = optional(string, "amd64")
    platform = optional(string, "nocloud")
    proxmox_datastore = optional(string, "local")
  })
}
```

Here we've keyed the
_[proxmox_virtual_environment_download_file](https://github.com/bpg/terraform-provider-proxmox/blob/main/docs/resources/virtual_environment_download_file.md)_
resource to be `<host>_<schematic_id>_<version>` which will trigger a recreation of the connected VM only when the node
_update_ variable is changed.

At the time of writing,
the [v0.6.0-alpha.1 pre-release](https://github.com/siderolabs/terraform-provider-talos/releases/tag/v0.6.0-alpha.1)
of the Talos provider has built-in support for generating schematic IDs,
so this step can probably be simplified in the future.

For more details on how to customize Talos images check
the [Talos Image Factory GitHub repository](https://github.com/siderolabs/image-factory) for documentation.

If you're partial to NVIDIA GPU acceleration,
Talos has [instructions](https://www.talos.dev/latest/talos-guides/configuration/nvidia-gpu/) on how to enable this in
their [documentation](https://www.talos.dev/latest/),
so there's no need to cover it here.

### Client Configuration

The first step in setting up Talos machines is to create _machine secrets_ and _client configuration_ shared by all
nodes.

The [
_talos_machine_secrets_](https://github.com/siderolabs/terraform-provider-talos/blob/main/docs/resources/machine_secrets.md)
resource generates certificates to be shared between the nodes for security,
the only optional arguments is _talos_version_.

```terraform
resource "talos_machine_secrets" "this" {
  talos_version = var.cluster.talos_version
}
```

Next we generate [
_talos_client_configuration_](https://github.com/siderolabs/terraform-provider-talos/blob/main/docs/data-sources/client_configuration.md)
where we set the _cluster_name_ and add machine secrets from the previous resource.

```terraform
data "talos_client_configuration" "this" {
  cluster_name         = var.cluster.name
  client_configuration = talos_machine_secrets.this.client_configuration
  nodes                = [for k, v in var.nodes : v.ip]
  endpoints            = [for k, v in var.nodes : v.ip if v.machine_type == "controlplane"]
}
```

As for optional configuration we've added all the _nodes_ from our input variable and only the `controlplane` nodes as
_endpoints_.
Read the `talosctl` documentation for the [difference between
_nodes_ and
_endpoints_](https://www.talos.dev/v1.7/introduction/getting-started/#understand-talosctl-endpoints-and-nodes).

### Machine Configuration

With the client configuration in place we next need to prepare the machine configuration for the Talos nodes.

We've prepared separate machine-config templates for the control-plane and worker nodes.
The worker machine config simply contains the node hostname,
along with
the [well-known
`topology.kubernetes.io` labels](https://kubernetes.io/docs/reference/labels-annotations-taints/#topologykubernetesioregion)
which will be used by the Proxmox CSI plugin later

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/machine-config/worker.yaml.tftpl" >}}
{{< /highlight >}}

If you're going for a [high availability](https://en.wikipedia.org/wiki/High_availability) setup where the nodes can
move around, you should look into either the [Talos](https://github.com/siderolabs/talos-cloud-controller-manager)
or [Proxmox](https://github.com/sergelogvinov/proxmox-cloud-controller-manager)
[Cloud Controller Managers](https://kubernetes.io/docs/concepts/architecture/cloud-controller/) to set the topology
labels dynamically based on the node location.

The control-plane machine configuration starts off similar to the worker configuration,
but we're adding cluster configuration to allow scheduling on the control-plane nodes,
as well as disabling the default kube-proxy which we'll replace by the Cilium CNI using an inline bootstrap manifest.

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/machine-config/control-plane.yaml.tftpl" >}}
{{< /highlight >}}

You can also add extra manifests to be applied during the bootstrap,
e.g. the [Gateway API](https://gateway-api.sigs.k8s.io/) CRDs if you plan to use the Gateway API.

The different _talos_machine_configuration_ for each node is prepared using the following recipe

```terraform
data "talos_machine_configuration" "this" {
  for_each         = var.nodes
  cluster_name     = var.cluster.name
  cluster_endpoint = var.cluster.endpoint
  talos_version    = var.cluster.talos_version
  machine_type     = each.value.machine_type
  machine_secrets  = talos_machine_secrets.this.machine_secrets
  config_patches   = each.value.machine_type == "controlplane" ? [
    templatefile("${path.module}/machine-config/control-plane.yaml.tftpl", {
      hostname       = each.key
      node_name      = each.value.host_node
      cluster_name   = var.cluster.proxmox_cluster
      cilium_values  = var.cilium.values
      cilium_install = var.cilium.install
    })
  ] : [
    templatefile("${path.module}/machine-config/worker.yaml.tftpl", {
      hostname     = each.key
      node_name    = each.value.host_node
      cluster_name = var.cluster.proxmox_cluster
    })
  ]
}
```

### Cilium Bootstrap

The `cilium-bootstrap` _inlineManifest_ is modified from the Talos documentation example
on [deploying Cilium CNI using a job](https://www.talos.dev/v1.7/kubernetes-guides/network/deploying-cilium/#method-5-using-a-job)
to use values passed from the `cilium-values` _inlineManifest_ ConfigMap.
This allows us to easily bootstrap Cilium using a `values.yaml` file we can reuse later.

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/inline-manifests/cilium-install.yaml" >}}
{{< /highlight >}}

A basic `values.yaml` configuration that works with Talos would be the following as suggested in the Talos documentation

```yaml
kubeProxyReplacement: true

# Talos specific
# https://www.talos.dev/latest/kubernetes-guides/configuration/kubeprism/
k8sServiceHost: localhost
k8sServicePort: 7445

securityContext:
  capabilities:
    ciliumAgent: [ CHOWN,KILL,NET_ADMIN,NET_RAW,IPC_LOCK,SYS_ADMIN,SYS_RESOURCE,DAC_OVERRIDE,FOWNER,SETGID,SETUID ]
    cleanCiliumState: [ NET_ADMIN,SYS_ADMIN,SYS_RESOURCE ]

cgroup:
  autoMount:
    enabled: false
  hostRoot: /sys/fs/cgroup

# https://docs.cilium.io/en/stable/network/concepts/ipam/
ipam:
  mode: kubernetes
```

We can later expand on this configuration by optionally
enabling [L2 Announcements](https://docs.cilium.io/en/latest/network/l2-announcements/),
[IngressController](https://docs.cilium.io/en/latest/network/servicemesh/ingress/),
[Gateway API](https://docs.cilium.io/en/latest/network/servicemesh/gateway-api/gateway-api/),
and/or [Hubble](https://docs.cilium.io/en/latest/gettingstarted/hubble_intro) as we've done in the [summary](#summary)
section.

If you're interested in learning more about the capabilities of Cilium,
I've written about ARP, L2 announcements, and LB IPAM in a previous article
about [Migrating from MetalLB to Cilium]({{< ref "/articles/2023/12/migrating-from-metallb-to-cilium" >}}).
I've also authored an article about using the [Cilium Gateway API]({{< ref "/articles/2023/12/cilium-gateway-api" >}})
implementation as a replacement for the Ingress API.

### Virtual Machines

Before we can apply the machine configuration we need to create the VMs to perform this action on.

If you're new to Proxmox and virtual machines I've covered some of the configuration choices in
my previous [Kubernetes on Proxmox]({{< ref "/articles/2024/03/proxmox-k8s-with-cilium#virtual-machines" >}}) article
where I also try to explain PCI-passthrough.

The [
_proxmox_virtual_environment_vm_](https://github.com/bpg/terraform-provider-proxmox/blob/main/docs/resources/virtual_environment_vm.md)
recipe we'll be using is pretty straight-forward,
expect the conditional boot-disk `file_id` based on if we want an
updated image or not,
and the dynamic _hostpci_ block for conditional PCI-passthrough.

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/virtual-machines.tf" >}}
{{< /highlight >}}

Note that the iGPU has to be mapped in Proxmox first.
This can be done under **Datacenter** > **Resource Mappings** > **Add**.
Go through all the physical nodes and manually map the applicable iGPU for each of them as shown below.

{{< dynamic-image title="Proxmox Datacenter Resource Mapping"
alt="Resource mapping" dark="images/resource-mapping-dark.png" light="images/resource-mapping-light.png" >}}

### Talos Bootstrap

With the VMs booted up.
the Talos machine configuration can be applied using the aptly named [
_talos_machine_configuration_apply_](https://github.com/siderolabs/terraform-provider-talos/blob/main/docs/resources/machine_configuration_apply.md)
resource

```terraform
resource "talos_machine_configuration_apply" "this" {
  depends_on = [proxmox_virtual_environment_vm.this]
  for_each                    = var.nodes
  node                        = each.value.ip
  client_configuration        = talos_machine_secrets.this.client_configuration
  machine_configuration_input = data.talos_machine_configuration.this[each.key].machine_configuration
  lifecycle {
    replace_triggered_by = [proxmox_virtual_environment_vm.this[each.key]]
  }
}
```

which should depend on the VMs being up and running.
The configuration should also be re-applied on changed VMs,
e.g. when the boot image is updated to facilitate an upgrade.

Finally, we can bootstrap the cluster using a [
_talos_machine_bootstrap_](https://github.com/siderolabs/terraform-provider-talos/blob/main/docs/resources/machine_bootstrap.md)
resource

```terraform
resource "talos_machine_bootstrap" "this" {
  node                 = [for k, v in var.nodes : v.ip if v.machine_type == "controlplane"][0]
  endpoint             = var.cluster.endpoint
  client_configuration = talos_machine_secrets.this.client_configuration
}
```

Note that we've arbitrarily picked the ip of the first control-plane node here,
but also supplied the (optional) potentially load-balanced cluster endpoint.

To make sure that the cluster is up and running we probe the cluster health

```terraform
data "talos_cluster_health" "this" {
  depends_on = [
    talos_machine_configuration_apply.this,
    talos_machine_bootstrap.this
  ]
  client_configuration = data.talos_client_configuration.this.client_configuration
  control_plane_nodes  = [for k, v in var.cluster_config.nodes : v.ip if v.machine_type == "controlplane"]
  worker_nodes         = [for k, v in var.cluster_config.nodes : v.ip if v.machine_type == "worker"]
  endpoints            = data.talos_client_configuration.this.endpoints
  timeouts = {
    read = "10m"
  }
}
```

When the cluster is up and healthy we can fetch the kube-config file from the [
_talos_cluster_kubeconfig_](https://github.com/siderolabs/terraform-provider-talos/blob/main/docs/data-sources/cluster_kubeconfig.md)
data source

```terraform
data "talos_cluster_kubeconfig" "this" {
  depends_on = [
    talos_machine_bootstrap.this,
    data.talos_cluster_health.this
  ]
  node                 = [for k, v in var.nodes : v.ip if v.machine_type == "controlplane"][0]
  endpoint             = var.cluster.endpoint
  client_configuration = talos_machine_secrets.this.client_configuration
  timeouts = {
    read = "1m"
  }
}
```

Here we've again arbitrarily picked the required _node_-parameter to be the first control plane node IP,
but also set the optional endpoint.

### Module Output

We've configured the Talos module to output the client config file to be used with the `talosctl` tool,
and the Kubernetes config file to be used with `kubectl`

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/output.tf" >}}
{{< /highlight >}}

For debug purposes we've also included the machine configuration,
which arguably should also be marked as sensitive.

## Sealed-secrets (Optional)

Once the cluster is up and running
— and with the [kubeconfig](https://kubernetes.io/docs/reference/config-api/kubeconfig.v1/)-file in hand,
we can start bootstrapping the cluster.

[Sealed secrets](https://sealed-secrets.netlify.app/) promises that
> [they] are safe to store in your local code repository, along with the rest of your configuration.

This makes Sealed Secrets an alternative to
e.g. [Secrets Store CSI Driver](https://secrets-store-csi-driver.sigs.k8s.io/),
and as advertised allows us to keep the secrets in the same place as the configuration.

The only gotcha with this approach is that you need to keep the decryption key.
If you tear down and rebuild your cluster you thus need to inject the same encryption/decryption keys to be able to
reuse the same SealedSecret objects.

To bootstrap an initial secret for this purpose we can use the following Tofu recipe

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/sealed-secrets/config.tf" >}}
{{< /highlight >}}

with the _cert_-variable defined as

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/sealed-secrets/variables.tf" >}}
{{< /highlight >}}

This will create the `sealed-secrets` namespace and populate it with a single secret which Sealed Secrets should pick
up.

A valid Sealed Secrets certificate-key pair can be generated using [OpenSSL](https://www.openssl.org/) by running

```shell
openssl req -x509 -days 365 -nodes -newkey rsa:4096 -keyout sealed-secrets.key -out sealed-secrets.cert -subj "/CN=sealed-secret/O=sealed-secret"
```

to be used as input.

For increased security you should rotate the certificate and re-encrypt the secrets one in a while.

## Proxmox CSI Plugin (Optional)

Since we're running on Proxmox,
a natural choice for
a [CSI (Container Storage Interface)](https://github.com/container-storage-interface/spec/blob/master/spec.md) driver
would be utilising Proxmox.

A great alternative for this is the [Proxmox CSI Plugin](https://github.com/sergelogvinov/proxmox-csi-plugin)
by [Serge Logvinov](https://sinextra.dev/)
to provision persistent storage for our Kubernetes cluster.

I've covered how to configure Proxmox CSI Plugin
in [Kubernetes Proxmox CSI]({{< ref "/articles/2024/06/k8s-proxmox-csi" >}}) so I'll allow myself to be brief here.

We first need to create a `CSI` role in Proxmox

```terraform
resource "proxmox_virtual_environment_role" "csi" {
  role_id = "CSI"
  privileges = [
    "VM.Audit",
    "VM.Config.Disk",
    "Datastore.Allocate",
    "Datastore.AllocateSpace",
    "Datastore.Audit"
  ]
}
```

and bestow that role to a `kubernetes-csi` user

```terraform
resource "proxmox_virtual_environment_user" "kubernetes-csi" {
  user_id = "kubernetes-csi@pve"
  comment = "User for Proxmox CSI Plugin"
  acl {
    path      = "/"
    propagate = true
    role_id   = proxmox_virtual_environment_role.csi.role_id
  }
}
```

We then create a token for that user

```terraform
resource "proxmox_virtual_environment_user_token" "kubernetes-csi-token" {
  comment               = "Token for Proxmox CSI Plugin"
  token_name            = "csi"
  user_id               = proxmox_virtual_environment_user.kubernetes-csi.user_id
  privileges_separation = false
}
```

which we input in a privileged namespace (which is required according to the Proxmox CSI
Plugin [documentation](https://github.com/sergelogvinov/proxmox-csi-plugin?tab=readme-ov-file#install-csi-driver))

```terraform
resource "kubernetes_namespace" "csi-proxmox" {
  metadata {
    name = "csi-proxmox"
    labels = {
      "pod-security.kubernetes.io/enforce" = "privileged"
      "pod-security.kubernetes.io/audit"   = "baseline"
      "pod-security.kubernetes.io/warn"    = "baseline"
    }
  }
}
```

as a secret containing the Proxmox CSI Plugin configuration

```terraform
resource "kubernetes_secret" "proxmox-csi-plugin" {
  metadata {
    name      = "proxmox-csi-plugin"
    namespace = kubernetes_namespace.csi-proxmox.id
  }

  data = {
    "config.yaml" = <<EOF
clusters:
- url: "${var.proxmox.endpoint}/api2/json"
  insecure: ${var.proxmox.insecure}
  token_id: "${proxmox_virtual_environment_user_token.kubernetes-csi-token.id}"
  token_secret: "${element(split("=", proxmox_virtual_environment_user_token.kubernetes-csi-token.value), length(split("=", proxmox_virtual_environment_user_token.kubernetes-csi-token.value)) - 1)}"
  region: ${var.proxmox.cluster_name}
EOF
  }
}
```

## Provision Volumes (Optional)

With Proxmox CSI Plugin we can provision volumes using `PersistentVolumeClaims` referencing a `StorageClass` which uses
the `csi.proxmox.sinextra.dev` _provisioner_, e.g.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-test
  namespace: proxmox-csi-test
spec:
  storageClassName: proxmox-csi
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
```

for the `StorageClass`

```yaml
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: proxmox-csi
provisioner: csi.proxmox.sinextra.dev
parameters:
  cache: writethrough
  csi.storage.k8s.io/fstype: ext4
  storage: local-zfs
```

This will automatically create a VM disk in Proxmox and a corresponding `PersistentVolume` referencing the mounted VM
disk.
The potential issue with this is that the created VM disk will be named with a random UUID
— e.g.  `vm-9999-pvc-96bff316-1d50-45d5-b8fa-449bb3825211`,
which makes referencing it again after a cluster rebuild cumbersome.

A solution to this issue is to manually create a VM disk in Proxmox and a `PersistentVolume` referencing it,
which we will do here.

The Proxmox provider we've chosen doesn't (yet) support creating VM disks directly,
so we have to resort to the [Proxmox VE REST API](https://pve.proxmox.com/pve-docs/api-viewer/index.html) to use it.
For this purpose we can use the [Mastercard restapi provider](https://github.com/Mastercard/terraform-provider-restapi)
to conjure the following OpenTofu recipe

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/proxmox-volume/config.tf" >}}
{{< /highlight >}}

with the inpu variables defined as

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/proxmox-volume/variables.tf" >}}
{{< /highlight >}}

On the Kubernetes side we create a matching `PersistentVolume` using
the [Hashicorp kubernetes provider](https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs)

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/persistent-volume/config.tf" >}}
{{< /highlight >}}

providing the module with the following variables

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/persistent-volume/variables.tf" >}}
{{< /highlight >}}

Combining the proxmox- and persistent-volume moules we can use the output from the former to set the _volume_handle_ in
the latter to tie them together

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/main.tf" >}}
{{< /highlight >}}

The input to this aggregated volume-module takes Proxmox API details and a map of volumes as input

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/variables.tf" >}}
{{< /highlight >}}

To provision a 4 GB `PersistentVolume` backed by a Proxmox VM disk attached to e.g. the `abel` node you can supply

```terraform
volumes = {
  pv-test = {
    node = "abel"
    size = "4G"
  }
}
```

as input to the volume-module.
To claim this module you then reference the PV in a `PersistentVolumeClaim`'s _volumeName_ field, e.g.

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: lidarr-config
  namespace: pvc-test
spec:
  storageClassName: proxmox-csi
  volumeName: pv-test
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 4G
```

Since we're not running with distributed storage
— e.g. [Ceph](https://ceph.io/), [GlusterFS](https://www.gluster.org/), or [Longhorn](https://longhorn.io/),
we have to manually specify that the pod using the PVC has to run on the same node as the
Proxmox backed volume.
We can easily do this by referencing the corresponding `topology.kubernetes.io/zone` label
in a pod _nodeSelector_, i.e.

```yaml
nodeSelector:
  topology.kubernetes.io/zone: abel
```

## Main Course

Having prepared the main Talos dish and possibly a few of the side-courses,
we can start combining all the OpenTofu recipes into a fully-fledged ~meal~ cluster.

The providers backing the whole operation are

```terraform
terraform {
  required_providers {
    talos = {
      source  = "siderolabs/talos"
      version = "0.5.0"
    }
    proxmox = {
      source  = "bpg/proxmox"
      version = "0.61.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.31.0"
    }
    restapi = {
      source  = "Mastercard/restapi"
      version = "1.19.1"
    }
  }
}
```

with details on how to connect to the Proxmox API supplied as a variable

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/variables.tf" >}}
{{< /highlight >}}

As an example we can directly connect to one of the Proxmox nodes using the following variables

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/proxmox.auto.tfvars" >}}
{{< /highlight >}}

An API token can be generated in the **Datacenter** > **Permissions** > **API Tokens** menu by clicking **Add** as shown
below

{{< dynamic-image title="Generating a Proxmox API Token"
alt="Proxmox API Token" dark="images/proxmox-api-token-dark.png" light="images/proxmox-api-token-light.png" >}}

The Talos provider requires no configuration,
while the Proxmox provider consumes the previously defined variable

```terraform
provider "proxmox" {
  endpoint = var.proxmox.endpoint
  insecure = var.proxmox.insecure

  api_token = var.proxmox.api_token
  ssh {
    agent    = true
    username = var.proxmox.username
  }
}
```

Next, the Kubernetes provider is configured using output from the Talos module

```terraform
provider "kubernetes" {
  host = module.talos.kube_config.kubernetes_client_configuration.host
  client_certificate = base64decode(module.talos.kube_config.kubernetes_client_configuration.client_certificate)
  client_key = base64decode(module.talos.kube_config.kubernetes_client_configuration.client_key)
  cluster_ca_certificate = base64decode(module.talos.kube_config.kubernetes_client_configuration.ca_certificate)
}
```

Lastly the Rest API provider consumes the same variable as the Proxmox provider

```terraform
provider "restapi" {
  uri                  = var.proxmox.endpoint
  insecure             = var.proxmox.insecure
  write_returns_object = true

  headers = {
    "Content-Type"  = "application/json"
    "Authorization" = "PVEAPIToken=${var.proxmox.api_token}"
  }
}
```

Following the provider configuration we can start to populate the talos module configuration

```terraform
module "talos" {
  source = "./talos"

  providers = {
    proxmox = proxmox
  }

  image = {
    version = "v1.7.5"
    schematic = file("${path.module}/talos/image/schematic.yaml")
  }

  cilium = {
    install = file("${path.module}/talos/inline-manifests/cilium-install.yaml")
    values = file("${path.module}/../kubernetes/cilium/values.yaml")
  }

  cluster = {
    name            = "talos"
    endpoint        = "192.168.1.100"
    gateway         = "192.168.1.1"
    talos_version   = "v1.7"
    proxmox_cluster = "homelab"
  }

  nodes = {
    "ctrl-00" = {
      host_node     = "abel"
      machine_type  = "controlplane"
      ip            = "192.168.1.100"
      mac_address   = "BC:24:11:2E:C8:00"
      vm_id         = 800
      cpu           = 8
      ram_dedicated = 4096
    }
    "ctrl-01" = {
      host_node     = "euclid"
      machine_type  = "controlplane"
      ip            = "192.168.1.101"
      mac_address   = "BC:24:11:2E:C8:01"
      vm_id         = 801
      cpu           = 4
      ram_dedicated = 4096
      igpu          = true
    }
    "ctrl-02" = {
      host_node     = "cantor"
      machine_type  = "controlplane"
      ip            = "192.168.1.102"
      mac_address   = "BC:24:11:2E:C8:02"
      vm_id         = 802
      cpu           = 4
      ram_dedicated = 4096
    }
    "work-00" = {
      host_node     = "abel"
      machine_type  = "worker"
      ip            = "192.168.1.110"
      mac_address   = "BC:24:11:2E:08:00"
      vm_id         = 810
      cpu           = 8
      ram_dedicated = 4096
      igpu          = true
    }
  }
}
```

Here we've supplied the Talos image schematic in an external file previously mentioned in
the [Image Factory](#image-factory) section.
The Cilium install script is the same as in the [Cilium Bootstrap](#cilium-bootstrap) section,
and the corresponding values are picked up from an external file shown in the [Summary](#summary) section.
The remaining _cluster_ and _nodes_ variables are the same as from the [Talos Module](#talos-module) section for
a four-node cluster.

To output the created talos- and kube-config files along with the machine configuration we can use the following recipe

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/output.tf" >}}
{{< /highlight >}}

This stores the variables under `./output`, and makes it possible to show them by running

```shell
tofu output -raw kube_config
tofu output -raw talos_config
```

If you've opted to use the sealed_secrets module it can be configured by sending along the kubernetes provider and
supplying it with the certificate you created in the [Sealed Secrets](#sealed-secrets-optional) section.

```terraform
module "sealed_secrets" {
  depends_on = [module.talos]
  source = "./bootstrap/sealed-secrets"

  providers = {
    kubernetes = kubernetes
  }

  cert = {
    cert = file("${path.module}/bootstrap/sealed-secrets/certificate/sealed-secrets.cert")
    key = file("${path.module}/bootstrap/sealed-secrets/certificate/sealed-secrets.key")
  }
}
```

The proxmox_csi_plugin module requires the proxmox and kubernetes providers along with the proxmox variable used in the
main module

```terraform
module "proxmox_csi_plugin" {
  depends_on = [module.talos]
  source = "./bootstrap/proxmox-csi-plugin"

  providers = {
    proxmox    = proxmox
    kubernetes = kubernetes
  }

  proxmox = var.proxmox
}
```

To provision storage we use the volumes module which we supply with configured restapi and kubernetes providers.
We can also reuse the same proxmox variable to use the Proxmox API.
The volumes are supplied as a map with the backing node and size as the only required values

```terraform
module "volumes" {
  depends_on = [module.proxmox_csi_plugin]
  source = "./bootstrap/volumes"

  providers = {
    restapi    = restapi
    kubernetes = kubernetes
  }

  proxmox_api = var.proxmox

  volumes = {
    pv-test = {
      node = "abel"
      size = "4G"
    }
  }
}
```

If you want to reuse previously created volumes
— e.g. during a cluster rebuild,
these can be imported into the Tofu state by running

```shell
tofu import 'module.volumes.module.proxmox-volume["<VOLUME_NAME>"].restapi_object.proxmox-volume' /api2/json/nodes/<NODE>/storage/<DATASTORE_ID>/content/<DATASTORE_ID>:vm-9999-<VOLUME_NAME>
```

for the Proxmox VM disk and

```shell
tofu import 'module.volumes.module.persistent-volume["<VOLUME_NAME>"].kubernetes_persistent_volume.pv' <VOLUME_NAME>
```

for the Kubernetes persistent volume

## Kubernetes Bootstrap

Assuming the cluster is up and running and the kubeconfig file is in the expected place you should now be able to
run `kubectl get nodes` and be greeted with

```shell
NAME      STATUS   ROLES           AGE   VERSION
ctrl-00   Ready    control-plane   30h   v1.30.0
ctrl-01   Ready    control-plane   30h   v1.30.0
ctrl-02   Ready    control-plane   30h   v1.30.0
work-00   Ready    control-plane   30h   v1.30.0
```

You're now ready to start populating your freshly baked OpenTofu-flavoured Talos Kubernetes cluster.

Popular choices to do this in a declarative manner is [Flux CD](https://fluxcd.io/)
and [Argo CD](https://argo-cd.readthedocs.io/en/stable/).

I picked the latter and wrote an article on making use
of [Argo CD with Kustomize + Helm]({{< ref "/articles/2023/09/argocd-kustomize-with-helm" >}}),
which is what I'm currently using.

For inspiration, you can check out the configuration for my
homelab [here](https://github.com/vehagn/homelab/tree/e24a059de66edcd1fa32dd82dca9f0b4a3aeee7f/k8s).

## Upgrading the Cluster

Talos has a built-in capabilities of [upgrading a cluster](https://www.talos.dev/latest/talos-guides/upgrading-talos/)
using the `talosctl` tool.
There's unfortunately no support yet for this in the Talos provider.

To combat this shortcoming we've set up the Talos module such that we can successively change the image used by each
node.
Assume we start with the following abbreviated module configuration

{{< highlight terraform "linenos=table" >}}
{{< readfile-rel file="resources/tofu/upgrade/00-init.tf" >}}
{{< /highlight >}}

Using the above configuration and running `kubectl get nodes -o wide` should give us something similar to

```shell
NAME      STATUS   ROLES           VERSION   OS-IMAGE         KERNEL-VERSION   CONTAINER-RUNTIME
ctrl-00   Ready    control-plane   v1.30.0   Talos (v1.7.4)   6.6.32-talos     containerd://1.7.16
ctrl-01   Ready    control-plane   v1.30.0   Talos (v1.7.4)   6.6.32-talos     containerd://1.7.16
ctrl-02   Ready    control-plane   v1.30.0   Talos (v1.7.4)   6.6.32-talos     containerd://1.7.16
```

which shows all nodes running on Talos v1.7.4.

By adding `updated_version = "v1.7.5"` (line 3) and telling `ctrl-02` to use the updated image (line 19) we can update
that node

{{< alert "triangle-exclamation" >}}
Since we're removing and creating a new VM for the update process we should only do one at a time to make sure quorum is
upheld in the cluster.
{{< /alert >}}

{{< highlight terraform "linenos=table, hl_lines=3 19" >}}
{{< readfile-rel file="resources/tofu/upgrade/01-first-node.tf" >}}
{{< /highlight >}}

Ideally cordoning and draining node `ctrl-02` before running `tofu apply` we should be eventually be met with

```shell
NAME      STATUS   ROLES           VERSION   OS-IMAGE         KERNEL-VERSION   CONTAINER-RUNTIME
ctrl-00   Ready    control-plane   v1.30.0   Talos (v1.7.4)   6.6.32-talos     containerd://1.7.16
ctrl-01   Ready    control-plane   v1.30.0   Talos (v1.7.4)   6.6.32-talos     containerd://1.7.16
ctrl-02   Ready    control-plane   v1.30.0   Talos (v1.7.5)   6.6.33-talos     containerd://1.7.18
```

indicating that node `ctrl-02` is now running Talos v1.7.5.

Performing the same ritual for node `ctrl-01`

{{< highlight terraform "linenos=table, hl_lines=15" >}}
{{< readfile-rel file="resources/tofu/upgrade/02-second-node.tf" >}}
{{< /highlight >}}

we should in time be greeted with the following cluster status

```shell
NAME      STATUS   ROLES           VERSION   OS-IMAGE         KERNEL-VERSION   CONTAINER-RUNTIME
ctrl-00   Ready    control-plane   v1.30.0   Talos (v1.7.4)   6.6.32-talos     containerd://1.7.16
ctrl-01   Ready    control-plane   v1.30.0   Talos (v1.7.5)   6.6.33-talos     containerd://1.7.18
ctrl-02   Ready    control-plane   v1.30.0   Talos (v1.7.5)   6.6.33-talos     containerd://1.7.18
```

indicating that only `ctrl-00` is left on version 1.7.4.

To complete the upgrade we can update the main version to be `v1.7.5` (line 2),
and remove the _update_-flag for nodes `ctrl-01` and `ctrl-02`.

{{< highlight terraform "linenos=table, hl_lines=2" >}}
{{< readfile-rel file="resources/tofu/upgrade/03-complete-upgrade.tf" >}}
{{< /highlight >}}

Successfully running `tofu apply` a third time should ultimately yield all nodes running on Talos v1.7.5 with upgraded
kernel and containerd versions.

```shell
NAME      STATUS   ROLES           VERSION   OS-IMAGE         KERNEL-VERSION   CONTAINER-RUNTIME
ctrl-00   Ready    control-plane   v1.30.0   Talos (v1.7.5)   6.6.33-talos     containerd://1.7.18
ctrl-01   Ready    control-plane   v1.30.0   Talos (v1.7.5)   6.6.33-talos     containerd://1.7.18
ctrl-02   Ready    control-plane   v1.30.0   Talos (v1.7.5)   6.6.33-talos     containerd://1.7.18
```

## Potential improvements

The above recipe is fairly complex,
and I'm sure not without room for improvement.

Below are some of the improvements I've though about while writing this article.
If you have any comments or other ideas for improvement I would love to hear from you!

### Talos Linux Image Schematic ID

The 0.6.0 release of the Talos Provider promises support for their Image Factory.
An improvement would be to change the current custom code to fetch the schematic ID to use Sidero Labs' own
implementation.

The ability to use different schematics for different nodes would also be interesting,
though it would add complexity to the current upgrade procedure.

### Machine config

The possibility of overriding machine config to allow for a non-homogeneous cluster would probably be a nifty feature to
have, though I see no pressing need for one.

There's also lot more that can be configured using the machine config,
e.g. like [Bernd Schorgers](https://github.com/bjw-s) (bjw-s) has
done [here](https://github.com/bjw-s/home-ops/blob/1ab0a30665b85888b89c7c2facb58663a5f028b0/kubernetes/main/bootstrap/talos/talconfig.yaml).

### Networking

The current implementation uses the default Proxmox network bridge.
An improvement would be to create at a dedicated subnet for the Kubernetes cluster and configure firewall rules.

[Serge Logvinov](https://github.com/sergelogvinov) has
done [some work](https://github.com/sergelogvinov/terraform-talos/tree/main/proxmox) on this that might be interesting
to check out properly.

Load balancing and IPv6 would also be interesting to implement.

### Storage

Implementing Ceph for distributed storage would be a great improvement to allow pods to not be bound to a single
physical hypervisor node.
It would also allow some for of fail-over in case a node should become unresponsive.

Petitioning the maintainers of the Proxmox provider used here to be able to directly create VM disks would allow us to
ditch the Mastercard REST API provider in favour of fewer dependencies and possibly a more streamlined experience.

I've opened a [GitHub issue](https://github.com/bpg/terraform-provider-proxmox/issues/1465) with the maintainers of the
Proxmox provider to ask for this feature.

### Cluster upgrading

I find the current approach to upgrading the cluster by destroying and recreating VMs to not be the most elegant
solution.
A better solution would be to hook into the already existing `talosctl` capabilities to gracefully upgrade the cluster
as suggested by this [GitHub issue](https://github.com/siderolabs/terraform-provider-talos/issues/140),
though [this reply](https://github.com/siderolabs/terraform-provider-talos/issues/140#issuecomment-2178169530) is
somewhat discouraging.

Currently, changing the `cluster.talos_version` variable will destroy and recreate the whole cluster,
something that not might not be wanted when upgrading from e.g. v1.7.5 to v1.8.0 when that version becomes available.

## Summary

The resources created in this article can be found in the repository hosting the code for this site
at [GitLab](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/08/talos-proxmox-tofu/resources).

You can find a snapshot of my homelab IaC configuration running this
setup on [GitHub](https://github.com/vehagn/homelab/tree/e24a059de66edcd1fa32dd82dca9f0b4a3aeee7f).

```shell
🗃️
├── 📂 kubernetes
│   └── 📂 cilium
│       ├── 📋 kustomization.yaml
│       ├── 📄 announce.yaml
│       ├── 📄 ip-pool.yaml
│       └── 📄 values.yaml
└── 📂 tofu
    ├── 📝 providers.tf
    ├── 📝 variables.tf
    ├── 📃 proxmox.auto.tfvars
    ├── 📝 main.tf
    ├── 📝 output.tf
    ├── 📂 talos
    │   ├── 📝 providers.tf
    │   ├── 📝 variables.tf
    │   ├── 📝 image.tf
    │   ├── 📝 config.tf
    │   ├── 📝 virtual-machines.tf
    │   ├── 📝 output.tf
    │   ├── 📂 image
    │   │   └── 📄 schematic.yaml
    │   ├── 📂 machine-config
    │   │   ├── 📋 control-plane.yaml.tftpl
    │   │   └── 📋 worker.yaml.tftpl
    │   └── 📂 inline-manifests
    │       └── 📄 cilium-install.yaml
    └── 📂 bootstrap
        ├── 📂 sealed-secrets
        │   ├── 📝 providers.tf
        │   ├── 📝 variables.tf
        │   └── 📝 config.tf
        ├── 📂 proxmox-csi-plugin
        │   ├── 📝 providers.tf
        │   ├── 📝 variables.tf
        │   └── 📝 config.tf
        └── 📂 volumes
            ├── 📂 persistent-volume
            │   ├── 📝 providers.tf
            │   ├── 📝 variables.tf
            │   └── 📝 config.tf
            ├── 📂 proxmox-volume
            │   ├── 📝 providers.tf
            │   ├── 📝 variables.tf
            │   └── 📝 config.tf
            ├── 📝 providers.tf
            ├── 📝 variables.tf
            └── 📝 main.tf
```

### Main Kubernetes Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/proxmox.auto.tfvars" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/main.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/output.tf" >}}
{{< /highlight >}}

#### Cilium

{{< highlight yaml >}}
{{< readfile-rel file="resources/kubernetes/cilium/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kubernetes/cilium/announce.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kubernetes/cilium/ip-pool.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kubernetes/cilium/values.yaml" >}}
{{< /highlight >}}

### Talos Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/image.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/config.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/virtual-machines.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/talos/output.tf" >}}
{{< /highlight >}}

#### Image Schematic

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/image/schematic.yaml" >}}
{{< /highlight >}}

#### Machine config

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/machine-config/control-plane.yaml.tftpl" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/machine-config/worker.yaml.tftpl" >}}
{{< /highlight >}}

#### Inline Manifests

{{< highlight yaml >}}
{{< readfile-rel file="resources/tofu/talos/inline-manifests/cilium-install.yaml" >}}
{{< /highlight >}}

### Sealed Secrets Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/sealed-secrets/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/sealed-secrets/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/sealed-secrets/config.tf" >}}
{{< /highlight >}}

### Proxmox CSI Plugin Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/proxmox-csi-plugin/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/proxmox-csi-plugin/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/proxmox-csi-plugin/config.tf" >}}
{{< /highlight >}}

### Volumes Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/main.tf" >}}
{{< /highlight >}}

#### Proxmox Volume Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/proxmox-volume/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/proxmox-volume/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/proxmox-volume/config.tf" >}}
{{< /highlight >}}

#### Persistent Volume Module

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/persistent-volume/providers.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/persistent-volume/variables.tf" >}}
{{< /highlight >}}

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/bootstrap/volumes/persistent-volume/config.tf" >}}
{{< /highlight >}}

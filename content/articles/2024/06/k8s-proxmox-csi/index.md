---
title: "Kubernetes Proxmox Container Storage Interface"
date: 2024-06-17T18:00:00+02:00
lastmod: 2024-07-10T12:45:00+02:00

categories:
  - posts
  - kubernetes

tags:
  - k8s
  - kubernetes
  - homelab
  - proxmox
  - storage
  - csi
  - container storage interface
---

If you plan to run stateful applications in your Kubernetes cluster,
you quickly run into the question of where to store this state.
A database is often the best solution,
though writing to disk is sometimes the only
— or at least easier, option.

In this article we'll take a look at the [Proxmox CSI Plugin](https://github.com/sergelogvinov/proxmox-csi-plugin),
authored and maintained by [Serge](https://sinextra.dev).
This plugin is an implementation of
the [Container Storage Interface](https://github.com/container-storage-interface/spec/blob/master/spec.md) (CSI)
for [Kubernetes](https://kubernetes.io/)
using [Proxmox Virtual Environment](https://www.proxmox.com/en/proxmox-virtual-environment/overview) backed volumes,
which can be expanded to include e.g. [Ceph](https://ceph.io/).

The simplest solution for Kubernetes workloads is a _hostPath_ volume,
though that comes with its own [warnings and caveats](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath).
Other options include [NFS](https://en.wikipedia.org/wiki/Network_File_System)-mounting
— which might be slow,
or more complicated setup using [Rook](https://rook.io/).
A good middle ground might also be [Longhorn](https://longhorn.io/).

## Overview

The Proxmox CSI Plugin assumes a Kubernetes instance running on VMs inside a Proxmox cluster.
I've written about how to set up [Kubernetes on Proxmox]({{< ref "/articles/2024/03/proxmox-k8s-with-cilium" >}})
using [OpenTofu](https://opentofu.org/) in an earlier article,
though there are of
course [other](https://jmcglock.substack.com/p/running-a-kubernetes-cluster-using)
great [resources](https://jmcglock.substack.com/p/running-a-kubernetes-cluster-using-1de)
on [how to do this](https://www.khanhph.com/install-proxmox-kubernetes/).

Proxmox CSI requires your Proxmox instance to be clustered,
even though you only have one node.
The plugin also requires correct labels on each Kubernetes node,
indicating which _region_ (Proxmox cluster) and _zone_ (Proxmox node) the Kubernetes node is running on.

{{< mermaid-init >}}

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/cluster.mmd" >}}
{{< /mermaid-chart >}}

As an example we'll use a three node Proxmox cluster called _homelab_ consisting of three nodes
— [abel](https://en.wikipedia.org/wiki/Niels_Henrik_Abel),
[cantor](https://en.wikipedia.org/wiki/Georg_Cantor), and
[euclid](https://en.wikipedia.org/wiki/Euclid).
Each node hosts a virtual machine that we'll use to create our Kubernetes cluster control plane
— `ctrl-00`, `ctrl-01`, and `ctrl-02`.
For convenience each VM will act as a control plane,
but with the corresponding [taint](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)
removed, allowing us to run regular workloads on them.
We're also adding a fourth node on the abel node called `work-00` which is only for workloads.

Initially,
we'll create [PersistentVolumes](https://kubernetes.io/docs/concepts/storage/persistent-volumes/) attached to
the `ctrl-00` VM before we take a look at how the volumes can be moved around in our cluster.

## Configuration

Before taking advantage of the Proxmox CSI plugin we need to configure both our Proxmox cluster and Kubernetes
installation.

### Proxmox

A prerequisite for the CSI plugin is a clustered Proxmox installation.
This can easily be done both through the web GUI and using the `pvecm` command line tool
as [described in the Proxmox wiki](https://pve.proxmox.com/wiki/Cluster_Manager#pvecm_create_cluster).
In the web GUI the cluster setting is found under the _Datacenter_ view as shown below.

{{< dynamic-image title="Cluster information showing three nodes; abel, cantor, and euclid."
alt="Cluster information" dark="images/cluster-information-dark.png" light="images/cluster-information-light.png" >}}

The CSI plugin also requires a Proxmox user to be able to automatically provision the requested storage.
Following
the [Proxmox CSI Plugin readme](https://github.com/sergelogvinov/proxmox-csi-plugin?tab=readme-ov-file#install-csi-driver),
this can be done using the `pveum` CLI-tool from one of the Proxmox nodes to create a `CSI`-role with the following
privileges

```shell
pveum role add CSI -privs "VM.Audit VM.Config.Disk Datastore.Allocate Datastore.AllocateSpace Datastore.Audit"
```

We then create a new user called `kubernetes-csi` in the `pve` realm

```shell
pveum user add kubernetes-csi@pve
```

and give it the `CSI` role we just created for all VMs

```shell
pveum aclmod / -user kubernetes-csi@pve -role CSI
```

The [Tofu](https://opentofu.org/)/[Terraform](https://www.terraform.io/) code to create the above role and user can be
found in the [Summary](#summary) section.
In the web GUI this user should now look like in the figure below.

{{< dynamic-image title="Proxmox user for Kubernetes CSI plugin"
alt="Kubernetes CSI user" dark="images/csi-user-dark.png" light="images/csi-user-light.png" >}}

Having a user with only the required privileges at hand we can now create a token without privilege separation by
running

```shell
pveum user token add kubernetes-csi@pve csi -privsep 0
```

This should give you something similar to

```shell
┌──────────────┬──────────────────────────────────────┐
│ key          │ value                                │
╞══════════════╪══════════════════════════════════════╡
│ full-tokenid │ kubernetes-csi@pve!csi               │
├──────────────┼──────────────────────────────────────┤
│ info         │ {"privsep":"0"}                      │
├──────────────┼──────────────────────────────────────┤
│ value        │ c00010ff-f00d-c0de-dead-dabbad00cafe │
└──────────────┴──────────────────────────────────────┘
```

where we're interested in the value field for the next section.

Finally, make sure that the VM name in Proxmox matches the node [hostname](https://en.wikipedia.org/wiki/Hostname)
— i.e. the hostname of Proxmox VM `ctrl-00` should be `ctrl-00`,
as Proxmox CSI Plugin needs this information in order to provision a disk to the correct VM.
This can be an issue you're using e.g. [Talos](https://www.talos.dev/),
— which generates a random hostname by default,
as [reported](https://github.com/sergelogvinov/proxmox-csi-plugin/issues/192#issuecomment-2177890121)
by [@breakingflower](https://github.com/breakingflower) on GitHub.

### Kubernetes

The Proxmox CSI plugin needs to know the [topology](https://en.wikipedia.org/wiki/Network_topology)
of our Kubernetes cluster to be able to provision storage on the correct node.
In order to determine cluster topology,
the plugin uses the
well-known [topology.kubernetes.io/region](https://kubernetes.io/docs/reference/labels-annotations-taints/#topologykubernetesioregion)
and [topology.kubernetes.io/zone](https://kubernetes.io/docs/reference/labels-annotations-taints/#topologykubernetesiozone)
labels.

To help with the labelling, Serge has
created [Proxmox Cloud Controller Manager](https://github.com/sergelogvinov/proxmox-cloud-controller-manager/tree/main)
(CCM)
— an implementation
of [Kubernetes Cloud Controller Manager](https://kubernetes.io/docs/tasks/administer-cluster/running-cloud-controller/).
It's unfortunately [a bit finicky](https://github.com/sergelogvinov/proxmox-cloud-controller-manager/issues/111) to
[configure correctly](https://github.com/sergelogvinov/proxmox-cloud-controller-manager/issues/63),
so I ended up [throwing in the towel](https://dictionary.cambridge.org/dictionary/english/throw-in-the-towel) and
labelling the Kubernetes nodes manually

```shell
kubectl label nodes --all topology.kubernetes.io/region=homelab
kubectl label node ctrl-00 work-00 topology.kubernetes.io/zone=abel
kubectl label node ctrl-01 topology.kubernetes.io/zone=euclid
kubectl label node ctrl-02 topology.kubernetes.io/zone=cantor
```

With the nodes labelled we can install the Proxmox CSI plugin either through [Helm](https://helm.sh/),
or [Kustomize](https://kustomize.io/) following
the [readme](https://github.com/sergelogvinov/proxmox-csi-plugin?tab=readme-ov-file#method-2-by-helm).

The plugin is configured using a Secret with the cluster information

{{< highlight yaml "linenos=table,hl_lines=10-14">}}
{{< readfile-rel file="resources/k8s/proxmox-csi/proxmox-csi-config.yaml" >}}
{{< /highlight >}}

filling in `<NODE>` (line 10) with either a node IP or hostname of one of the Proxmox nodes.
If you're using a self-signed certificate you might have to enable an insecure connection (line 11).
The `token_id` (line 12) should be the user we created in the previous section with the associated token in
the `token_secret` field (line 13).
The `region` (line 14) should correspond to the Proxmox cluster name.

Next we create a [`StorageClass` definition](https://kubernetes.io/docs/concepts/storage/storage-classes/) that uses
the Proxmox CSI plugin as a provisioner (line 14)

{{< highlight yaml "linenos=table,hl_lines=9 14">}}
{{< readfile-rel file="resources/k8s/proxmox-csi/storage-class.yaml" >}}
{{< /highlight >}}

The `storage` parameter (line 9) must also match a Proxmox storage ID supporting disk image content as shown in the
picture below

{{< dynamic-image title="Proxmox user for Kubernetes CSI plugin"
alt="Kubernetes CSI user" dark="images/storage-dark.png" light="images/storage-light.png" >}}

More options for the Proxmox CSI Plugin Storage Class are described in
the [documentation](https://github.com/sergelogvinov/proxmox-csi-plugin/blob/main/docs/options.md).

With the configuration in place we can install Proxmox CSI plugin by fetching the relevant manifests directly from
the [GitHub repository](https://github.com/sergelogvinov/proxmox-csi-plugin/blob/main/docs/deploy/proxmox-csi-plugin-release.yml)
from a given tag

{{< highlight yaml >}}
{{< readfile-rel file="resources/k8s/proxmox-csi/kustomization.yaml" >}}
{{< /highlight >}}

If everything is working as expected, you should be able to run

```shell
kubectl get csistoragecapacities -ocustom-columns=CLASS:.storageClassName,AVAIL:.capacity,ZONE:.nodeTopology.matchLabels -A
```

to display the available storage on each node

```shell
proxmox-csi   821991632Ki   map[topology.kubernetes.io/region:homelab topology.kubernetes.io/zone:euclid]
proxmox-csi   916754956Ki   map[topology.kubernetes.io/region:homelab topology.kubernetes.io/zone:cantor]
proxmox-csi   960049560Ki   map[topology.kubernetes.io/region:homelab topology.kubernetes.io/zone:abel]
```

## Usage

Using Proxmox CSI Plugin to provision `PersistentVolumes` is done by referencing the `proxmox-csi` `StorageClass` we
just created.
We'll take a look at creating ephemeral volumes as well as more persistent volumes.

### Ephemeral volumes

[Ephemeral](https://www.merriam-webster.com/dictionary/ephemeral) means _short-lived_.
In the context of Kubernetes,
an [ephemeral volume](https://kubernetes.io/docs/concepts/storage/ephemeral-volumes/) is a volume that follows a Pod's
lifetime and is defined inline with the Pod spec

{{< highlight yaml "linenos=table,hl_lines=9 23">}}
{{< readfile-rel file="resources/k8s/examples/pod-with-ephemeral-volume.yaml" >}}
{{< /highlight >}}

Notice we've bound the pod to the `ctrl-00` Kubernetes node (line 9) and referenced the `proxmox-csi` `StoreageClass` on
line 23.

Applying the above manifest we should now see both a `PersistentVolumeClaim` and a `PersistentVolume` created along with
the `Pod`

```shell
kubectl get pod,pv,pvc -n proxmox-csi-test
NAME       READY   STATUS    RESTARTS   AGE
pod/pod-test   1/1     Running   0          51m

NAME                                                        CAPACITY   ACCESS MODES   RECLAIM POLICY   STATUS   CLAIM                                        STORAGECLASS   VOLUMEATTRIBUTESCLASS   REASON   AGE
persistentvolume/pvc-e948aab3-705d-4544-9ef4-126a5b9b96a7   2Gi        RWO            Retain           Bound    proxmox-csi-test/pod-test-ephemeral-volume   proxmox-csi    <unset>                          51m

NAME                                              STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   VOLUMEATTRIBUTESCLASS   AGE
persistentvolumeclaim/pod-test-ephemeral-volume   Bound    pvc-e948aab3-705d-4544-9ef4-126a5b9b96a7   2Gi        RWO            proxmox-csi    <unset>                 51m
```

The volume can also be seen attached to the `ctrl-00` VM in the Proxmox Web GUI

{{< dynamic-image title="Proxmox CSI Plugin provisioned volume in the web GUI"
alt="Proxmox CSI Plugin volume" dark="images/pod-volume-dark.png" light="images/pod-volume-light.png" >}}

Since the Pod manifest creates an ephemeral volume
— meaning it will be recreated together with the pod and the previous data lost,
we can schedule this Pod on all of our nodes.

Deleting the pod, the volume should also be removed from the VM.
Since we've chosen a `Retain` _reclaimPolicy_ for the `StorageClass` we're using,
the disk won't be deleted and can be found on the Proxmox node.

{{< dynamic-image title="Detached volume in the Proxmox web GUI"
alt="Detached volume" dark="images/detached-volume-dark.png" light="images/detached-volume-light.png" >}}

This makes it easier to recover data in case the persistent volume was deleted prematurely by mistake.

Recreating the Pod a new volume is provisioned,
meaning that you should consider a different _reclaimPolicy_ if you plan on running many short-lived Pods with ephemeral
volumes.

### Persistent volumes

Using a CSI driver we can create a `PersistentVolume` using a `PersistentVolumeClaim`

{{< highlight yaml "linenos=table,hl_lines=8">}}
{{< readfile-rel file="resources/k8s/examples/pvc.yaml" >}}
{{< /highlight >}}

The `StorageClass` we're using (line 8) is configured with _volumeBindingMode_ to `WaitForFirstConsumer`,
so we need to said consumer, which can be a Deployment Pod

{{< highlight yaml "linenos=table,hl_lines=10 20 31">}}
{{< readfile-rel file="resources/k8s/examples/deployment-with-pvc.yaml" >}}
{{< /highlight >}}

We've bound the Deployment Pod to the `ctrl-00` node (line 21) which is where the _persistentVolumeClaim_ referenced
on line 32 will thus have its volume provisioned.
To be able to move the Deployment Pod to a different node we need to detach the volume before trying to start the Pod
on the other node.
This can be done by setting the Deployment _type_
to [`Recreate`](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#recreate-deployment) (line 10),
which kills existing pod
— thus detaching the volume,
before starting it on a different node since we're using the
`ReadWriteOnce` [_accessMode_](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#access-modes).

Deleting the `Deployment` will keep the provisioned volume attached to the node,
and recreating it should attach it again with the data intact.

{{< dynamic-image title="Provisioned volume attached to node ctrl-00"
alt="Volume attached to node ctrl-00" dark="images/pvc-ctrl-00-dark.png" light="images/pvc-ctrl-00-light.png" >}}

Schematically this looks like the graph below

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/ctrl-00-volume.mmd" >}}
{{< /mermaid-chart >}}

To move the Pod to a different node we can change the _nodeSelector_ field of the Pod spec in the Deployment to e.g.
`kubernetes.io/hostname: work-00`.

{{< dynamic-image title="Provisioned volume attached to node work-00"
alt="Volume attached to node work-00" dark="images/pvc-work-00-dark.png" light="images/pvc-work-00-light.png" >}}

This will prompt the Proxmox CSI Plugin to move the provisioned volume from the `ctrl-00` node to the `work-00` node.
Since both nodes are running on the same Proxmox hypervisor host (zone),
the plugin is able to move the disk automatically.

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/work-00-volume.mmd" >}}
{{< /mermaid-chart >}}

If we instead change the _nodeSelector_ to `ctrl-01`
— which is hosted on `euclid`, a different Proxmox node/zone,
we'll be met with

```
0/4 nodes are available: 1 node(s) had volume node affinity conflict, 3 node(s) didn't match Pod's node affinity/selector.
preemption: 0/4 nodes are available: 4
Preemption is not helpful for scheduling. 
```

[According to the author](https://github.com/sergelogvinov/proxmox-csi-plugin/issues/157#issuecomment-2159923924) this
is due to a limitation in the Kubernetes API,
though he has graciously created a CLI tool to overcome this shortcoming,
called [`pvecsictl`](https://github.com/sergelogvinov/proxmox-csi-plugin/blob/main/docs/pvecsictl.md).

The `pvecsictl` tool is available from the Proxmox CSI Plugin repository as
a [Release](https://github.com/sergelogvinov/proxmox-csi-plugin/releases) and takes configuration similar to the plugin

{{< highlight yaml "linenos=table">}}
{{< readfile-rel file="resources/cmd/config.yaml" >}}
{{< /highlight >}}

The tool requires root-privileges since it's POST-ing to
an [experimental endpoint](https://pve.proxmox.com/pve-docs/api-viewer/#/nodes/{node}/storage/{storage}/content/{volume})
which is root only.

Running `pvecsictl` with the following arguments we move the disk to the `euclid` node

```shell
pvecsictl migrate --config=cmd/config.yaml -n proxmox-csi-test pvc-test euclid 
INFO moving disk vm-9999-pvc-0b328ae5-00db-4f14-afa2-8180910f456d to proxmox node euclid 
INFO replacing persistentvolume topology          
INFO persistentvolumeclaims pvc-test has been migrated to proxmox node euclid 
```

With the disk moved to the same physical node where we've constrained the Pod to run,
it has no trouble starting up, and we should find the same data on the volume

{{< dynamic-image title="Provisioned volume attached to node ctrl-01 on euclid"
alt="Volume attached to node ctrl-01" dark="images/pvc-ctrl-01-dark.png" light="images/pvc-ctrl-01-light.png" >}}

or schematically as

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/ctrl-01-volume.mmd" >}}
{{< /mermaid-chart >}}

Doing the above with a [`StatefulSet`](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/) is left
as an exercise to the reader

{{< highlight yaml "linenos=table">}}
{{< readfile-rel file="resources/k8s/examples/stateful-set-with-pvc.yaml" >}}
{{< /highlight >}}

## Addendum

### Proxmox CSI

My opinion of the Proxmox CSI plugin is of a great middle ground between ease-of-use and abstraction.
The provisioned disks can relatively easily be mounted in a different VM/container if needed to inspect the data on
them.
It's also possible to configure VM backups with the disk included,
though [some care](https://github.com/sergelogvinov/proxmox-csi-plugin/issues/201#issuecomment-2159908332) should be
taken if you decide to do this.

In the future I'd like to experiment with shared storage
like [Ceph](https://pve.proxmox.com/wiki/Deploy_Hyper-Converged_Ceph_Cluster) on Proxmox to see if the extra overhead is
worth it in my humble homelab.

### Proxmox CCM

It bothers me that I didn't get [Proxmox CCM](https://github.com/sergelogvinov/proxmox-cloud-controller-manager) to work
properly.
Trying to follow [The Noflake Manifesto](https://noflake.org) I want the VMs to be interchangeable
without much fuss,
having something automatically label the node topology would help with this.
Scheduling a Kubernetes VM on a different Proxmox node would mean that I need to manually change the labels.

When I started writing this article I initially used a [Debian](https://www.debian.org) image
and [my own cloud-init debauchery]({{< ref "/articles/2024/03/proxmox-k8s-with-cilium" >}}) to get Kubernetes up and
running.
I tried
following [tips](https://github.com/sergelogvinov/proxmox-cloud-controller-manager/issues/63#issuecomment-1774747738)
from
GitHub [issues](https://github.com/sergelogvinov/proxmox-cloud-controller-manager/issues/63#issuecomment-1775572964),
adding `KUBELET_EXTRA_ARGS="--node-ip=192.168.1.100 --cloud-provider=external"` in `/etc/default/kubelet` and restarting
the kubelet service, but to no avail.

While writing this article I also switched over to using [Talos](https://www.talos.dev) (more on that later!).
I therefore postponed looking at Proxmox CCM and instead labelled the nodes manually.

### Named volumes

The volumes created are given a randomly generated [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier)
which doesn't tell you anything about which resource owns the volume.
I optimistically tried setting
the [_spec.volumeName_](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#reserving-a-persistentvolume)
field in a `PersistentVolumeClaim` which I thought might be used to name a volume,
but it's used to bind to existing volumes, not create new ones.

A [recent GitHub issue](https://github.com/sergelogvinov/proxmox-csi-plugin/issues/205) addresses the naming issue,
though I figured out you can manually create a VM disk using `pvesm`
— [Proxmox VE Storage Manager](https://pve.proxmox.com/pve-docs/pvesm.1.html),
which you can then refer to in a `PersistentVolume`.

As an example,
say we want to create a `config` volume for an application running on the `ctrl-00` node with VM ID 8000.
The `ctrl-00` Kubernetes node is hosted on the `abel` hypervisor in the `homelab` cluster.

First, open a shell on `abel` and run

```shell
pvesm alloc local-zfs 8000 vm-8000-app-config 1G
```

This will allocate 1 GB on the `local-zfs` storage for VM 8000 and be named `vm-8000-app-config`.
We can then reference the manually allocated disk in the _volumeHandle_ field of a `PersistentVolume` (line 18),
and consequently reference the `PersistentVolume` using the _volumeName_ field in a `PersistentVolumeClaim` (line 30)

{{< highlight yaml "linenos=table,hl_lines=18 30">}}
{{< readfile-rel file="resources/k8s/examples/named-volume.yaml" >}}
{{< /highlight >}}

Although this approach isn't scalable,
it works for a small amount of volumes if the disk name is important.

## Summary

Since I can be a bit verbose,
I like to succinctly summarise the setup at the end.
All the resources used in this article can be found in the GitLab repo for this
blog [here](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/06/k8s-proxmox-csi/resources).

```shell
❯ tree
.
├── k8s
│   └── proxmox-csi
│       ├── kustomization.yaml
│       ├── proxmox-csi-config.yaml
│       └── storage-class.yaml
└── tofu
    └── kubernetes-csi-user.tf
```

To get Proxmox CSI running you first need cluster the Proxmox installation,
even if you only have one node.
Next you should create a dedicated CSI-role and Proxmox user.
Using [BGP's Proxmox Terraform Provider](https://github.com/bpg/terraform-provider-proxmox) this can be done with the
following configuration

{{< highlight terraform >}}
{{< readfile-rel file="resources/tofu/kubernetes-csi-user.tf" >}}
{{< /highlight >}}

On the Kubernetes side of things, you first need to correctly label your nodes
— either manually or using [Proxmox CCM](https://github.com/sergelogvinov/proxmox-cloud-controller-manager),
before applying the following kustomization

{{< highlight yaml >}}
{{< readfile-rel file="resources/k8s/proxmox-csi/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/k8s/proxmox-csi/proxmox-csi-config.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/k8s/proxmox-csi/storage-class.yaml" >}}
{{< /highlight >}}

Proxmox CSI plugin can move `PersistentVolumes` between VMs on the same hypervisor node.
To move volumes to a VM on a different hypervisor node you can use the `pvecsictl` tool.
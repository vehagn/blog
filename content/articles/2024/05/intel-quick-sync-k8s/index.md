---
title: "Intel Quick Sync Video with Kubernetes"
date: 2024-05-28T20:50:00+02:00
lastmod: 2024-05-31T21:40:00+02:00

categories:
  - posts
  - kubernetes

tags:
  - k8s
  - kubernetes
  - qsv
  - intel
  - quick sync
  - quick sync video
  - hardware transcoding
  - homelab
  - debian
  - plex
  - jellyfin
  - emby
  - proxmox
  - nfd
  - node feature discovery
---

Taking advantage of [hardware acceleration](https://wiki.debian.org/HardwareVideoAcceleration) can drastically reduce
both CPU and power usage when playing video,
enabling seemingly innocuous chips
— like
the [Intel N100](https://ark.intel.com/content/www/us/en/ark/products/231803/intel-processor-n100-6m-cache-up-to-3-40-ghz.html),
to render multiple [4K video](https://en.wikipedia.org/wiki/4K_resolution) streams.

In this article we'll go through how to
enable [Intel<sup>®</sup> Quick Sync Video](https://en.wikipedia.org/wiki/Intel_Quick_Sync_Video)
for hardware accelerated transcoding in containers orchestrated by [Kubernetes](https://kubernetes.io/).
As an example we'll use both [Jellyfin](https://jellyfin.org/) and [Plex](https://www.plex.tv/),
though it should work similar for other media server solutions like [Emby](https://emby.media/)
and [Universal Media Server](https://www.universalmediaserver.com/).

Other
— arguably more lightweight,
containerised solutions also exists using e.g. [LXC](https://linuxcontainers.org/) as described
by [Cipher Menial](https://ciphermenial.github.io/) in
their [Configure GPU Passthrough to LXD Container](https://ciphermenial.github.io/posts/jellyfin-gpu-passthrough/)
article.

## Background

This article is written using
a[CWWK mini-PC](https://cwwk.net/products/6-lan-firewall-appliance-2-5g-router-12th-gen-intel-i3-n305-n100-ddr5-2-nvme-2-sata3-0-fanless-mini-pc-esxi-proxmox-host?variant=44695403331816)
with
an [Intel i3-N305](https://www.intel.com/content/www/us/en/products/sku/231805/intel-core-i3n305-processor-6m-cache-up-to-3-80-ghz/specifications.html)
CPU,
though just about any newer non-F Intel CPU should work.
If in doubt that your CPU comes with an integrated GPU,
you can check a list of Quick Sync Video
support [here](https://ark.intel.com/content/www/us/en/ark/search/featurefilter.html?productType=873&0_QuickSyncVideo=True).

I'm running [Proxmox VE 8.2](https://www.proxmox.com/en/proxmox-virtual-environment/overview) as a hypervisor.
This extra abstraction layer makes experimenting easier,
but also adds complexity as you need to pass through the iGPU to a virtual machine.
I've tried to summarise how to do this in
an [earlier article]({{< ref "/articles/2024/03/proxmox-k8s-with-cilium/#pci-passthrough" >}}).

I opted to go for a [Debian](https://www.debian.org/) 12 Bookworm [backports](https://backports.debian.org/) image[^3],
due to reports on Reddit[^1] of support on hardware transcoding for newer Intel CPUs
requiring [Linux kernel](https://www.kernel.org) version 6.2 or newer
— along with changes to the 6.8 version reportedly[^2] breaking hardware transcoding.
The image I chose ships with Linux kernel 6.6 and appears to work.
A bare-metal installation of Debian 12 with backports enabled and a newer kernel should work similar.

[^1]: Linux kernel 6.1 and older reported not working by
user [calinet2](https://www.reddit.com/user/calinet6/) on Reddit:
[comment 1](https://www.reddit.com/r/PleX/comments/11vf91z/comment/jtrlidz/)
[comment 2](https://www.reddit.com/r/PleX/comments/14qs9zs/comment/jv1sjd1/).

[^2]: Linux kernel 6.8 reported having problem with hardware transcoding on
the [Plex forums](https://forums.plex.tv/t/ubuntu-24-04-hw-transcoding/873765).

[^3]: The specific Debian image used during the writing of this article
was [debian-12-backports-generic-amd64-20240429-1732](https://cloud.debian.org/images/cloud/bookworm-backports/20240429-1732/debian-12-backports-generic-amd64-20240429-1732.qcow2).

## Overview

This article will assume you already have a working Kubernetes cluster.
If you don't
— and ~~have too much spare time~~ would like to try your hands at Kubernetes,
I've written articles
on [Bootstrapping k3s with Cilium]({{< ref "/articles/2024/02/bootstrapping-k3s-with-cilium" >}}),
and
— ~~if you have way too much spare time~~,
running [Kubernetes on Proxmox]({{< ref "/articles/2024/03/proxmox-k8s-with-cilium" >}}).

We'll start by installing the required packages on the Kubernetes host OS before deploying the required operators to get
hardware acceleration with Quick Sync to work in our containers.

Next we take a look at manually mapping the iGPU-device into the container and explore the
required [Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) to utilise the
GPU.

Before wrapping up the article,
we take a look at hardening pods by limiting privileges.

If you're not one for details and diatribe you can skip to the [Summary](#summary) section for a quick guide on how to
get Quick Sync to work in Kubernetes.

## Prerequisites

Before we try to get Quick Sync to work with containers we should make sure that everything works as expected on the
Kubernetes host machine.

Firmware for
Intel's [integrated GPUs](https://www.intel.com/content/www/us/en/products/docs/processors/what-is-a-gpu.html)
is available in the _non-free-firmware_ repository of Debian in
the [`firmware-misc-nonfree` package](https://packages.debian.org/bookworm/all/firmware-misc-nonfree).
To enable [VA-API](https://en.wikipedia.org/wiki/Video_Acceleration_API) you probably also want to grab the
[`intel-media-va-driver-non-free` package](https://packages.debian.org/bookworm/intel-media-va-driver-non-free) from
the _non-free_ repository.
You should also install the `intel-gpu-tools` package from the _main_ repository.

If you haven't already added the _non-free_ and _non-free-firmware_ sources
(check the `Components:` line in the `/etc/apt/sources.list.d/debian.sources`),
you can add them by running

```shell
sudo sed -i '/Components/s/$/ non-free non-free-firmware/' /etc/apt/sources.list.d/debian.sources
```

The `debian.sources` file should now look similar to

```yaml
Types: deb deb-src
URIs: mirror+file:///etc/apt/mirrors/debian.list
Suites: bookworm bookworm-updates bookworm-backports
Components: main non-free non-free-firmware

Types: deb deb-src
URIs: mirror+file:///etc/apt/mirrors/debian-security.list
Suites: bookworm-security
Components: main non-free non-free-firmware
```

Next update your local apt repository and install the packages

```shell
sudo apt update
sudo apt install firmware-misc-nonfree intel-media-va-driver-non-free intel-gpu-tools
```

With the new firmware installed,
reboot the machine and run

```shell
sudo intel_gpu_top
```

If everything works you should now see GPU usage

```
intel-gpu-top: Intel Alderlake_n (Gen12) @ /dev/dri/card1      0/   0 MHz
         ENGINES     BUSY                         MI_SEMA MI_WAIT
       Render/3D    0.00% |                             |      0%      0%
         Blitter    0.00% |                             |      0%      0%
           Video    0.00% |                             |      0%      0%
    VideoEnhance    0.00% |                             |      0%      0%
```

If you're instead met with

```
intel_gpu_top: ../tools/intel_gpu_top.c:1932: init_engine_classes: Assertion `max >= 0' failed.
Aborted
```

something is obviously wrong.
It possibly means you need to update your Linux kernel.
To check you current kernel version run

```shell
uname -a
```

According to the reports mentioned in the [Background](#background)[^1] section this should be version 6.2 or newer.
Check for available kernel versions by running

```shell
apt-cache search linux-image
```

and pick a suitable kernel which can be installed by running e.g.

```shell
sudo apt install linux-image-6.6.13+bpo-amd64
```

At the time of writing there's reports[^2] of the 6.8 kernel having trouble with hardware decoding,
so you might want to avoid that.

For more details on upgrading your kernel,
take a look at the [Debian wiki](https://wiki.debian.org/HowToUpgradeKernel).

There's also custom kernels like [Liquorix](https://liquorix.net/) or [XanMod](https://xanmod.org/)
— or you could try
to [compile you own kernel](https://kernel-team.pages.debian.net/kernel-handbook/ch-common-tasks.html#s-common-official).

## Node Feature Discovery (optional)

In
the [readme for installing the Intel Device Plugins Operator Helm Chart](https://github.com/intel/helm-charts/tree/main/charts/device-plugin-operator),
[Node Feature Discovery](https://kubernetes-sigs.github.io/node-feature-discovery/stable/get-started/index.html) (NFD)
is mentioned as an optional dependency.
It should be possible to label your nodes manually,
but I found it easier to run NFD instead of trying to figure out which labels are needed.

To install and configure NFD we can apply the following [Kustomization](https://kustomize.io/) file

{{< highlight yaml "linenos=table,hl_lines=7 9">}}
{{< readfile-rel file="resources/nfd/kustomization.yaml" >}}
{{< /highlight >}}

Line 7 fetches
the [default NFD v0.15.4 installation](https://github.com/kubernetes-sigs/node-feature-discovery/blob/v0.15.4/deployment/overlays/default/kustomization.yaml)
from GitHub.
Intel
provides [Node Feature Rules](https://kubernetes-sigs.github.io/node-feature-discovery/v0.15/usage/custom-resources.html#nodefeaturerule)
for their hardware which we fetch on line 9 from
their [intel-device-plugins-for-kubernetes repository](https://github.com/intel/intel-device-plugins-for-kubernetes/tree/release-0.30/deployments/nfd/overlays/node-feature-rules).

To configure the NFD components we can use [ConfigMaps](https://kubernetes.io/docs/concepts/configuration/configmap/).
Using
the [configMapGenerator](https://kubernetes.io/docs/tasks/manage-kubernetes-objects/kustomization/#configmapgenerator)
provided by Kustomize,
we can configure the NFD worker to only run on startup.
This is done by configuring a non-positive sleep interval, e.g. `0`

{{< highlight yaml "linenos=table">}}
{{< readfile-rel file="resources/nfd/nfd-worker.conf" >}}
{{< /highlight >}}

For more information about configuration parameters take a look at
the [NFD sigs documentation](https://kubernetes-sigs.github.io/node-feature-discovery/v0.15/reference/worker-configuration-reference#coresleepinterval).

Apply the above kustomization and check that the node labels have been updated by running

```shell
kubectl get node -o 'jsonpath={.items[*].metadata.labels}' | jq
```

If everything went well you should now see an `"intel.feature.node.kubernetes.io/gpu": "true"` label on your nodes with
an Intel GPU.
Since we've edited the NFD-worker config to only run once on startup,
make sure you run the NFD-worker after the Intel NFD rules have been applied,
or edit the configuration to run on an interval.

## Kubernetes Device Plugins

Intel has divided their support for the
Kubernetes [Device Plugins](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/device-plugins/)
framework into a
general [operator](https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/operator/README.md)
and several [plugins](https://github.com/intel/intel-device-plugins-for-kubernetes).

With the nodes correctly labelled we can now install the
Intel [Device Plugin](https://kubernetes.io/docs/concepts/extend-kubernetes/compute-storage-net/device-plugins/) for
Kubernetes.

## Cert-manager

The Intel Device Plugins Operator has a hard requirement on [Cert-manager](https://cert-manager.io/).

Cert-manager helps you create and manage TLS-certificates for workloads in Kubernetes.
If you're interested in learning more, I've written about how you can use Cert-manager to
create [Wildcard Certificates with Traefik]({{< ref "/articles/2023/12/traefik-wildcard-certificates" >}}).

Cert-manager can easily be [installed](https://cert-manager.io/docs/installation/kubectl/) by running

```shell
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.14.5/cert-manager.yaml
```

or as a [Helm Chart](https://cert-manager.io/docs/installation/helm/) as shown in the [Summary](#cert-manager-1)
section.

## Intel Device Plugins Operator

With the proper requirements in place,
the Intel Device Plugins Operator is
can be [installed](https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/INSTALL.md) as
a [Helm chart](https://helm.sh/).

Here we've taken advantage of the Kustomize
built-in [Helm Chart Inflation Generator](https://kubectl.docs.kubernetes.io/references/kustomize/kustomization/helmcharts/)
instead of installing it directly with Helm.

{{< highlight yaml "linenos=table">}}
{{< readfile-rel file="resources/intel-device-plugins/operator/kustomization.yaml" >}}
{{< /highlight >}}

After creating the `intel-device-plugins` namespace, run

```shell
kubectl kustomize --enable-helm intel-device-plugins/operator | kubectl apply -f -
```

to install the operator.

## Intel GPU Plugin

Once the Intel Device Plugins Operator is in place we can add Intel Device Plugins.
In our case we're only interested in
the [GPU plugin](https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/gpu_plugin/README.md),
though Intel provides [plugins](https://github.com/intel/intel-device-plugins-for-kubernetes) for a plethora of their
other hardware devices,
including [FPGAs](https://en.wikipedia.org/wiki/Field-programmable_gate_array) and other specialised hardware like the
Intel [In-Memory Analytics Acceleration](https://www.intel.com/content/www/us/en/products/docs/accelerator-engines/in-memory-analytics-accelerator.html).

We can configure the GPU plugin to allow multiple pods to share the same GPU resource,
here we've configured for up to five workloads to share each iGPU (line 3).
Since we've already applied the Intel NFD rules in a previous section we don't need to add them again (line 4).
We've also configured the plugin to only run on nodes with an Intel GPU as labelled by NFD (line 6).
For more configuration values take a look at
the [chart's values.yaml file](https://github.com/intel/helm-charts/blob/main/charts/gpu-device-plugin/values.yaml).

{{< highlight yaml "linenos=table, hl_lines=3 4 6">}}
{{< readfile-rel file="resources/intel-device-plugins/gpu/values.yaml" >}}
{{< /highlight >}}

There's experimental support
for [GPU Aware Scheduling](https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/gpu_plugin/fractional.md),
though it's only used for scheduling workloads to nodes,
not limiting usage.
A container requesting 10% can still use 100% of the GPU.

To install the GPU plugin without NFD follow the
guide [here](https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/gpu_plugin/advanced-install.md),
or possibly relax the `nodeSelector` criteria in the Helm chart values.

Using the above values we can again create a Kustomize file using the Helm chart inflation generator

{{< highlight yaml "linenos=table">}}
{{< readfile-rel file="resources/intel-device-plugins/gpu/kustomization.yaml" >}}
{{< /highlight >}}

and apply it

```shell
kubectl kustomize --enable-helm intel-device-plugins/operator | kubectl apply -f -
```

If you prefer you can of course just use Helm directly as well.

Verify that the plugin is installed correctly by running

```shell
kubectl get node -o 'jsonpath={.items[*].status.allocatable}' | jq 
```

which should display

```json
{
  ...
  "gpu.intel.com/i915": "5",
  ...
}
```

You can also try to run a sample workload as described in the Intel GPU
plugin [documentation](https://github.com/intel/intel-device-plugins-for-kubernetes/blob/main/cmd/gpu_plugin/README.md#testing-and-demos).

## Allocating GPU resources to a workload

Allocating GPU resources to a workload is as simple as setting a resource limit for the correct resource,
e.g. `gpu.intel.com/i915: 1000m`,
similar to how you would limit CPU, memory,
and [NVIDIA GPU resources]({{< ref "/articles/2024/01/cuda-on-kubernetes" >}}).

### Plex

{{< alert "circle-info">}}
Hardware acceleration requires a paid [Plex Pass](https://www.plex.tv/plex-pass/).
{{< /alert >}}

A minimal deployment of Plex using the official image from [DockerHub](https://hub.docker.com/r/plexinc/pms-docker) with
Intel GPU acceleration looks like

{{< highlight yaml "linenos=table">}}
{{< readfile-rel file="resources/plex-minimal-gpu.yaml" >}}
{{< /highlight >}}

A more detailed Deployment with a both a host and an NFS mount,
including an attached Service and Ingress can be found in this article's GitLab
repository [here](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/05/intel-quick-sync-k8s/resources/plex).

To make sure hardware transcoding works with Plex you can try to play some of the media you've added to your library.

If the media is encoded in a supported format we will get a direct stream
— which is better than transcoding.
We can check this in the Plex Dashboard while playing media.

![Plex direct stream](images/plex-direct-stream.webp "Plex showing direct streaming taking place")

To force transcoding we can select a lower quality.

![Plex hardware transcode](images/plex-hw-transcode.webp "Plex showing hardware transcoding taking place")

You should now see `(hw)` in the transcode information if everything works.
If you don't see `(hw)` in the transcode information it means that you're using software transcoding.
A possible remedy is to make sure you have the [required](#prerequisites) firmware installed.
As a last resort disable HDR tone mapping in the transcode settings.

We can also verify that hardware transcoding is taking place by running

```shell
sudo intel_gpu_top
```

on the host machine to see if the Intel GPU is being utilised.

### Jellyfin

Jellyfin is completely free, and therefore require no payment to enable hardware transcoding.

A Jellyfin deployment is pretty similar to a Plex one,
only changing the [image](https://github.com/jellyfin/jellyfin/pkgs/container/jellyfin) and corresponding names/labels.
A complete deployment of Jellyfin can be found in this article's GitLab
repository [here](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/05/intel-quick-sync-k8s/resources/jellyfin).

Arguably, Jellyfin has better support for transcoding than Plex
— Jellyfin at least has a
lot [more levers](https://jellyfin.org/docs/general/administration/hardware-acceleration/intel) you can pull.
To know which levers you safely can pull,
Wikipedia has an
informative [table](https://en.wikipedia.org/wiki/Intel_Quick_Sync_Video#Hardware_decoding_and_encoding)
detailing which formats a given CPU generation supports.
E.g. the [Alder Lake](https://en.wikipedia.org/wiki/Alder_Lake) (12th gen) i3-N305 I'm running on doesn't support
[AV1](https://aomediacodec.github.io/av1-spec/av1-spec.pdf) encoding,
but does support [HEVC](https://www.itu.int/rec/T-REC-H.265) encoding.
This means playback crashes if I try to allow encoding in AV1 format,
but allowing encoding in HEVC format works.

Trying to stream a 4K HDR HEVC encoded video works flawlessly without the need for any transcoding.
To get Jellyfin to transcode we can try [downsampling](https://en.wikipedia.org/wiki/Downsampling_(signal_processing))
the video again

![Jellyfin transcode](images/jellyfin-transcode.webp "Jellyfin showing transcoding taking place")

Checking the playback info in Jellyfin,
we unfortunately don't see any explicit indication of hardware transcoding taking place,
though running `intel_gpu_top` we see that it in fact is.

```
intel-gpu-top: Intel Alderlake_n (Gen12) @ /dev/dri/card1 -  213/ 909 MHz
         ENGINES     BUSY                                 MI_SEMA MI_WAIT
       Render/3D   55.49% |████████████████▏            |      0%      0%
         Blitter    0.00% |                             |      0%      0%
           Video   99.19% |████████████████████████████▉|      0%      0%
    VideoEnhance    0.00% |                             |      0%      0%
   PID              NAME  Render/3D    Blitter      Video    VideoEnhance 
 46994            ffmpeg |█████▉    ||          ||██████████||          |
```

## Manually mapping the GPU

An imho less elegant way of getting hardware transcoding to work is mounting the iGPU device directly into the
container as show on line 19–20 and 22–24.

{{< highlight yaml "linenos=table, hl_lines=17 19-20 22-24">}}
{{< readfile-rel file="resources/plex-privileged-mount.yaml" >}}
{{< /highlight >}}

Mapping the iGPU device into the container will make it appear as a transcoding device,
but you still need the required privileges to actually use it.
If you're a brute that don't need no security you can simply change
the [Security Context](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/) of the container to
privileged (line 17).
This can potentially give processes in the container root access to the host machine (this is bad!),
but at least you'll be able to play your hardware accelerated videos while you scoff about those silly hackers!

I tried removing the privilege and instead adding the iGPU groups (line 14, below) as reported by the host by running

```shell
ls -ln /dev/dri
```

which shows

```shell
crw-rw---- 1 0  44 226,   0 May 29 17:27 card0
crw-rw---- 1 0  44 226,   1 May 29 17:27 card1
crw-rw---- 1 0 104 226, 128 May 29 17:27 renderD128
```

{{< highlight yaml "linenos=table, hl_lines=14">}}
{{< readfile-rel file="resources/plex-supplemental-groups.yaml" >}}
{{< /highlight >}}

I unfortunately couldn't get this to work.

Though I wouldn't recommend mounting the GPU directly,
I'd love to hear from you if you've gotten this to work without giving the container root privileges.

## Hardening

Running containers that you might potentially expose to the internet is risky business.
You should therefore only run them with the minimum privileges they require to function.

Taking inspiration from [onedr0p's](https://github.com/onedr0p) collection
of [container images](https://github.com/onedr0p/containers) and
his [home-ops](https://github.com/onedr0p/home-ops/blob/aaef230c9d2a8a4e3066d9f5e8a7a8c3489a2cf4/kubernetes/main/apps/default/plex/app/helmrelease.yaml)
Plex configuration
— as well as getting some help from the man himself over at
the [Home Operations {{< icon "discord" >}} Discord](https://discord.gg/home-operations),
I was able to craft the following hardened deployment for Plex

{{< highlight yaml "linenos=table, hl_lines=23 13-14 16 18 20 25 27 28 34 38-39">}}
{{< readfile-rel file="resources/plex-hardened.yaml" >}}
{{< /highlight >}}

Here we're using onedr0p's Plex image (line 22) which
[by default](https://github.com/onedr0p/containers/blob/5a258efdca300d916efb6fb4b9a88e076eca346e/apps/plex/Dockerfile#L65)
runs as user `65534 (nobody)` with group `65534 (nogroup)`.
We've copied this to the security context on line 14-16,
but the id could in principle be any positive number.
We also make sure the pod is not running as `0 (root)` by adding line 16.

On line 18 we add groups `44 (video)` and `104 (render)` found by running

```shell
ls -ln /dev/dri
```

to check which groups the Intel GPU device belongs to.

As a sidenote, I was able to hardware transcode using only `104 (render)` as a supplemental group,
but I don't see much harm in including `44 (video)` in case it's needed for something I didn't test for.

To further harden the pod we can enforce the
RuntimeDefault [secure computing mode](https://kubernetes.io/docs/tutorials/security/seccomp/#create-a-pod-that-uses-the-container-runtime-default-seccomp-profile)
as done on line 20.

Next we move on to harden the Plex container itself by explicitly disallowing privilege escalation (line 25) and
dropping all Linux [capabilities](https://man7.org/linux/man-pages/man7/capabilities.7.html) on line 27.

We also set the root filesystem to read only on line 28.
Due to a read only root filesystem and the default transcode directory being `/transcode`,
we have to create an [`emptyDir` volume](https://kubernetes.io/docs/concepts/storage/volumes/#emptydir) (line 38-39) and
mount it under `/transcode` (line 34) to avoid playback errors with the transcoder failing to start due to a missing
write permission.

For potential further hardening you can take a look
at [AppArmor profiles](https://kubernetes.io/docs/tutorials/security/apparmor/)
and [SELinux labels](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/#assign-selinux-labels-to-a-container)
as well.

Similar hardening should also work with Jellyfin.

## Summary

Before configuring your Kubernetes cluster you have to prepare the host kernel for GPU transcoding to work.
A Linux kernel version 6.2 or newer is required for newer Intel CPUs.
For Debian based systems,
you can run the following to enable non-free sources with the required packages and install them

```shell
sudo sed -i '/Components/s/$/ non-free non-free-firmware/' /etc/apt/sources.list.d/debian.sources
sudo apt update
sudo apt install -y firmware-misc-nonfree intel-media-va-driver-non-free intel-gpu-tools
```

For configuring Kubernetes all the required resources can be found
in [this GitLab repo](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/05/intel-quick-sync-k8s/resources).
The configuration are written for use
by [Argo CD with Kustomize + Helm]({{< ref "/articles/2023/09/argocd-kustomize-with-helm" >}}),
but should be easily adaptable for other approaches using e.g. [Flux CD](https://fluxcd.io/).

```shell
❯ tree
.
├── cert-manager
│   ├── kustomization.yaml
│   └── ns.yaml
├── intel-device-plugins
│   ├── gpu
│   │   ├── kustomization.yaml
│   │   └── values.yaml
│   ├── kustomization.yaml
│   ├── ns.yaml
│   └── operator
│       └── kustomization.yaml
├── jellyfin
│   ├── deployment.yaml
│   ├── ingress.yaml
│   ├── kustomization.yaml
│   ├── ns.yaml
│   ├── pv-jellyfin-config.yaml
│   ├── pvc-jellyfin-config.yaml
│   └── svc.yaml
├── nfd
│   ├── kustomization.yaml
│   └── nfd-worker.conf
├── plex
│   ├── deployment.yaml
│   ├── ingress.yaml
│   ├── kustomization.yaml
│   ├── ns.yaml
│   └── svc.yaml
├── plex-hardened.yaml
├── plex-minimal-gpu.yaml
├── plex-privileged.yaml
└── plex-supplemental-groups.yaml
```

### Node Feature Discovery

{{< highlight yaml >}}
{{< readfile-rel file="resources/nfd/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/nfd/nfd-worker.conf" >}}
{{< /highlight >}}

### Cert-manager

{{< highlight yaml >}}
{{< readfile-rel file="resources/cert-manager/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cert-manager/ns.yaml" >}}
{{< /highlight >}}

### Intel Device Plugins

{{< highlight yaml >}}
{{< readfile-rel file="resources/intel-device-plugins/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/intel-device-plugins/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/intel-device-plugins/operator/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/intel-device-plugins/gpu/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/intel-device-plugins/gpu/values.yaml" >}}
{{< /highlight >}}

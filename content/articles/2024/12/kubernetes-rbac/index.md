---
title: "Demystifying Kubernetes RBAC and OIDC Auth"
date: 2024-12-05T21:00:00+01:00
lastmod: 2024-12-07T22:40:00+01:00

categories:
  - posts
  - kubernetes

showAuthor: true
authors:
  - "olav-st"

tags:
  - k8s
  - kubernetes
  - homelab
  - rbac
  - service account
  - role
  - role binding
  - cluster role
  - cluster role binding
  - auth
  - authentication
  - oidc
  - authelia
---

Kubernetes RBAC configuration can seem like a daunting task at first.
In this article we will try to demystify some of the mechanism behind the authorisation process in Kubernetes and learn
how to generate our own credentials to communicate with the Kubernetes API server.

We'll also take a look at why you generally shouldn't use the kubeconfig you initially get after creating a cluster,
and how to configure OIDC authentication with the Kubernetes API server for use with `kubectl`.

Although the topic of Kubernetes authorisation is covered in-depth in
the [official documentation](https://kubernetes.io/docs/reference/access-authn-authz/)
— [and](https://collabnix.github.io/kubelabs/RBAC101/)
[multiple](https://www.strongdm.com/blog/kubernetes-rbac-role-based-access-control)
[articles](https://www.aquasec.com/blog/kubernetes-authorization/)
[and](https://www.redhat.com/en/blog/kubeconfig)
[videos](https://youtu.be/JkvqhlpTXFY)
[already](https://youtu.be/PSDVanXZ0a4)
[exist](https://www.youtube.com/shorts/TFLUzwWB-W4)
[on](https://youtu.be/ECTxTONWgw8)
[the](https://youtu.be/iE9Qb8dHqWI)
[topic](https://youtu.be/jvhKOAyD8S8),
I've written this summary article as a way of better understanding Kubernetes,
and hopefully helping others in doing the same.

## RBAC Authorisation

Authorization in Kubernetes can be done
through [role-based access control](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) (RBAC).
Kubernetes also
supports [attribute-based access control](https://kubernetes.io/docs/reference/access-authn-authz/abac/) (ABAC),
and there exists a special-purpose [node authorisation](https://kubernetes.io/docs/reference/access-authn-authz/node/)
mode to authorise API requests made by kubelets.
In this article we'll only focus on RBAC authorisation.

For cluster-wide authorisation we can use non-namespaced
_[ClusterRole](https://kubernetes.io/docs/reference/kubernetes-api/authorization-resources/cluster-role-v1/)_ resource
— which defines
_[PolicyRules](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#policyrule-v1-rbac-authorization-k8s-io)_
for what the role allows,
and
_[ClusterRoleBindings](https://kubernetes.io/docs/reference/kubernetes-api/authorization-resources/cluster-role-binding-v1/)_
— which links _ClusterRoles_ to
_[Subjects](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.31/#subject-v1-rbac-authorization-k8s-io)_
(users, groups, or service accounts),
granting them access to specified Kubernetes resources.

More fine-grained authorisation can be obtained by using the namespaced
_[Role](https://kubernetes.io/docs/reference/kubernetes-api/authorization-resources/role-v1/)_ and
_[RoleBinding](https://kubernetes.io/docs/reference/kubernetes-api/authorization-resources/role-binding-v1/)_ resources.
Note that you can bind a _ClusterRole_ to a _RoleBinding_ to limit the scope of the _ClusterRole_ to the namespace the
_RoleBinding_ exists in.

{{< mermaid-init >}}

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/rbac.mmd" >}}
{{< /mermaid-chart >}}

Kubernetes ships with
some [default _ClusterRoles_](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#user-facing-roles).
These include the super-user `cluster-admin` _ClusterRole_,
and the aptly named `admin`, `edit` and `view` _ClusterRoles_ intended to be granted within namespaces using
_RoleBindings_.

To have a request authorised,
you have to present either a valid token or certificate recognised as a _subject_
by a _RoleBinding_, which in turn references a _Role_ describing what is allowed.
There is also a special,
[hard-coded `system:master`
_Group_](https://kubernetes.io/docs/reference/access-authn-authz/authorization/#the-system-masters-group)
that allows full access to the cluster without any associated _ClusterRole_ or _ClusterRoleBinding_.
This group is meant for break-glass access to a cluster,
and should generally be avoided in favour
of [least-privilege access](https://kubernetes.io/docs/concepts/security/rbac-good-practices/#least-privilege).

{{< dynamic-image title="Kubernetes RBAC resources relationship. The RoleBindings connects subjects with Roles."
alt="Kubernetes RBAC resources relationship" dark="images/roles-dark.svg" light="images/roles-light.svg" >}}

## Kubeconfig

As a primer before we continue,
we'll quickly go through the [kubeconfig](https://www.redhat.com/en/blog/kubeconfig)-file used by `kubectl` to
store [access details to clusters](https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/).

Kubernetes follows the [X.509](https://en.wikipedia.org/wiki/X.509) standard for certificates,
and [uses](https://kubernetes.io/docs/setup/best-practices/certificates/)
a [public key infrastructure](https://en.wikipedia.org/wiki/Public_key_infrastructure) (PKI) for certificate
distribution.

Given the sample kubeconfig file below,
the _certificate-authority_ field (line 8) contains a base64 encoded certificate meant to authenticate the
API server we're trying to communicate with.
This helps [mitigate man-in-the-middle attacks](https://en.wikipedia.org/wiki/Man-in-the-middle_attack),
making it harder to spoof the API server.

{{< highlight yaml "linenos=table, hl_lines=8 13 14 16" >}}
{{< readfile-rel file="resources/kubeconfig/kubeconfig.yaml" >}}
{{< /highlight >}}

For authorising with the Kubernetes API server we can present a _client-certificate_ (line 13) signed by the
Kubernetes [Certificate Authority](https://en.wikipedia.org/wiki/Certificate_authority) (CA) and our private
_client-key_ (line 14) used to generate
the [certificate signing request](https://en.wikipedia.org/wiki/Certificate_signing_request).
It's also possible to use a token instead of a certificate, something we'll look into later.

The context field (line 16) holds configuration used to communicate with the API server,
i.e. a combination of user, cluster and optionally a namespace.

Whereas the CA and client certificates can be considered public,
the client key should never be shared unless you want everyone to have access to your cluster.
In lack of better judgement,
I've added certificates and client keys for defunct clusters in the [Summary](#summary) section as examples.

Although [certificate revocation](https://en.wikipedia.org/wiki/Certificate_revocation) is a well established part of
PKI, [Kubernetes does not (yet) support this feature](https://github.com/kubernetes/kubernetes/issues/18982).
This means that any certificate signed by the Kubernetes CA is valid throughout its whole lifetime.

It's possible to [change the whole certificate chain](https://github.com/kubernetes/website/issues/30575) instead,
but I can only imagine it's an arduous task filled with peril... and possibly a dragon or maybe a balrog.
Kubeadm does have support
for [managing certificates](https://kubernetes.io/docs/tasks/administer-cluster/kubeadm/kubeadm-certs/),
and there exist documentation on how
to [manually rotate the Kubernetes CA certificates](https://kubernetes.io/docs/tasks/tls/manual-rotation-of-ca-certificates/),
so it is possible.

## Out-of-the-box Authorisation

Before we investigate how we can generate our own means of authorisation,
let's first look at what we get _out-of-the-box_ from some common Kubernetes distributions.

The easiest starting point is [Minikube](https://minikube.sigs.k8s.io/), which is meant for local use.
Looking at the _kubeconfig_ generated by spinning up a Minikube-cluster,
we see it referencing files for the _certificate-authority_, _client-certificate_ and _client-key_ fields.

Following the [RSA algorithm](https://en.wikipedia.org/wiki/RSA_(cryptosystem)),
the _client-key_ is just a collection of integers,
including some really big ones that have special properties and are hard to both guess and compute
— barring collapsing [wave functions](https://en.wikipedia.org/wiki/Wave_function) in
future [quantum computers](https://www.csoonline.com/article/3562701/chinese-researchers-break-rsa-encryption-with-a-quantum-computer.html),
so we won't pay it much mind.

If you're interested in the RSA-key details you can check them using `openssl` by executing

```shell
openssl rsa -text -noout -in <key-file>
```

The more interesting part for our case are the certificates.
These can be inspected by running

```shell
openssl x509 -text -noout -in <certificate-file>
```

Looking at the CA certificate, we see that it is self-signed
— the _Issuer_ and _Subject_ fields reference the same Common Name (CN).

```shell
Certificate:
  Data:
    Issuer: CN=minikubeCA
    Subject: CN=minikubeCA
    X509v3 extensions:
      X509v3 Basic Constraints: critical
        CA:TRUE
      X509v3 Subject Key Identifier: 
        61:34:D4:BB:7A:50:5C:59:BE:B7:12:AF:EB:86:6F:49:E5:7B:3C:02
```

Also note the _Subject Key Identifier_ value.

Inspecting the client certificate reveals the CN of the certificate _Issuer_ to be `minikubeCA`,
we can also recognise the _Subject Key Identifier_ from the CA certificate as the _Authority Key Identifier_,
linking the two certificates.
More interestingly, the subject Organisation (O) is the special `system:masters` group.

```shell
Certificate:
  Data:
    Issuer: CN=minikubeCA
    Subject: O=system:masters, CN=minikube-user
      X509v3 extensions:
        X509v3 Basic Constraints: critical
        CA:FALSE
      X509v3 Authority Key Identifier: 
        61:34:D4:BB:7A:50:5C:59:BE:B7:12:AF:EB:86:6F:49:E5:7B:3C:02
```

Directly referencing the special `system:masters` group means that there is no way to rescind authorisation for this
certificate since we can't remove the group, nor revoke the certificate itself.

[Kubeadm](https://kubernetes.io/docs/reference/setup-tools/kubeadm/) addressed the `system:masters` issue
in
version [1.28](https://github.com/kubernetes/kubernetes/blob/master/CHANGELOG/CHANGELOG-1.29.md#changelog-since-v1280)
onwards.
Kubeadm now issues two kubeconfig-files upon cluster bootstrapping,
a `super-admin.conf` file which is supposed to be kept for emergency purposes only
— since it uses the `system:masters` group,

```shell
Certificate:
  Data:
    Issuer: CN=kubernetes
    Subject: O=system:masters, CN=kubernetes-super-admin
```

and another kubeconfig file — `admin.conf`,

```shell
Certificate:
  Data:
    Issuer: CN=kubernetes
    Subject: O=kubeadm:cluster-admins, CN=kubernetes-admin
```

which instead references the _ClusterRoleBinding_
`kubeadm:cluster-admins`

We can inspect the `kubeadm:cluster-admins` CRB to see that it in turn references the default `cluster-admin`
_ClusterRole_ which gives full access to the cluster

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/kubeadm/kubeadm-cluster-admins.yaml" >}}
{{< /highlight >}}

The difference between the _kubeconfig_-files is that if we suspect the `admin.conf` file to be compromised,
we can delete the `kubeadm:cluster-admins` CRB,
and the associated certificate loses its authorisation.
Executing this
proverbial [footgun](https://en.wiktionary.org/wiki/footgun#:~:text=footgun%20(plural%20footguns),shooting%20themself%20in%20the%20foot.)
using the `admin.conf` credentials,
we see that we lose access to the cluster

```shell
> kubectl delete clusterrolebinding kubeadm:cluster-admins
clusterrolebinding.rbac.authorization.k8s.io "kubeadm:cluster-admins" deleted
> kubectl get nodes
Error from server (Forbidden): nodes is forbidden: User "kubernetes-admin" cannot list resource "nodes" in API group "" at the cluster scope
```

Take note that if you switch to the `super-admin.conf` kubeconfig and re-create the CRB we just deleted,
the `admin.conf` kubeconfig regains its privileges.
Avoid restoring this CRB if you suspect that the `admin.conf` kubeconfig is compromised!
Instead, conjure up a new _Group_ subject and issue a new certificate as in the next section.

Out of the other Kubernetes distros I
tried, [Talos](https://www.talos.dev/), [AKS](https://azure.microsoft.com/en-us/products/kubernetes-service),
and [K3s](https://k3s.io/) all generated a `system:masters` level access certificate,
except [GKE](https://cloud.google.com/kubernetes-engine) which uses
a [plugin](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-access-for-kubectl#install_plugin)
based on
the [client-go credential plugins](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#client-go-credential-plugins)
framework for authorisation.

## Custom Authorisation

For custom authorisation in Kubernetes, we can either take advantage of one of the default _ClusterRoles_
— `cluster-admin`, `admin`, `edit`, `view`, or we can craft our own by stitching
together [appropriate rules](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#role-examples).

It's also possible to
create [aggregated cluster roles](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#aggregated-clusterroles).
Instead of directly defining _policyRules_,
aggregated cluster roles defines an aggregationRule which combines the _policyRules_ of _ClusterRoles_ which matches the
given criteria,
this includes other aggregated cluster roles.
As an example you can inspect the default `admin`, `edit`, and `view` _ClusterRoles_.

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/aggregated-cluster-roles.mmd" >}}
{{< /mermaid-chart >}}

The aggregation mechanism comes in handy when you
create [custom resources](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) where
you might want cluster users to be able to view or create the resources.
Assuming you're already using the built-in roles for cluster users,
you can simply define a new _ClusterRole_ with the appropriate label which will then be aggregated to an applicable
default _ClusterRole_.

As an example of creating and using our own _ClusterRole_,
we will copy the details of the built-in `cluster-admin`
— since it's fairly straight-forward,
_ClusterRole_ and name it `super-admin` (line 4) to distinguish it

{{< highlight yaml "linenos=table, hl_lines=4" >}}
{{< readfile-rel file="resources/super-admin/role/cluster-role.yaml" >}}
{{< /highlight >}}

To actually use the _ClusterRole_ we just created,
we need to create a _ClusterRoleBinding_ to link this role to a user, group, or service account.

Kubernetes assumes that a cluster-independent
service [manages normal users](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#users-in-kubernetes),
and thus Kubernetes does not have any objects that represents normal user accounts.

Without any third party tool managing users,
that means we're free to make up users on a whim,
which is what we'll do here.

{{< highlight yaml "linenos=table, hl_lines=6-7 9-10 12-13 16-17" >}}
{{< readfile-rel file="resources/super-admin/role/cluster-role-binding.yaml" >}}
{{< /highlight >}}

The above CRB grants any _subject_ recognised as belonging to the group `super-admin` (line 6-7),
the specified user `lain` (line 9-10), and the `sa-super-admin`
_[ServiceAccount](https://kubernetes.io/docs/concepts/security/service-accounts/)_ in namespace `cluster-access`
(line 12-13) the `super-admin` role (line 17-18) we created earlier.

## Generating Certificates

Following PKI, we need to first create a private key before generating
a [CertificateSigningRequest](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/)
(CSR).
Diverging from the more common RSA cryptographic scheme,
we can instead try out [Elliptic-curve cryptography](https://en.wikipedia.org/wiki/Elliptic-curve_cryptography) (ECC)
which offers equivalent security with smaller keys compared to RSA,
thus arguably being more efficient.
We can generate a [secp521r1](https://neuromancer.sk/std/secg/secp521r1) curve[^1] EC-key using [
`openssl`](https://docs.openssl.org/) by running

```shell
openssl ecparam -genkey -name secp521r1 -out private.key
```

[^1]: I think this curve is overkill, and I'm open to suggestions on better cryptographic curves!

To conjure up a user called `alice` belonging to the`super-admin` group we can create
a [certificate signing request](https://en.wikipedia.org/wiki/Certificate_signing_request)
signed by our freshly generated key
— with organisation (O) `super-admin` and common name (CN) `alice`,
by invoking

```shell
openssl req -new -key private.key -subj "/O=super-admin/CN=alice" -out alice.csr
```

Next we create a
_[CertificateSigningRequest](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#normal-user)_
(CSR) resource with the `alice.csr` certificate signing request base64 encoded (line 6) to ask
a [Kubernetes signer](https://kubernetes.io/docs/reference/access-authn-authz/certificate-signing-requests/#kubernetes-signers)
(line 7) to sign the certificate for us

{{< highlight yaml "linenos=table, hl_lines=6 7 9" >}}
{{< readfile-rel file="resources/super-admin/subjects/alice-certificate-signing-request.yaml" >}}
{{< /highlight >}}

We also have to indicate that the certificate is to be used for authentication by adding `client auth` in the _usages_
list (line 9).

After applying the above CSR to a cluster,
we can see it pending approval

```shell
> kubectl get csr
NAME        AGE   SIGNERNAME                            REQUESTOR   REQUESTEDDURATION   CONDITION
alice-csr   26s   kubernetes.io/kube-apiserver-client   admin       <none>              Pending
```

Assuming we have the right privileges we can approve the request by running

```shell
kubectl certificate approve alice-csr
```

If everything went well,
the signed certificate should now be in the satus field of the CSR resource.
We can extract the signed certificate from the status field to `alice.crt` file by running

```shell
kubectl get csr alice-csr -o yaml | yq '.status.certificate' | base64 -d > alice.crt
```

Note the validity of the certificate

```shell
> openssl x509 -text -noout -in alice.crt
Certificate:
  Data:
    Validity
      Not Before: Nov 30 19:01:08 2024 GMT
      Not After : Nov 30 19:01:08 2025 GMT
```

Be sure to issue a new certificate before it expires if you wish to keep the access.
If you want to revoke Alice's access you have to delete either the _ClusterRole_ or _ClusterRoleBinding_ to rescind the
authorisation vested in the certificate.
Deleting the CR or CRB will naturally also revoke access to anyone else relying on them,
so it might be a good idea to create a CRB for each cluster user.

With our `private.key` private key and `alice.crt` signed certificate in hand we can add them as
credentials to our kubeconfig file by executing

```shell
kubectl config set-credentials alice --client-key=private.key --client-certificate=alice.crt --embed-certs=true
```

Next, create a new context that combines the newly created user credentials with the current cluster
(you can find the cluster name by running `kubectl config get-clusters`)

```shell
kubectl config set-context alice --cluster=kubernetes --user=alice
```

To change to the new context run

```shell
kubectl config use-context alice
```

and verify that everything works by invoking

```shell
kubectl get pods -A
```

This last command should give an overview of all the pods running in the cluster if the certificate is valid and the
private key is correct.

A simplified graph of the authorisation flow we just performed is summarised in the figure below

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/auth.mmd" >}}
{{< /mermaid-chart >}}

Creating a user certificate for the omnipotent [Lain](https://www.cjas.org/~leng/lain.htm) is left as an exercise to the
reader.

## ServiceAccounts

Every namespace in Kubernetes contains a _ServiceAccount_
named [default](https://kubernetes.io/docs/concepts/security/service-accounts/#default-service-accounts).
This `default` _ServiceAccount_ is only granted
the [default API discovery permission](https://kubernetes.io/docs/reference/access-authn-authz/rbac/#discovery-roles)
which all authenticated users get.

If we don't [manually assign a _ServiceAccount_ to a
_Pod_](https://kubernetes.io/docs/concepts/security/service-accounts/#assign-to-pod),
that pod will get be assigned this `default` _ServiceAccount_.

A _ServiceAccount_ can be used by Pods to communicate with the Kubernetes API server.
Tools like [Argo CD](https://argo-cd.readthedocs.io/), [Cilium](https://cilium.io/)
and [Cert manager](https://cert-manager.io/) extensively use the concepts in this article to communicate with the API
server.
If you run similar tools in your cluster I urge you to explore the _ClusterRoles_, _ClusterRoleBindings_, and
_ServiceAccounts_ these tools create when installed.

## ServiceAccount Tokens

We can [manually retrieve
_ServiceAccount_ tokens](https://kubernetes.io/docs/concepts/security/service-accounts/#get-a-token) by either looking
in `/var/run/secrets/kubernetes.io/serviceaccount` inside a Pod,
or by using
the [TokenRequest](https://kubernetes.io/docs/reference/kubernetes-api/authentication-resources/token-request-v1/) API.

_[ServiceAccounts](https://kubernetes.io/docs/concepts/security/service-accounts/)_ are primarily intended to be used by
applications and services
— i.e. non-human entities,
although the documentation for
the [Kubernetes Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard)
links to
a [guide to create a sample user](https://github.com/kubernetes/dashboard/blob/master/docs/user/access-control/creating-sample-user.md)
where they use a _ServiceAccount_,
so we'll allow ourselves to do the same for demonstrative purposes.

With the `super-admin` _ClusterRoleBinding_ we created earlier,
we can create the following _ServiceAccount_ to bind to it (line 4)

{{< highlight yaml "linenos=table, hl_lines=4" >}}
{{< readfile-rel file="resources/super-admin/subjects/service-account.yaml" >}}
{{< /highlight >}}

To authenticate using the service account we can create a base 64
encoded [JSON Web Token](https://en.wikipedia.org/wiki/JSON_Web_Token) (JWT) by running

```shell
kubectl -n cluster-access create token sa-super-admin
```

We can inspect the generated JWT by pasting it into e.g. [https://jwt.io](https://jwt.io/)

```json
{
  "exp": 1733053970,
  "iat": 1733050370,
  "sub": "system:serviceaccount:cluster-access:sa-super-admin",
  ...
}
```

here we see that the token is valid for 3 600 seconds (`exp - iat`), or an hour.

The JWT can be used in a similar fashion as a certificate by adding it as a credential to a kubeconfig context

```shell
TOKEN=$(kubectl -n cluster-access create token sa-super-admin)
kubectl config set-credentials sa-super-admin --token=$TOKEN
kubectl config set-context sa-super-admin --cluster kubernetes --user sa-super-admin
kubectl config use-context sa-super-admin
```

If this is your only way of authenticating wth the cluster you'll have to refresh the token every hour before it
expires, or else you lose access.

## OpenID Connect Authorisation

Rather than juggling long-lived certificates and abusing _ServiceAccounts_,
we can instead use [OpenID Connect](https://openid.net/developers/how-connect-works/) (OIDC) for a more user-friendly
way of authenticating to the Kubernetes API server.

Although OIDC authorisation is easier for the end-user,
it requires some extra configuration from the cluster admin.
We'll need to set up an OIDC provider as well as configure
the [kube-apiserver](https://kubernetes.io/docs/reference/command-line-tools-reference/kube-apiserver/)
to use said OIDC provider.

To avoid dealing with a client secret we'll use the
_[Proof Key for Code Exchange by OAuth Public Clients](https://datatracker.ietf.org/doc/html/rfc7636)_,
or PKCE for short, pronounced _pixy_.

It's possible to [authenticate with the API server using vanilla
`kubectl`](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#using-kubectl),
though using a plugin like [kubelogin](https://github.com/int128/kubelogin) makes it a lot easier,
and is what we'll be using here.

Assuming you already know the `client-id` and URL-base of your OIDC provider discovery URL
— e.g. `<URL>/.well-known/openid-configuration`,
you can configure the kubelogin plugin by adding the following to your kubeconfig file

{{< highlight yaml "linenos=table, hl_lines=10 11 12" >}}
{{< readfile-rel file="resources/oidc/kubectl.yaml" >}}
{{< /highlight >}}

Here `<URL>` on line 10 is the base of the discovery URL, and `kubectl` is the client ID (line 11).
We also ask for the `profile` and `groups` scope which we can use to match with CRB `subjects`.

The next step is to configure an OIDC capable identity provider.
There's a plethora of self-hosted OIDC providers out there.
Some examples are [Authelia](https://www.authelia.com/), [Authentik](https://goauthentik.io/),
[Kanidm](https://kanidm.com/), [Keycloak](https://www.keycloak.org/), [Rauthy](https://sebadob.github.io/rauthy/),
and [Zitadel](https://zitadel.com/).
I'm currently running both Authelia (with [LLDAP](https://github.com/lldap/lldap) for user management) and
Keycloak (using the [Crossplane Keycloak-provider](https://github.com/crossplane-contrib/provider-keycloak) for
declarative setup),
though I'm contemplating giving Kanidm a go.
If you instead prefer an external service you can e.g. create
a [GitHub App](https://docs.github.com/en/apps/creating-github-apps/about-creating-github-apps/about-creating-github-apps),
or use [Google's OpenID Connect](https://developers.google.com/identity/openid-connect/openid-connect) solution.

I opted to use Authelia because of its inherent declarative config.
Authelia was a bit finicky to set up as an OIDC provider at first
— I'm working on an article on how to configure it,
but once it's working I added the following configuration

{{< highlight yaml>}}
{{< readfile-rel file="resources/oidc/authelia.yaml" >}}
{{< /highlight >}}

To [enable OIDC integration](https://kubernetes.io/docs/reference/access-authn-authz/authentication/#openid-connect-tokens)
in Kubernetes we need to pass the relevant arguments to kube-apiserver.

Using Minikube this can be done by starting it with the following parameters

{{< highlight bash "linenos=table, hl_lines=2 3 4 6" >}}
{{< readfile-rel file="resources/oidc/minikube.sh" >}}
{{< /highlight >}}

Notice the OIDC-provider base `<URL>` on line 2 and `client-id` on line 3.
We've also explicitly configured the _oidc-username-claim_ (line 4) to be the JWT `sub`-claim.
Other options for the username claim depends on your OIDC provider setup,
some options can be either `email`, `name`, or `preferred_username`.
The username-claim will be used to match `User`-kind subject in CRBs.
To match `Group`-kind subjects we selected the aptly named `groups`-claim to select from (line 6).

The equivalent in Talos' machineconfig would be

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/oidc/talos-machineconfig.yaml" >}}
{{< /highlight >}}

With Minikube started,
the `kubectl` context should be automatically change to the `system:masters` context for the cluster.
Using this context we can create the following CRB to match appropriate OIDC-claims

{{< highlight yaml "linenos=table hl_lines 7 10 14" >}}
{{< readfile-rel file="resources/oidc/oidc-cluster-admin-role-binding.yaml" >}}
{{< /highlight >}}

Here we've chosen to match the `k8s:cluster-admin` (line 7) group from the `groups`-claim and the specific user with the
`sub`-claim `lain` (line 10).
To make it easy for ourselves we've used the built-in `cluster-admin` CR to grant full access to the cluster for
subjects matching the given rules.

We can now create a new `kubectl` context using the oidc-user we manually created earlier and start using that context

```shell
kubectl config set-context oidc --cluster minikube --user oidc
kubectl config use-context oidc
```

When running a `kubectl` command that requires authentication,
your web browser should pop up and greet you with the
login page of your selected OIDC provider.
After successfully logging in you should be greeted with a consent screen to hand over your details

![Authelia consent](images/authelia-login.png "Authelia consent screen")

Consenting to sharing your details, a Kubelogin view from localhost should appear informing you that you've successfully
logged in to the cluster.

![Kubelogin](images/kubelogin.png "Kubelogin authenticated screen")

The JWT is cached in the `~/.kube/oidc-login/`-folder if you want to inspect it.
This token will be forwarded with each request to the API server.

## Summary

Note that it is generally a _very_ bad idea to share certificates and private keys as they can be used to wreak havoc.
These have been generated inside a virtual machine that has now been decommissioned,
so like a key to a burnt down house, they have no use except as examples.

I encourage you to inspect both certificates and keys using e.g. `openssl`.

### Minikube

Public CA certificate
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/minikube/ca.crt" >}}
{{< /highlight >}}

Client certificate signed by CA
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/minikube/client.crt" >}}
{{< /highlight >}}

Private key
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/minikube/client.key" >}}
{{< /highlight >}}

### K3s

Public CA certificate
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/k3s/ca.crt" >}}
{{< /highlight >}}

Client certificate signed by CA
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/k3s/client.crt" >}}
{{< /highlight >}}

Private key
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/k3s/client.key" >}}
{{< /highlight >}}

### Kubeadm

Public CA certificate
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/kubeadm/ca.crt" >}}
{{< /highlight >}}

Super admin certificate signed by CA
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/kubeadm/super-admin.crt" >}}
{{< /highlight >}}

Private key for super-admin certificate
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/kubeadm/super-admin.key" >}}
{{< /highlight >}}

Admin certificate signed by CA
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/kubeadm/admin.crt" >}}
{{< /highlight >}}

Private key for admin certificate
{{< highlight text >}}
{{< readfile-rel file="resources/kubeconfig/kubeadm/admin.key" >}}
{{< /highlight >}}

`kubeadm:cluster-admins` ClusterRoleBinding
{{< highlight yaml >}}
{{< readfile-rel file="resources/kubeadm/kubeadm-cluster-admins.yaml" >}}
{{< /highlight >}}

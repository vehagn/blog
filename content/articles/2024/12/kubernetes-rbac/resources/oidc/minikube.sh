minikube start \
  --extra-config=apiserver.oidc-issuer-url=<URL> \
  --extra-config=apiserver.oidc-client-id=kubectl \
  --extra-config=apiserver.oidc-username-claim=sub \
  --extra-config=apiserver.oidc-username-prefix="oidc:" \
  --extra-config=apiserver.oidc-groups-claim=groups

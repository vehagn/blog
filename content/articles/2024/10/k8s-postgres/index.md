---
title: "Postgres databases in Kubernetes"
date: 2024-10-27T21:45:00+01:00
published: 2024-10-27T21:45:00+01:00
#lastmod: 2024-07-10T12:45:00+02:00

categories:
  - posts
  - kubernetes

tags:
  - k8s
  - kubernetes
  - homelab
  - helm
  - csi
  - database
  - postgres
  - postgresql
  - cnpg
  - cloudnativepg
---

Traditionally, an ideal Kubernetes workload should be stateless,
meaning it should be able to run anywhere and scale freely,
though this also severely limits which kinds of applications we can run, i.e. stateful applications.

In this article we'll take a look at how we can solve the _run anywhere_ and scalability challenges with stateful
workloads in Kubernetes.
We'll first create a rudimentary Postgres database workload to get a grasp of the basics,
before we take a look at Helm charts to ease some of the hassle of deploying a database.
Lastly we'll take a look at database operators and how they might make things even easier.
At the end we'll deploy the [CloudNative PG](https://cloudnative-pg.io/) operator to automate everything for us.

I'm not a database admin, so if you are
— or at least know more than me about databases,
I'd be happy to get your feedback and corrections!

## Why a standalone database?

Databases are used to store data in an organised and consistent manner,
making the data easy to access and manipulate.
Many applications intended for self-hosting opt for an embedded database like [SQLite](https://www.sqlite.org/),
though they often support external databases.

Assuming we decide to use an application's embedded database we now have a stateful Kubernetes workload,
thus running into both the node affinity and scalability issues previously mentioned.

To alleviate the affinity limitation for stateful applications we can use distributed storage solutions
like [Ceph](https://ceph.io/en/) or [Longhorn](https://longhorn.io/),
which prevents us from binding the workload to a single node.
Though, with distributed storage we have no guarantee that the application natively supports concurrent writes,
meaning we have to make sure only one instance of the application runs at any one time.
In Kubernetes, we can guarantee this by e.g. using
a [StatefulSet](https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/),
or the [Recreate](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#recreate-deployment) strategy
for a [Deployment](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#recreate-deployment),
making sure [replicas](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#replicas) is set to 1.

A possible solution to both the affinity and scalability challenges is using a database
like [PostgreSQL](https://www.postgresql.org/).
If done correctly we should be able to connect to the database from every node
— solving the node affinity issue,
and if we're lucky the database connection will also deal
with [concurrent writes](https://www.postgresql.org/docs/7.1/mvcc.html)
— solving the scalability issue.

Here we're effectively shifting the state from one workload
— the main application,
to another workload
— the database.
This helps with separation of concern,
and by creating a [database cluster](https://www.postgresql.org/docs/current/creating-cluster.html) we can potentially
more easily configure high availability of the application.
Backup of an external database if often also easier than with an embedded database,
and is made even easier if you rely on an operator
like [CloudNative PG](https://cloudnative-pg.io/documentation/1.24/backup/).
We can potentially also collect multiple applications under different schemas in the same database,
though this is [not a recommended approach](https://cloudnative-pg.io/documentation/1.24/faq/).

{{< mermaid-init >}}

## Databases in Kubernetes?

This article won't discuss the benefits and drawback of running databases in Kubernetes.
Instead, I'd like to guide you to a very informative article titled [_Recommended architectures for PostgreSQL in
Kubernetes_](https://www.cncf.io/blog/2023/09/29/recommended-architectures-for-postgresql-in-kubernetes/) on the
official [CNCF](https://www.cncf.io/) blog.

I've decided that running databases inside the cluster makes sense in my homelab,
though your mileage may vary.

## A Simple Database in Kubernetes

The easiest way to get started is by creating a StatefulSet using Docker's
official [Postgres image](https://github.com/docker-library/docs/blob/master/postgres/README.md)

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/simple/stateful-set.yaml" >}}
{{< /highlight >}}

This spins up a containerised PostgreSQL 17 database in a pod with no persistent storage,
meaning we will lose state when the pod is deleted.

If you're inclined you can connect to the database by port-forwarding the pod which can be done by running

```shell
kubectl -n postgres-example port-forward postgres-0 5432:5432
```

You should now be able to connect to the database at `localhost:5432/postgres` using `admin` as the username and
`supers3cret` as the password connecting to the `postgres` database.

To actually connect to the database from a different workload in the cluster we can create a
[Headless Service](https://kubernetes.io/docs/concepts/services-networking/service/#headless-services) which makes it
possible to address a specific pod directly on `<pod-name>.<service-name>.<namespace>.svc.cluster.local`

We can create a headless service setting `ClusterIP` to `None`, e.g.

{{< highlight yaml "linenos=table" >}}
{{< readfile-rel file="resources/simple/svc.yaml" >}}
{{< /highlight >}}

This allows other workloads inside the cluster to reach the database at
`postgres-0.postgres-service.postgres-simple.svc.cluster.local`.

Note that
the [StatefulSetSpec API reference](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/stateful-set-v1/#StatefulSetSpec)
mentions that the `serviceName` _Service_ must exist before the _StatefulSet_,
so you probably have to delete and recreate the database StatefulSet above for the pod to be reachable.

We're still not storing the state through pod deletion though,
for this we need to attach a persistent volume.

Assuming you're following my Kubernetes journey you might know that I'm currently
running [Talos on Proxmox]({{< ref "/articles/2024/08/talos-proxmox-tofu" >}}) and using
the [Proxmox CSI Plugin]({{< ref "/articles/2024/06/k8s-proxmox-csi" >}}) to provision storage,
though however you provision storage should be fine.

If you're just getting started and want to try things out,
a simple [hostPath](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath) mount might be enough, e.g. adding

```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: postgres
  namespace: postgres-example
spec:
  ...
  template:
    ...
    spec:
      containers:
          - name: postgres
            ...
            volumeMounts:
              - name: local
                mountPath: /var/lib/postgresql/data
      volumes:
        - name: local
          hostPath:
            path: <HOST_NODE_PATH>
```

to the StatefulSet above.
Though if you intend for a more permanent installation you should instead consider using
a [_local_](https://kubernetes.io/docs/concepts/storage/volumes/#local) persistent volume.

You can find a complete example using [Kustomize](https://kustomize.io/) to deploy a Postgres database connected to a
NFS volume in the [Summary](#kustomize) section.

## Leveraging Helm charts

Instead of manually manufacturing and maintaining manifests for our Postgres installation we can
use [Helm](https://helm.sh/) charts to do the heavy lifting.
A popular choice for this is the [Bitnami PostgreSQL chart](https://artifacthub.io/packages/helm/bitnami/postgresql),
and we can often find it bundled as an optional sub-chart to other Helm charts,
e.g. [Authelia](https://github.com/authelia/chartrepo/blob/eaecffff4b819b6fa3238f40e15bd72a63fc6fcf/charts/authelia/Chart.yaml#L26)
and [Immich](https://github.com/immich-app/immich-charts/blob/a59fd88f94a7b81b25e46304ff672b698bce10f0/charts/immich/Chart.yaml#L23).

The Bitnami PostgreSQL chart has a lot of buttons and levers,
including [LDAP](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol) integration,
an easy way to create multiple read only replicas,
and settings for taking backups.
Properly covering all the options is an article in itself,
so I'll allow myself to be brief by only spinning up a primary instance.
You can take a look at all the options in the
chart [default values.yaml](https://github.com/bitnami/charts/blob/main/bitnami/postgresql/values.yaml) file.

Before we spin up a database using the Bitnami PostgreSQL chart we have to decide how we want to store the state.
Assuming you already have a [_StorageClass_](https://kubernetes.io/docs/concepts/storage/storage-classes/) that can
automatically provision [_PersistentVolumes_](https://kubernetes.io/docs/concepts/storage/persistent-volumes/)
— e.g. one that uses [Proxmox CSI Plugin](https://github.com/sergelogvinov/proxmox-csi-plugin),
the easiest way to get started is to reference that in the chart `values.yaml` file, e.g.

```yaml
global:
  defaultStorageClass: "<STORE_CLASS_NAME>"
```

If you don't care about persistence you can instead disable it for the primary instance by adding

```yaml
primary:
  persistence:
    enabled: false
```

to the `values.yaml` file.

As a third option I've chosen to manually create
a [_PersistentVolumeClaim_](https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims)
using a [pre-defined
_StorageClass_](https://github.com/vehagn/homelab/blob/4c2d60a5fb1e5aadc1c96897d69aef54e809e3a2/k8s/infra/storage/proxmox-csi/values.yaml),
though you can also find an example of a local-storage PV and PVC in the [Summary](#bitnami-helm-chart) section.

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/pvc.yaml" >}}
{{< /highlight >}}

Applying the above PVC we can reference it the chart `values.yaml` file for the primary database instance by adding

```yaml
primary:
  persistence:
    existingClaim: postgres-data
```

Next we should configure authentication for our database.
A common pattern is to create an admin user for setting up the database using
e.g. [Flyway](https://github.com/flyway/flyway),
and a less privileged user for the application runtime.

To do this we can append

```yaml
auth:
  enablePostgresUser: true
  username: user
  existingSecret: auth
```

to the `values.yaml` file.
The secret containing the passwords for the `postgres` and `app` users can be created by running

```shell
kubectl create secret generic auth \
  --from-literal postgres-password=supers3cret \
  --from-literal password=s3cret \
  --namespace postgres-helm
```

using the default secret key names which is `postgres-password` and `password` for the admin and custom user
respectively.

If you even think about checking this secret into your version control system you should take a look at another Bitnami
project called [Sealed secrets](https://github.com/bitnami-labs/sealed-secrets) used encrypt secrets.

Having crafted a desired `values.yaml` configuration we can install the Helm chart by running

```shell
helm install postgresql \
  oci://registry-1.docker.io/bitnamicharts/postgresql \
  --values values.yaml \
  --namespace postgres-helm
```

which should greet us with a command we can use to connect to the database similar to

```shell
kubectl port-forward --namespace postgres-helm svc/postgresql 5432:5432 &
psql --host 127.0.0.1 --dbname postgres --pport 5432 --username postgresql
```

Note that the Helm chart creates both a headless _Service_ for internal communication and a regular _Service_ for
external connections which is what we connect to using the above port-forwarding.

The chart also creates a [_PodDisruptionBudget_](https://kubernetes.io/docs/tasks/run-application/configure-pdb/)
setting `maxUnavailable` primary database pods to `1`
— which doesn't really do much with our one replica,
and a [_NetworkPolicy_](https://kubernetes.io/docs/concepts/services-networking/network-policies/) allowing all egress
traffic and ingress traffic on port `5432` from any source.
A [_ServiceAccount_](https://kubernetes.io/docs/concepts/security/service-accounts/) is also created which I assume can
be given special privileges though the chart.

### Sub-charts

Configuring a standalone PostgreSQL database helps us explore the different options.
For other Helm charts bundling the Bitnami PostgreSQL chart we can often directly configure the installation under the
`postgresql` key in the main application chart, e.g.

```yaml
postgresql:
  global:
    defaultStorageClass: proxmox-csi
  primary:
    persistence:
      enabled: true
```

## CloudNative PG Operator

A step up from Helm charts in regard to automating the Kubernetes workload lifecycle is
using [operators](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/).
Operators extend the Kubernetes API with [_Custom Resource
Definitions_](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources/) to allow for new
functionality.

There are many database operators to chose from.
Inspired by [Jérôme Petazzoni](https://jpetazzo.github.io/)'s and [Alexandre Buisine](https://github.com/abuisine)'s
[KubeCon + CloudNativeCon Europe 2024](https://events.linuxfoundation.org/kubecon-cloudnativecon-europe-2024/) talk
titled [_We Tested and Compared 6 Database Operators. The Results are In!_](https://youtu.be/l33pcnQ4cUQ) I decided
to give [CloudNative PG](https://cloudnative-pg.io/) a go.
An honorable mention is the [Zalando postgres-operator](https://postgres-operator.readthedocs.io/en/latest/) which also
has an [optional GUI](https://github.com/zalando/postgres-operator/blob/master/docs/operator-ui.md).

### Installation

Following the [CloudNative PG documentation](https://cloudnative-pg.io/documentation/1.24/),
we can install the operator either directly from the manifest

```shell
kubectl apply --server-side -f \
  https://raw.githubusercontent.com/cloudnative-pg/cloudnative-pg/release-1.24/releases/cnpg-1.24.1.yaml
```

or through the [CloudNative-PG Helm chart](https://github.com/cloudnative-pg/charts) as

```shell
helm repo add cnpg https://cloudnative-pg.github.io/charts
helm upgrade --install cnpg \
  --namespace cnpg-system \
  --create-namespace \
  cnpg/cloudnative-pg
```

which will install the operator in the `cnpg-system` namespace.
Note that CloudNative PG requires a [_Server-side
apply_](https://kubernetes.io/docs/reference/using-api/server-side-apply/)
to ensure compatability with Kubernetes 1.29 and higher.
Keep this in mind if you're planning to install CloudNative PG using other methods,
e.g. using [Argo CD Applications](https://argo-cd.readthedocs.io/en/stable/user-guide/sync-options/#server-side-apply).

To see if the operator is up and running,
we can check that the associated deployment is ready by executing

```shell
kubectl get deploy -n cnpg-system 
```

For Operator customisation see the Helm
chart [values file](https://github.com/cloudnative-pg/charts/blob/main/charts/cloudnative-pg/values.yaml) and the
documentation on [Operator Configuration](https://cloudnative-pg.io/documentation/1.24/operator_conf/).

With the operator operational we're ready to
deploy a [Postgres Database Cluster](https://www.postgresql.org/docs/current/creating-cluster.html) using the
`postgresql.cnpg.io/v1` [
_Cluster_](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-Cluster) CRD.
This is also available as
a [Cluster Helm chart](https://github.com/cloudnative-pg/charts/blob/main/charts/cluster/Chart.yaml) from the same place
as the operator.

### Kubectl Plugin

CloudNative PG comes with an optional [kubectl plugin](https://kubernetes.io/docs/tasks/extend-kubectl/kubectl-plugins/)
to help make it easier to manage CloudNative PG resources.
You can install the plugin using the time-honoured tradition of curling directly to a shell

```shell
curl -sSfL \
  https://github.com/cloudnative-pg/cloudnative-pg/raw/main/hack/install-cnpg-plugin.sh | \
  sudo sh -s -- -b /usr/local/bin
```

The [plugin documentation](https://cloudnative-pg.io/documentation/1.24/kubectl-plugin/) also lists other ways of
installing the plugin if you prefer your packages signed.
There is also an [addon](https://cloudnative-pg.io/documentation/1.24/kubectl-plugin/#integration-with-k9s) for the
popular [K9s](https://k9scli.io/) TUI.

### Basic Database Cluster

Assuming you've configured a [default
_StorageClass_](https://kubernetes.io/docs/tasks/administer-cluster/change-default-storage-class/) that can
automatically provision volumes for you,
the simplest database "cluster" manifest you can create is

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/basic-cluster.yaml" >}}
{{< /highlight >}}

This will provision a 4 GiB volume for our database,
create RBAC objects
— [_ServiceAccount_, _Role_, and
_RoleBinding_](https://kubernetes.io/docs/reference/access-authn-authz/rbac/) —
[certificates](https://cloudnative-pg.io/documentation/1.24/certificates/),
a [_PodDisruptionBudget_](https://kubernetes.io/docs/tasks/run-application/configure-pdb/),
and [three different _Services_](https://cloudnative-pg.io/documentation/1.24/service_management/),
along with a [bootstrap](https://cloudnative-pg.io/documentation/1.24/bootstrap/) [
_Job_](https://kubernetes.io/docs/concepts/workloads/controllers/job/),
before finally starting the database in a new [Pod](https://kubernetes.io/docs/concepts/workloads/pods/).
A _Secret_ containing the database connection info is also created.

The certificates are used to facilitate TLS encryption and can be user provided with
e.g. [Cert-manager](https://cloudnative-pg.io/documentation/1.24/certificates/#cert-manager-example),
or manually created.

Note that the default `spec.instance` value is `1`,
which spins up only a primary database with no replicas,
i.e. not really a database cluster.

### Database Services

The _Cluster_ resource creates three _Services_ (prefixed `rw`, `ro` and `r`).
In the case of multiple instances the `rw`-service (read/write) will point to the primary instance,
the `ro`-service (read-only) points to the replicas,
and the `r`-service (read) points to any database instance.
For further customisation,
the  `ro`- and `r`
-services [can be disabled](https://cloudnative-pg.io/documentation/1.24/service_management/#disabling-default-services).
It's also possible
to [add custom services](https://cloudnative-pg.io/documentation/1.24/service_management/#adding-your-own-services),
if you e.g. require a [_LoadBalancer_](https://kubernetes.io/docs/concepts/services-networking/service/#loadbalancer)
type _Service_.

{{< mermaid-chart >}}
{{< readfile-rel file="resources/mermaid/services.mmd" >}}
{{< /mermaid-chart >}}

### Bootstrapping

The [bootstrap job](https://cloudnative-pg.io/documentation/1.24/bootstrap/) can be used to create a new cluster from
scratch, or from an existing PostgreSQL cluster,
either directly or indirectly through a backup.

In this article we'll only take a look at bootstrapping a fresh cluster,
though I encourage you to get to know the other bootstrapping possibilities for recovery purposes.

### Database Users

CloudNative PG automatically generates a `postgres` and an `app` user with a random password.

If no user password is configured using the `.spec.bootstrap.initdb.secret` field,
a `<CLUSTER_NAME>-app` secret is created containing the user password and database connection details.
The `.spec.bootstrap.initdb` can also be used to change the username of the custom user
— in which case the generated secret will be postfixed with that username instead of `app`.

The `postgres` superuser
— and the `postgres` database,
are supposed to be only used by the operator to configure the database cluster,
though it can be enabled by setting `.spec.enableSuperuserAccess` to `true`,
which will create a `<CLUSTER_NAME>-superuser` secret similar to the `app`-user secret.

To create additional users we can use the
declarative [Database Role Management](https://cloudnative-pg.io/documentation/1.24/declarative_role_management/)
options to configure them.

### Custom Database Cluster

The CloudNative PG Cluster resource has a lot of options.
I recommend you take a look at the [samples](https://cloudnative-pg.io/documentation/1.24/samples/) along with
the [API Reference](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-ClusterSpec)
to explore all the possibilities.

To go through some of the options I've prepared the following custom three instance (line 9) cluster
— one primary and two replicas.
You can find the full example in the [Summary](#cloud-native-pg) section.

In the example cluster, the `storageClass` is explicitly set (line 12) to be `proxmox-csi` (more details in
[this article]({{< ref "/articles/2024/06/k8s-proxmox-csi" >}})).
The database version is pinned in line 15 to be `16.4`.
Against better judgement we've enabled superuser access (line 20) with the password provided in the secret on line 22.

{{< highlight yaml "linenos=table, hl_lines=9 12 15 20 22 27 29 31 33-36 40 56 64" >}}
{{< readfile-rel file="resources/cnpg/custom-cluster.yaml" >}}
{{< /highlight >}}

In the bootstrap section we're creating a new database called `appdb` (line 27) with an owner called `user` (line 29).
We again provide the login password using a named _Secret_ (line 31).
In the post init section we've referenced a _ConfigMap_ with some illustrative instructions (lines 33-36).

We lightly touch the [
`postgresql` parameters](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-PostgresConfiguration)
to set the timezone to something appropriate (line 40).

Next we utilise the [
`managed` configuration](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-ManagedConfiguration)
to create a read-only user called `read` and disable the default `read-only` (`ro`) service on line 56.
We also create a custom _LoadBalancer_ type _Service_ to allow for external connections.
Here we've also taken advantage of [Cilium's LB-IPAM functionality](https://docs.cilium.io/en/stable/network/lb-ipam/)
(line 64) to declaratively give the _Service_ an IP we can reach outside the cluster (more details
in [this article]({{< ref "/articles/2023/12/migrating-from-metallb-to-cilium" >}})).

### Backups and Recovery

CloudNative PG relies on [Barman](https://pgbarman.org/) to perform on physical
and [Write-Ahead Logging](https://en.wikipedia.org/wiki/Write-ahead_logging) (WAL)
backups,
though if you prefer to have a logical backup of your database they've described how to perform this using `pg_dump` in
an [Emergency backup section](https://cloudnative-pg.io/documentation/1.24/troubleshooting/#emergency-backup) of their
documentation.

This "emergency" procedure boils down to running

```shell
kubectl exec  "<PRIMARY_CLUSTER_POD>" -c postgres \
  -- pg_dump -Fc -d "<DATABSE>" > "<DATABASE>.dump"
```

to save the logical backup on your work machine.
To restore the logical backup you can then run

```shell
kubectl exec -i "<NEW_PRIMARY_CLUSTER_POD>" -c postgres \
  -- pg_restore --no-owner --role="<USER>" -d "<DATABASE>" --verbose < "<DATABASE>.dump"
```

Regular backups using CloudNative PG requires that the database _Cluster_ is provided with a  [
_BackupConfiguration_](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-BackupConfiguration)
to perform either [Kubernetes volume snapshot](https://cloudnative-pg.io/documentation/1.24/backup_volumesnapshot/)
— backed by a _StorageClass_ that supports it,
or an [object store backup](https://cloudnative-pg.io/documentation/1.24/backup_barmanobjectstore/) using
the [Barman Cloud](https://pgbarman.org/) tool to
an [S3 compatible object store](https://cloudnative-pg.io/documentation/1.24/appendixes/object_stores/) like
e.g. [MinIO](https://github.com/minio/minio).

The recommended way of performing a backup using CloudNative PG is by using their [
_ScheduledBackup_](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-ScheduledBackup)
resource,
though they also offer an on-demand backup using the [
_Backup_](https://cloudnative-pg.io/documentation/1.24/cloudnative-pg.v1/#postgresql-cnpg-io-v1-Backup) resource.
Assuming you've configured the `backup` section of your _Cluster_ correctly,
this resource can be created using the kubectl plugin mentioned earlier by executing

```shell
kubectl cnpg backup <CLUSTER_NAME>
```

which should start creating a backup of your database.

### Monitoring

CloudNative
PG [readily integrates](https://cloudnative-pg.io/documentation/1.24/quickstart/#part-4-monitor-clusters-with-prometheus-and-grafana)
with both [Prometheus](https://prometheus.io/) and [Grafana](https://grafana.com/).

To make it easy to get started they've also prepared a
Grafana [dashboard template](https://github.com/cloudnative-pg/grafana-dashboards/blob/main/charts/cluster/grafana-dashboard.json)
to show relevant metrics for your different Cluster resources.

For more details see the [Monitoring](https://cloudnative-pg.io/documentation/1.24/monitoring/) section in the
CloudNative PG documentation.

### Other features

Worth noting is CloudNative PG's [Image Catalog](https://cloudnative-pg.io/documentation/1.24/image_catalog/) feature
along with the [Rolling Updates](https://cloudnative-pg.io/documentation/1.24/rolling_update/) options that allows for
a controlled upgrade of an existing database cluster.

For more advanced setups,
CloudNative PG also supports [Connection Pooling](https://cloudnative-pg.io/documentation/1.24/connection_pooling/)
using [PgBouncer](https://www.pgbouncer.org/),
[Tablespaces](https://cloudnative-pg.io/documentation/1.24/tablespaces/),
as well as the [PostGIS](https://cloudnative-pg.io/documentation/1.24/postgis/) extension,
and [pprof](https://github.com/google/pprof/tree/main)
for [profiling](https://cloudnative-pg.io/documentation/1.24/operator_conf/#pprof-http-server)
along with other nifty features.

For inspiration and further reading on how to use the CloudNative PG operator,
I
found [this article](https://mirakl.tech/mastering-kubernetes-and-database-administration-with-teleport-and-cloudnative-pg-a-step-by-step-f768f1c614de)
to be interesting.

## Summary

To avoid to many details in the main text I've put some of them in the summary to hopefully provide better examples.
You can also find these examples
on [GitLab](https://gitlab.com/vehagn/blog/-/tree/main/content/articles/2024/10/k8s-postgres/resources)

### Kustomize

{{< highlight yaml >}}
{{< readfile-rel file="resources/kustomize/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kustomize/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kustomize/svc.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kustomize/credentials.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/kustomize/stateful-set.yaml" >}}
{{< /highlight >}}

### Bitnami Helm chart

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/values.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/local-pv.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/local-pvc.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/helm/auth-secret.yaml" >}}
{{< /highlight >}}

### Cloud Native PG

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/kustomization.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/ns.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/postgres-credentials.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/user-credentials.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/read-credentials.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/cm-post-init.yaml" >}}
{{< /highlight >}}

{{< highlight yaml >}}
{{< readfile-rel file="resources/cnpg/custom-cluster.yaml" >}}
{{< /highlight >}}

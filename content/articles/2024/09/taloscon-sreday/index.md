---
title: "TalosCon & SREDay London 2024"
date: 2024-09-27T17:00:00+02:00
#lastmod: 2024-07-10T12:45:00+02:00

categories:
  - posts
  - conferences

tags:
  - conference
  - talos
  - taloscon
  - sre
  - sreday
  - london
---

[SREDay London 2024](https://sreday.com/2024-london/) together with the
day-0 [TalosCon](https://web.archive.org/web/20240919113858/https://www.siderolabs.com/taloscon/)[^1] just finished,
and I'm happy I got to attend!

[^1]: Sidero Labs already updated their webpages and apparently removed the schedule,
but the Wayback Machine remembers.

I first heard about TalosCon
during [KubeCon + CloudNativeCon](https://events.linuxfoundation.org/archive/2024/kubecon-cloudnativecon-europe/) in
Paris earlier this year,
which also gave me the final push to actually [try Talos]({{< ref "/articles/2024/08/talos-proxmox-tofu" >}}).

It was great getting to hang out and connect with fellow Talos users and SRE enthusiasts.
The intimate atmosphere at the Everyman Canary Wharf cinema made it easy to say hello to both the speakers and other
attendees.
I met a lot of great people (I still have much to learn!),
and as to not forget anyone I'll refrain from mentioning anyone except the presenters.

The following are my _from-the-hip_ impressions and thoughts around the conferences a week after.

## TalosCon

The [first on-prem TalosCon](https://www.siderolabs.com/blog/taloscon-2024-is-on-prem/) was held on September 18th.
The day was packed with interesting talks across two tracks along with a workshop track.
I of course couldn't attend all of them,
so apologies if you were one of the speakers and feel left out!
You can find recordings of the TalosCon 2024 talks in
this [YouTube playlist](https://www.youtube.com/playlist?list=PLSgt7RkT67fe79D0KfUjq0Ch0i5dxlxOM).

The opening Keynote held by [Steve Francis](https://www.linkedin.com/in/francissteve/)
— CEO of [Sidero Labs](https://www.siderolabs.com/),
reassured me that Talos is the way to go.
They seem to have found a great way of monetising their product with [Omni](https://omni.siderolabs.com/) and further
funding is secured.
Security is high on the agenda, and [v1.8.0](https://github.com/siderolabs/talos/releases/tag/v1.8.0)
— that just released,
offer [AppArmor](https://apparmor.net/) support,
with [SELinux](https://en.wikipedia.org/wiki/Security-Enhanced_Linux) support slated for the next minor release.
Better [disk management](https://www.talos.dev/v1.8/talos-guides/configuration/disk-management/#machine-configuration)
was also included in 1.8 which I've yet to play around with.

Following the inspiring
keynote, [Kubestronaut](https://www.cncf.io/training/kubestronaut/) [Pip Oomen](https://github.com/pjoomen)
from [Redpill-Linpro](https://www.redpill-linpro.com/en) held an informative history lesson on containerisation
techniques titled _OpenShift to Omni_.
For someone relatively new to the DevOps/SRE scene,
it was interesting to see the path taken to where we are today with Kubernetes.

![Openshift to Omni opening slide](images/openshift-to-omni.webp "The opening slide of Pip's Openshift to Omni talk")

Next on the agenda I attended [AT computing](https://atcomputing.nl/) [Michael Trip](https://michaeltrip.nl/)'s talk on
_Running [KubeVirt](https://kubevirt.io/)
on Talos Linux_.
The idea of running declaratively defined VMs inside an already declaratively defined Kubernetes cluster on top of an
immutable (and therefore also declaratively defined) OS is an exciting idea.
Next I'd just have to figure out [PXE Boot](https://en.wikipedia.org/wiki/Preboot_Execution_Environment) and how to
passthrough USB and PCIe-devices for a fully plug-and-play experience.
Michael recommend to steer away from [Multus](https://github.com/k8snetworkplumbingwg/multus-cni) for networking
unless you really needed it,
though a suggestion from the audience recommended he try [Kube-OVN](https://www.kube-ovn.io/) instead.

After lunch,
I opted to try out the workshop on running Omni on-prem with [Justin Garrison](https://justingarrison.com/) instead of
listening to [Tim Jones](https://www.linkedin.com/in/proto-tim-jones) on _How Sidero runs Omni_.
That unfortunately meant I also missed the talk on _Managing Talos with CUElang_
by [David Flanagan](https://www.linkedin.com/in/rawkode/), the man behind [Rawkode Academy](https://rawkode.academy/).
Both talks I'll have try to watch when the recordings are available.
The workshop unfortunately didn't go exactly as planned,
though we did get to chat with Justin,
and he convinced me that [Omni](https://omni.siderolabs.com/) was worth checking out
— at least in a professional setting.

With the workshop behind me, I attended [Ænix](https://aenix.io/) founder [Andrei Kvapil](https://github.com/kvaps)'s
talk on _Building a Bare Metal Platform with Talos_, which further inspired me to try something similar.
Parallel with Andrei's talk,
[Neil Cresswell](https://www.linkedin.com/in/ncresswell/)
— [Portainer](https://www.portainer.io/) CEO, presented _Omni deployments made easy_,
on how to manage multiple clusters with Portainer.
which — tongue-in-cheek, maybe should've been held before the Omni workshop.

The last talk I attended was titled _From homelab to prod_ by [Gerhard Lazu](https://gerhard.io/),
meaning I missed out on [42on](https://42on.com/)'s CTO [Wout van Heeswijk](https://github.com/wvh-github) talking about
_OpenStack on Talos Linux_.
I felt Gerhard's talk was more about [Dagger](https://dagger.io/) (which I _also_ need to check out soon),
than taking your homelab and running it in production,
though it could be I simply misunderstood the premise of the talk.

The remaining talks I didn't attend were two about edge deployments
— _Why Talos at the edge?_ held by [Pau Campaña](https://medium.com/@pau.campana) from [Roche](https://www.roche.com/),
and _Bringing Kubernetes to the Edge: Lessons from the Frontlines_ presented
by [Fullstaq](https://www.fullstaq.com/) CEO [Gerrit Tamboer](https://github.com/gerr1t),
along with [Vincent Behar](https://vincent.behar.name/) talking about how Ubisoft is _Running Game servers on Talos
Linux_, and [Timo Ahokas](https://www.linkedin.com/in/timoahokas/)'s _Talos @NESC: From core compute to AI/ML in Nokia
private & edge clouds_.

## SREDay London 2024

With three tracks over two days SREDay(s) was a blast.
There were a lot of great talks,
though I unfortunately didn't take notes, so I'll allow myself to only summarise what I thought were the highlights of
the conference and the general vibe.

The conference was kickstarted with a keynote by [Ajuna Kyaruzi](https://www.linkedin.com/in/ajuna/)
from [Datadog](https://www.datadoghq.com/) on _Automated Incident Management_
before [Chris Cooney](https://www.linkedin.com/in/chris-cooney-06751870/) from [Coralogix](https://coralogix.com/) told
_The History of DevOps in 30 minutes_.
[Alayshia Knighten](https://www.linkedin.com/in/alayshia/) ended with [Pulumi](https://www.pulumi.com/)'s solution to
_Unleashing Potential Screoss Teams: Infrastructure as Code_,
yet again affriming that I really have to try out Pulumi soon.

![A happy puppy](images/puppy.webp "Alayshia Knighten's dog with a pride scarf")

Many of the talks centered on culture creation in your organisation.
These talks included examples on both to build a great developer experience,
and sarcastic examples on how not to,
for instance [Cognizant](https://www.cognizant.com/)'
s [Antonio Cobo Cuenca](https://www.linkedin.com/in/antoniocobocuenca/)'s talk about _From Hero to Zero:
practical guide to destroy your best employees_.

[Leon Adato](https://www.linkedin.com/in/leonadato/) from [Kentik](https://www.kentik.com/) began the infra-track with
an insightful and fun lecture titled _Alerts don't suck, YOUR alerts suck_.
He made many great points about how you should only have alerts you care about.
Do you have that alert you don't really care about until the third time it shows up?
Then maybe just have an alert for when the trigger occurs three times during a given interval.
Sure, it might be more complex to configure,
but once it's set up you don't have to touch it again and there will be fewer attention stealing messages.

[Hrittik Roy](https://www.linkedin.com/in/hrittikhere/) presented [Loft Labs](https://www.loft.sh/)' approach to
_Virtual Kubernetes Clusters: A New Approach to Multi-tenancy_, which for me seems like a great solution for perhaps
a niche area.
I'd still want to play around with it and see how it ticks though!

With lunch finished, [Harel Safra](https://www.linkedin.com/in/harelsafra/) from [Riskified](https://www.riskified.com/)
gave the talk _When Infrastructure as Code Ends - Jump in and Create Some More_ which made a great job of writing your
first [OpenTofu](https://opentofu.org/)/[Terraform](https://www.terraform.io/) provider less intimidating.

[Stenn International](https://www.stenn.com/)'s [Aleksei Popov](https://www.linkedin.com/in/aleksei-popov-771b34233/)
walked us through
_Architecture of a Fintech Startup: Tackling Growing Complexity at Scale_ before
[Thijs Feryn](https://www.linkedin.com/in/thijsferyn/) which introduced us to _Caching the uncacheable
in [Varnish](https://www.varnish-software.com/)_.
I thought both talk were interesting,
and I'll definitely try to remember that Varnish exists when I have to think about caching at that scale.

After an evening socialising,
the last day begain with [Joyce Lin](https://www.linkedin.com/in/joyce-lin/) from [Viam](https://www.viam.com/)
reaffirming the importance of infrastructure as code.
Justin Garrison continued the keynotes with by restating the benefits of declarative configuration in his talk titled
_Declarative Linux with the Common Operating System Interface_, selling the idea behind Talos
— [COSI](https://github.com/cosi-project/community), the Common Operating System Interface.

The last keynote was held by [SREDay](https://sreday.com/)
co-founder [Miko Pawlikowski](https://www.linkedin.com/in/mikolajpawlikowski/) with a humours talk originally titled
_The state of SRE in 2024_ filled with dinosaurs.

[![The evolution of operations](images/triceratops.webp "The Evolution of Operations by @acloudguru")](https://x.com/acloudguru/status/1316054011724869632)

[Data on Kubernetes](https://dok.community/) ambassador [Divine Odazie](https://www.linkedin.com/in/divine-odazie/)
from [Severalnines](https://severalnines.com/) kicked off the tools-track with
_Cloud Native Technologies: The Building Blocks of Modern Database Software_,
talking about how cloud-native technologies like database operators
and [Container Storage Interfaces](https://kubernetes-csi.github.io/docs/) are revolutionising database software.

[Marcos Diez](https://github.com/marcosdiez) from [Modus Create](https://moduscreate.com/) delivered a very intriguing
talk on replacing YAML with Terraform code titled _Kubernetes, AWS, ALBs, Terraform and no Helm!_.
You can find an example of how he does that over at GitHub along with his presentation
in [this repository](https://github.com/marcosdiez/presentations).

Resident meme connoisseur [Oleg Fatkhiev](https://www.linkedin.com/in/ofats/) at [Citadel](https://www.citadel.com/)
continued the track with _Config files vs. flags: story of pain_ elaborating on the use of flags instead of config files
for easier development.

[Rawkode Academy](https://rawkode.academy/) founder [David Flanagan](https://www.linkedin.com/in/rawkode/) treated us
with an attempt to fix a broken Kubernetes cluster.
From my recollection he had a selection of clusters he had broken himself some time ago,
but had no idea which of the broken clusters he would attempt to fix.
Unbeknownst to all of us,
Cilium had released a [broken version of cilium-cli](https://github.com/cilium/cilium-cli/releases/tag/v0.16.17) a
couple of days prior,
which meant the cluster was broken in a completely new way!
The whole kerfuffle made for an entertaining, interactive,
and highly educational experience in debugging Kubernetes clusters.
With some help from the audience, David managed to fix the cluster justa couple minutes over schedule.

[Matteo Bianchi](https://www.mb-consulting.dev/services) from [Omnistrate](https://www.omnistrate.com/) held a humbling
talk about _Oops, I deployed too hard_ when he once accidentally took down a production service for a big customer and
unknowingly took a long spicy lunch when it happened, coming back to a flurry of messages.
He proselytised the importance of building a culture where it's acceptable to make mistakes and learn from them,
a belief I strongly hold myself.

![A convoluted workflow](images/workflow.webp "Matteo's previous painfully outdated workflow")

Following Matteo's modest talk,
[Ondřej Babec](https://www.linkedin.com/in/ondrej-babec/)
and [Jiří Novotný](https://www.linkedin.com/in/ji%C5%99%C3%AD-novotn%C3%BD-294915202/)
from [Red Hat](https://www.redhat.com/) presented _Debezium Server - New CDC runtime on the horizon!_.
[Debezium](https://debezium.io/) is an open source tool for change data capture,
listening to inserts, updates, and deletes that other apps commit to your database.
This is one more of those tools I don't need right now, but it's good to know it exists for when I need it!
You can check out the Debezium example they presented in
this [GitHub repository](https://github.com/obabec/sreday-2024-demo).

For the last two talk I changed to the observability track to listen to [OVHCloud](https://www.ovhcloud.com/en/)
engineer [Wilfried Roset](https://www.linkedin.com/in/wilfriedroset/).
He elaborated on how observability can improve service quality down to the database level in his talk titled _Improving
the efficiency of more than 800 databases with observability: 4 years later_.

Lastly, [Ricardo Castro](https://mccricardo.com/), SRE
at [FanDuel](https://sportsbook.fanduel.com/)/[Blip.pt](https://www.blip.pt/) reassured us that
_DevOps Is Not Dead. Not by a Long Shot_.
> DevOps is not a trend that has come and gone. It's a cultural shift that has fundamentally changed how software is
> developed and deployed. While emerging practices like Platform Engineering may seem to be taking over, they are not
> meant to replace DevOps; instead, they are complementary to it.

## Summary

I had a great experience attending TalosCon and SREDay London and I would like to
thank [Edgeworks](https://www.edgeworks.no/) for letting me attend as part of their excellent focus on professional
development (PS: We're hiring!).

I'd also like to thank all the great people I met and discussed with, I hope I meet you again later!
Please feel free to reach out if you'd like to stay in touch.

![THE END](images/the-end.webp "Gerhard's closing slide befitting the cinema setting")

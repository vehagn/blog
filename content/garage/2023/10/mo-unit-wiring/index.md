---
title: "mo.unit wiring"
date: 2023-10-31T12:00:00+02:00

layout: simple

draft: false
categories:
  - posts
  - garage
tags:
  - mo.unit
  - mo.unit blue
  - motorcycle
  - electrical
  - wiring
  - moto guzzi
  - ZDG 3.23
---

My first motorcycle was a Moto Guzzi V65 Custom from 1986 which I constantly tinker with.
One of the charms with veteran bikes is that you can kinda hold all the inner workings of the bike in your head at once,
and it all kinda just works if you don't mess up too bad.

I've gone through the whole transmission after the clutch release rod got bent,
changed the piston and cylinders, replaced the carburettors,
and — the focus of this article, upgraded the whole electrical system.

![Moto Guzzi V65 Custom 1986](images/01-moto-guzzi-v65c.jpg "My Moto Guzzi V65 Custom")

When I got the bike you could crank the engine without turning the key,
so I knew something was off with the wiring.
The ignition coils wouldn't fire,
but that didn't stop a would-be-thief for cranking the battery empty.

After the speedometer drive broke I decided I had enough (bad) excuses to upgrade the electrical components.

## Components

I've been continuously upgrading the electrical system over a couple of seasons.
The main components of my current set-up are

* [mo.unit blue](https://motogadget.com/products/mo-unit-blue) — central control unit
* [mo.button](https://motogadget.com/products/mo-button) — reduce cable clutter
* [mo.switch mini](https://motogadget.com/products/mo-switch-mini) — handlebar push buttons
* [motoscope pro](https://motogadget.com/products/motoscop-pro) — digital dashboard
* [msp breakout box A](https://motogadget.com/products/msp-breakout-box) — dashboard breakout box
* [mo.blaze disc](https://motogadget.com/products/mo-blaze-disc) — front blinkers
* [mo.blaze pin](https://motogadget.com/products/mo-blaze-disc) — rear blinkers
* [Elektronik Sachse Ignition ZDG 3.23](https://www.elektronik-sachse.de/shopsystem-3/en/digital-ignition-zdg-3-23-for-guzzi-v35-v65-nevada.html) —
  digital ignition
* [QuadLock USB-charger](https://www.quadlockcase.eu/) — USB-charger

I've also changed the rectifier/regulator and updated the battery to a small LiFePO<sub>4</sub>-one and kept the
nondescript LED headlight and Valeo starter the previous owner installed.

## Schematic

I decided to use [KiCad](https://www.kicad.org/) to create a schematic to tie all the components together.
This should make fault seeking errors easier when connecting everything together.
I'm no expert in KiCad, but I'm happy enough with the result that I decided make it available at 
GitHub [here](https://github.com/vehagn/motogadget-wiring).
If you'd like to use it for your own bike feel free to fork the repository or send me a pull request.

{{< dynamic-image title="Wiring schematic for an mo.unit blue unit in my Moto Guzzi V65C" alt="mo.unit wiring schematic" id="02-schematic" dark="images/02-schematic-dark.svg" light="images/02-schematic-light.svg">}}

## Afterthoughts

I'm planning to do a similar conversion on a Moto Guzzi 850 T5 Polizia I recently bought.
Some improvements in my current set-up I'd like to do is remove the mo.button so I can more easily change 
which switch does what.
Running fewer wires is nice, but the signal wires are small enough to not have too much of an impact.

The combined digital ignition and rectifier/regulator that 
[Elektronik Sachse](https://www.elektronik-sachse.de/shopsystem-3/en/ignition-rectifier-regulator-combination-pureg.html)
would also be a welcome improvement to save some space.

Next I have to figure out how [Kaffeemaschine Motorcycles](https://www.kaffee-maschine.net/) get their bikes
so clean and try to emulate that! ☕️🏍️

![mo.unit blue cable spaghetti](featured.webp "Cable spaghetti")

{{< comments >}}

